# KDevelop Website

[![Build Status](https://binary-factory.kde.org/buildStatus/icon?job=Website_kdevelop-org)](https://binary-factory.kde.org/job/Website_kdevelop-org/)

This is the git repository for [kdevelop.org](https://kdevelop.org), the website for KDevelop.

As a (Hu)Go module, it requires both Hugo and Go to work.

### Development
Read about [hugo-kde theme](https://invent.kde.org/websites/hugo-kde/-/wikis/)

### I18n
See [hugoi18n](https://invent.kde.org/websites/hugo-i18n)
