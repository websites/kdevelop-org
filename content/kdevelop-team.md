---
title: About
menu:
  main:
    parent: more
    weight: 62
---

KDevelop is a free software integrated development environment (IDE) developed under the [KDE](https://kde.org) Umbrella. KDevelop provides support for a wide variety of languages (such as C/C++, Python, PHP, Ruby, ...) via an extensible plugin framework.

KDevelop is mostly developed by volunteers, who contribute to the project in their spare time.

If you are not interested in [contributing to KDevelop](/contribute-kdevelop/), please take a look if you want to [contribute to KDE](https://community.kde.org/Get_Involved).
The KDE community is a very welcoming bunch of people.

If you are not able to contribute, you might be interested in [donating](https://kde.org/donations).
While these donations are not directly targeted at specific development, they help to keep the KDE community going.
For example some KDevelop related coding sprints were funded by the [KDE e.V.](https://ev.kde.org) based on these donations.
