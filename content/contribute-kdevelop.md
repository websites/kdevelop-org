---
title: Contribute to the KDevelop project
menu:
  main:
    parent: more
    weight: 61
    name: Contribute
---

If you are willing to help KDevelop, there are multiple ways to contribute.

## Programming

{{% i18n "contribute-programming" "irc://irc.libera.chat/kdevelop" "https://mail.kde.org/mailman/listinfo/kdevelop-devel" "https://invent.kde.org/kdevelop/kdevelop/-/merge_requests" "https://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&columnlist=product%2Ccomponent%2Cassigned_to%2Cbug_status%2Cresolution%2Cshort_desc%2Cchangeddate%2Copendate&list_id=1451847&order=changeddate%20DESC%2Cpriority%2Cbug_severity&product=kdevelop&product=kdevplatform&query_format=advanced" "https://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&bug_status=NEEDSINFO&keywords=junior-jobs%2C%20&keywords_type=allwords&known_name=KDevelop%20junior%20jobs&list_id=1382095&product=kdev-python&product=kdevelop&product=kdevplatform&query_based_on=KDevelop%20junior%20jobs&query_format=advanced" %}}

We are also looking for people proficient on non-Linux platforms, most notably MS Windows and Mac OS X. KDevelop can be built on these platforms, but there might be bugs. Especially on Windows there are some outstanding issues. We would very much appreciate your help, just contact us to get started.

### How to Build KDevelop

KDevelop can easily be build on a current Linux distribution, we provide a [tutorial about building KDevelop and the needed dependencies](/build-it/) for this.
For other operating systems, the [same page](/build-it/) page provides extra links with information.

## Translation

Localization of KDevelop is handled by the awesome people behind [l10n.kde.org](https://l10n.kde.org/). Please get in contact with them and start translating KDevelop. To check the translation status of KDevelop into your native language, please take a look [here](https://l10n.kde.org/stats/gui/stable-kf5/package/kdevelop/).

## Documentation

KDevelop's documentation is lacking behind unfortunately -- we are in need of help here. Please step up, contact us via IRC or the mailing lists and start writing documentation. It is as easy as writing a short text and adding some screenshots to the [KDevelop Documentation Wiki](https://userbase.kde.org/KDevelop).
