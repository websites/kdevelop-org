---
title: Download
scssFiles:
  - /scss/get-it.scss
menu:
  main:
    weight: 2
---

{{< get-it src="/reusable-assets/tux.png" >}}

### Linux

+  Install [KDevelop](https://apps.kde.org/kdevelop) from [your distribution](https://kde.org/distributions/).

  {{< appstream_badges >}}

+  Install [KDevelop's Snap package from Snapcraft](https://snapcraft.io/kdevelop).

  {{< store_badge type="snapstore" link="https://snapcraft.io/kdevelop" divClass="store-badge" imgClass="store-badge-img" >}}

{{< /get-it >}}

{{< get-it src="/reusable-assets/windows.svg" >}}

### Windows

+  Download [the nightly KDevelop 64-bit build](https://cdn.kde.org/ci-builds/kdevelop/kdevelop/master/windows/). \*\*
+  Install [KDevelop via Chocolatey](https://chocolatey.org/packages/kdevelop). \*\*\*

{{< /get-it >}}

{{< get-it src="/reusable-assets/macos.svg" >}}

### macOS

+  Download the [KDevelop release installer](https://binary-factory.kde.org/view/MacOS/job/KDevelop_Release_macos/lastSuccessfulBuild/artifact/). *

{{< /get-it >}}

{{< get-it src="/reusable-assets/git.svg" >}}

## Source Code

The [source code for KDevelop](https://invent.kde.org/kdevelop/kdevelop) is available on KDE’s GitLab instance. For detailed instructions on how to build KDevelop from source, check the [Build it](/build-it) page.

{{< /get-it >}}


{{< get-it-info >}}

**About the releases:** <br>
[KDevelop](https://apps.kde.org/kdevelop) is part of KDE Gear, which are [released typically 3 times a year en-masse](https://community.kde.org/Schedules). The [text editing](https://api.kde.org/frameworks/ktexteditor/html/) and the [syntax highlighting](https://api.kde.org/frameworks/syntax-highlighting/html/) engines are provided by [KDE Frameworks](https://kde.org/announcements/kde-frameworks-5.0/), which is [updated monthly](https://community.kde.org/Schedules/Frameworks). New releases are announced [here](https://kde.org/announcements/).

\* The **release** packages contain the latest version of KDevelop and KDE Frameworks.

\*\* The **nightly** packages are automatically compiled from the source code of the `master` branch, therefore they may be unstable and contain bugs or incomplete features. These are only recommended for testing purposes.

\*\*\* The Chocolatey packages are developed independently from KDE. It may or may not be up to date.

{{< /get-it-info >}}
