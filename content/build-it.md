---
title: Build It
hideMeta: true
author: Joshua Goins
date: 2023-02-12
menu:
  main:
    weight: 4
---

KDevelop's source code is located on [KDE Invent](https://invent.kde.org/), KDE's GitLab instance. This tutorial explains how to build KDevelop without touching your global installation.

All stuff will happen inside an extra development directory. This avoids the need for administrator rights and allows always to fallback to use the global installed KDevelop if the currently build version is broken.

As this tutorial will introduce a setup that allows to develop other parts of the KDE software stack, too, we will talk in the following about setting up a KDE development environment. Required KDE Frameworks will be compiled in addition to the KDevelop application parts. This eases contributions to all levels of the KDE software stack.

The way to download, compile, install and run KDevelop depends on how you want to download it and where do you want to install it to. So follow the step by step instructions below.

## Choose a KDE development directory

Choose a path where all KDevelopment & KDE development things should end up. Beside user local configuration files, nothing outside of this directory will be polluted. You can re-use this for development on any part of the KDE software stack.

For the remaining parts of this description we will use `~/projects/kde`, but feel free to customize this.

## Install kdesrc-build

To make development easier, KDE has a meta-build tool called `kdesrc-build` which will drag in any additional dependencies (mostly KDE frameworks) required by KDevelop. Installing it is as simple as cloning it:

```
mkdir -p ~/projects/kde/src
cd ~/projects/kde/src
git clone https://invent.kde.org/sdk/kdesrc-build.git
```

Then run the initial setup, which includes putting it in your `$PATH` and some required
dependencies from your distribution:

```
cd ~/projects/kde/src/kdesrc-build
./kdesrc-build --initial-setup
source ~/.bashrc
```

## Configuring kdesrc-build

kdesrc-build has some setup pass that can configure stuff for you, see [this introduction](https://community.kde.org/Get_Involved/development#Set_up_kdesrc-build).

Here we just show a **~/.config/kdesrc-buildrc** that is good enough for KDevelop development needs.

You can just copy and paste the below variant into your home directory as **~/.config/kdesrc-buildrc** and adapt the paths to your needs. If you stick with the `~/projects/kde` path we did choose above, this should be usable 1:1.

```
global
    # use the latest KF5 and Qt5-based software.
    branch-group kf5-qt5

    # Install directory for KDE software
    kdedir ~/projects/kde/usr

    # Directory for downloaded source code
    source-dir ~/projects/kde/src

    # Directory to build KDE into before installing
    # relative to source-dir by default
    build-dir ~/projects/kde/build
end global

include ~/projects/kde/src/kdesrc-build/kf5-frameworks-build-include
include ~/projects/kde/src/kdesrc-build/kf5-applications-build-include
include ~/projects/kde/src/kdesrc-build/kf5-workspace-build-include
include ~/projects/kde/src/kdesrc-build/kf5-extragear-build-include
```

## Build KDevelop & dependencies

To trigger a compile of KDevelop and all needed KDE dependencies now just type:

```
kdesrc-build --include-dependencies kdevelop
```

This will take more than a few minutes, even on a recent machine. Time to grab some coffee or think about what you actually want to implement.

KDevelop has some optional components not part of the main module, such as:
* [kdevelop-pg-qt](https://invent.kde.org/kdevelop/kdevelop-pg-qt) - Binding generators.
* [kdevelop-python](https://invent.kde.org/kdevelop/kdev-python) - Python support plugin.
* [kdevelop-php](https://invent.kde.org/kdevelop/kdev-php) - PHP support plugin.

To build these, just insert their name:

```
kde-src-build kdevelop-php kdevelop-python
```

And they will be installed and accessible within this installation of KDevelop.

If during the build you see any failures displayed with a red font, follow this guide - [How to solve build problems](https://community.kde.org/Get_Involved/development#How_to_solve_build_problems).

## Run KDevelop

Simply use the `kdesrc-run` script, which will set up the proper environment for you:

```
kdesrc-run kdevelop
```

Congratulations! You just built KDevelop from source and are already set up for development!

## Keep your stuff up-to-date

To keep your local version up-to-date, you can just use the above commands again. They will take care of pulling new changes from the KDE repositories and building/installing them into your local prefix.

## Contribute back!

Feel free to create a merge request on [KDE Invent](https://invent.kde.org/kdevelop/kdevelop/merge_requests). For more information see [Contribute](/contribute-kdevelop).
