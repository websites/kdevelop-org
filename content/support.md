---
title: Support
menu:
  main:
    weight: 3
---

Before looking for support, be sure to read the available documentation, whether that is the [application handbook](http://docs.kde.org/stable/en/applications/kdevelop/index.html), developer documentation or other documentation.

### Get in touch

When in need for help, you have the following options to get in touch with KDevelop developers and users:

  * Use our mailing list, <kdevelop-devel@kde.org>.
    The mailing list is open, that is you do not need to be subscribed to contact us. Do not forget to ask to be CC&#8217;d in answers in that case. To subscribe, visit the [kdevelop-devel mailing list](http://mail.kde.org/mailman/listinfo/kdevelop-devel). For archives, go [here](http://lists.kde.org/?l=kdevelop-devel&r=1&w=2). Mark Mail has a [great overview](http://markmail.org/search/?q=kdevelop-devel#query:kdevelop-devel%20list%3Aorg.kde.kdevelop-devel%20order%3Adate-backward+page:1+state:facets) of our list, too.
  * Visit our IRC channel [#kdevelop on irc.libera.chat](irc://irc.libera.chat/kdevelop).
    You can often meet KDevelop developers there, and we are usually willing to help with user support requests as well as development questions. Alternatively, try [#kde](irc://irc.libera.chat/kde) for user support and [#kde-devel](irc://irc.libera.chat/kde-devel) for development help.

Many of the bigger applications using Kate Part such as Kate or KWrite have their own support channels, mailing lists, IRC channels and web forums. If you have a problem with one of these applications and you are not sure if this is a bug of the Kate Part then please first report it to them. Please prefer these public channels over contacting the authors in private.

### Using the KDE bug system

The KDE bug tracking system at [bugs.kde.org](https://bugs.kde.org) is used for managing all bugs related to KDevelop. You can use the _Help->Report Bug_ menu item found in most KDE applications to report bugs or wishes. It will start the bug report wizard and fill in some information for you.

Be sure to include as much information as possible when reporting bugs, the better the report the better the chances we can fix the reported problem.
