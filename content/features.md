---
title: Features
menu:
  main:
    weight: 1
---

## Supported languages

![KDevelop's C/C++ language support](/images/kdevelop-code.png)

The following languages are fully supported, which means they have semantic syntax highlighting, code navigation and completion:
* C/C++ and ObjC (backed by the [Clang/LLVM](https://clang.llvm.org/docs/Tooling.html) libraries)
  * Including some extra features for the [Qt Framework](https://www.qt.io/developers/)
  * Including language support for [CUDA](https://developer.nvidia.com/cuda-zone) and [OpenCL](https://www.khronos.org/opencl/)
* [Qt QML](https://doc.qt.io/qt-5/qtqml-index.html) and JavaScript
* Python (optional plugin)
* PHP (optional plugin)

For virtually all non-supported languages, there is still basic syntax highlighting and text-based code completion.

## Supported version control systems

![The "Show annotation" feature with the Git VCS.](/images/annotate2.png)

The following version control systems have GUI integration:

* Git
* Bazaar
* Subversion
* CVS
* Mercurial (hg) (optional plugin)
* Perforce (since 5.1)

An especially useful feature is the _Annotate_ border, which shows who last changed a line and when. Showing the diff which introduced this change is just one click away!

## Supported build systems

![KDevelop's CMake settings dialog](/images/kdevelop-cmake-settings.png)

The following build systems are supported, which means that KDevelop can automatically obtain run and build targets as well as include paths from them:

* CMake
* QMake
* Custom Makefiles (partially)
* Generic build system (no automatic include path detection)

KDevelop knows how to invoke build automation tools such as _make_ and _ninja_ and has dedicated configuration pages for them.

## Language support features

For all supported languages, KDevelop supports the following features:

### Quick code navigation

![Navigation widget](/images/nav_0.png)

Navigation widget popup when moving the mouse over items, or when pressing Alt with the text cursor over them. The widget allows navigation, i.e. you can for example navigate to the parent class of the class you are viewing to look at the navigation widget of that parent class.

* Find uses of symbols (variables, types, etc.)
* Jump to declaration / definition
* **Quick Open**, allowing to search for any function, class or file project-wide.
* Project, class and function browser

The code browser mode (press Ctrl when clicking on an symbol) makes it super easy to browse through an unknown code base.

### Remarkable code comprehension

![KDevelop's code completion](/images/kdevelop-completion.png)

KDevelop always prided itself for its state of the art C/C++ language support. With the Clang adoption in 5.0, we get 100% correct code parsing functionality, which means the IDE can accurately check for coding faults even without having you to invoke the compiler! Not limited to C/C++, all supported languages share below features.

* **Semantic highlighting**: Highlighting which takes the semantics of the language into account, and for example highlights class names differently from variable names, even if this cannot be recognized from the syntax of the code alone.
* **Rainbow highlighting**: Highlights each occurrence of the same variable in a specific color, changing on a per-variable basis. Try it out, you will love it once you get used to it. Optional.
* **Context-sensitive, semantic code completion**: KDevelop has sophisticated heuristics designed to present you with the most likely suggestions at the current cursor position. For Python, PHP and JS, those completions are generated from KDevelop's cached code representation and are thus available instantly -- without noticeable delay.

**Persistent disk cache** for the code database. Depending on the size of complexity of your project, analysis might take a while at first; but after that first time, everything is cached on disk and loaded in few seconds on each subsequent startup of the IDE.

## Customizable

![KDevelop under a dark theme](/images/kdevelop-darktheme-small.png)

Easy to adapt to your own style!

* Free to reorder, enable or disable any buttons in the menu bar
* Arbitrary split views
* Arbitrary layout of tool views inside the main window
* Freely configurable color scheme, separately for application and editor.
* Can assign shortcuts to almost every action in the IDE

## Documentation integration

![KDevelop's documentation integration (here: QtHelp)](/images/kdevelop-documentation.png)

KDevelop provides seamless integration with a variety of documentation providers. The supported systems are:

* QtHelp documentation files (QCH)
* Man pages (the ones you can just view with `man` on command line)
* CMake help pages

You can simply open a documentation page by hovering a symbol and then selecting "Show documentation for ..."

## Other bits and pieces

* Fast startup and typically small memory footprint.
* Generally designed to behave like a simple text editor in many cases, for example you can just run kdevelop file.cpp from your terminal and it will open that file in the current session.
* Fully scriptable, powerful code snippets with template fields (press Tab to cycle between user-defined fields, can have fields with scriptable contents, etc); those can also be configured to show up in the code completion list
* Powerful find and replace, also across whole projects, optionally with regular expressions
* Unit test integration
* Filterable Problems toolview, showing all problems (syntax and semantic errors, TODOs, etc)
* Graphical debugger support (C/C++ and Python, PHP through an extra plugin)
* Code formatter support (several predefined ones for C/C++ and Python, as well as custom formatter scripts)
* File and project templates (extra templates can be downloaded or installed)
* Can view almost any file type as a tab / document inside the IDE through KParts (e.g. a PDF)
* Console toolview
* External script support
* Vim compatible input mode
