---
title: "KDevelop 5.4.3 released"
date: "2019-10-21 16:31:08+00:00"
lastmod: "2019-10-21 17:02:01+00:00"
author: "kossebau"
aliases:
- /news/kdevelop-543-released
tags:
- release
categories:
- News
---
We today provide a stabilization and bugfix release with version 5.4.3. This is a bugfix-only release, which introduces no new features and as such is a safe and recommended update for everyone currently using a previous version of KDevelop 5.4.


You can find the updated Linux AppImage as well as the source code archives on our [download](/download) page.


## ChangeLog


### [kdevelop](https://commits.kde.org/kdevelop)


* ProblemNavigationContext: Fix incorrect link target for file:line links with declarations. ([commit](https://commits.kde.org/kdevelop/7117c37a432a1b52a082a4bc3119c65f81ceb605))
* ProjectManagerView: Make items with an empty icon have the same indent as items with a valid icon. ([commit](https://commits.kde.org/kdevelop/de7ba3c75a7788728f1bca1602f437772f83fb0f))
* Welcome page: fix version of QtQuick.XmlListModel import to match Qt 5.7 ff. ([commit](https://commits.kde.org/kdevelop/fc5bba734c50f140b41633cd140ae923a2abdabf))
* Welcome page: fix version of QtQuick.Layouts import to match Qt 5.7 ff. ([commit](https://commits.kde.org/kdevelop/fa0f1f8f2b861eb37d234f6920c07d92f3117a29))
* [Grepview] Use the correct fallback icon for Collapse All. ([commit](https://commits.kde.org/kdevelop/24d0c68338b7ebe805712ed73e62f2b52756fd19))
* FileManagerListJob: use a semaphore for locking. ([commit](https://commits.kde.org/kdevelop/900d5ec826bcada005ba243bf26d753c89740cab))
* Fix Infinite Recursion in DUChain. ([commit](https://commits.kde.org/kdevelop/f81d8884bd27c9a48717000a50cdd9cc7050685b))
* Cache the defines/includes for gcc-like compilers per language type. ([commit](https://commits.kde.org/kdevelop/b740e32dec3d00bbed7b95b5a1eab1ce68a79cab))
* Only write defines file when needed. ([commit](https://commits.kde.org/kdevelop/2d40e10b83094d20ccd0546e8422cb30bd1fa189))
* Don't set super long name on completely anon structs. ([commit](https://commits.kde.org/kdevelop/9a91576316ef05539705fed32c75c40bf8c5b8b2))
* Use type name as fallback ID for anon typedef'ed structs/unions etc. ([commit.](https://commits.kde.org/kdevelop/26ee0da99a0cb9497fdebc0121576b34508e5d9d) fixes bug [#409041](https://bugs.kde.org/409041))
* Fixed crash when filtering class list. ([commit.](https://commits.kde.org/kdevelop/5bff5d65a023cfce078c0abc7f82b9899f49fffb) code review [D22660](https://phabricator.kde.org/D22660). fixes bug [#406931](https://bugs.kde.org/406931))
* Gcclikecompiler: properly resolve include paths with symbolic links. ([commit.](https://commits.kde.org/kdevelop/c315333c4aa38ec9d05f101844fc4729717fd13b) fixes bug [#405221](https://bugs.kde.org/405221))


### [kdev-python](https://commits.kde.org/kdev-python)


No user-relevant changes.


### [kdev-php](https://commits.kde.org/kdev-php)


* Fix expressions using array constants. ([commit.](https://commits.kde.org/kdev-php/cb71eff2153aeb8b3b9f09e0bb232807a523884d) fixes bug [#405719](https://bugs.kde.org/405719))
* Don't mark class constants as normal members. ([commit](https://commits.kde.org/kdev-php/df6070ff9991f24ea0828d08b6f8528130a64a2c))


