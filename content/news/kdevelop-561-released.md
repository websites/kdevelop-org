---
title: "KDevelop 5.6.1 released"
date: "2020-12-11 20:25:42+00:00"
lastmod: "2020-12-11 20:26:53+00:00"
author: "sbrauch"
aliases:
- /news/kdevelop-561-released
tags:
- release
categories:
- News
---
About three months after the release of KDevelop 5.6.0, we provide a first bugfix and stabilisation release for the 5.6 series today: KDevelop 5.6.1


Most notably,


* an incompatibility of kdev-python with Python versions earlier than 3.9.0 was fixed, which led to instability,
* support for gdb 10.x was fixed,
* an issue in the "Run test" feature was fixed with tests launched by the same executable (377639),
* several crashes on exit while debugging was fixed (425994, 425993, 425985),
* the output view now highlights glibc assert messages by default,
* behaviour of the "Stop" and "Stop all" toolbar buttons was made less confusing (it no longer has a delayed-dropdown),
* a set of oddities were fixed with initial parsing of project files,
* a crash was fixed when closing KDevelop directly after opening a large project (427387, 427386),
* a crash was fixed in some cases when launching an executable (399511, 416874)
* improved support for changing the CMake\_BUILD\_TYPE in projects (429605)
* a crash was fixed when the problem reporter plugin was turned off during runtime,
* various other small bug fixes were added, and
* various smaller performance improvements were done.


As usual, the source code and prebuilt binaries can be downloaded from <https://kdevelop.org/download>. Note that the AppImage is now using a new Centos 7 based build process. Please report any issues you have with it on our bug tracker: <https://bugs.kde.org>


