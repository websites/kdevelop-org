---
title: "KDevelop master now depends on KDE Frameworks 5!"
date: "2014-08-27 23:23:25+00:00"
lastmod: "2016-02-28 21:10:54+00:00"
author: "milian"
aliases:
- /frameworks/kdevelop-master-now-depends-kde-frameworks-5
tags:
- kde
- frameworks
- master
categories:
- News
---
Good news everyone!

Our master branches are now officially depending on KDE Frameworks 5 and thus also Qt 5. People who build KDevelop regularly from the git master branches are suggested to switch to the stable 4.7/1.7 branches for the short term. The release of the final KDevelop 4.7.0 will be announced in the next days. This will be the last KDE4 based feature release of KDevelop. All future development will be targeted at the new Qt5 version.

Developers who want to contribute to the frameworks version of KDevelop are invited to do so. Any help is welcome and many simple tasks and low-hanging porting fruits are waiting to be picked. While KDevelop and KDevplatform are mostly done, we also have many external plugins that are not yet ported to KF5, such as kdev-qmake, kdev-php, kdev-python and others. The initial porting step to get a plugin to compile is often very trivial. And [good documentation](http://www.proli.net/2014/06/21/porting-your-project-to-qt5kf5/) [is available](https://community.kde.org/Frameworks/Porting_Notes), as well as [a collection of porting scripts](http://quickgit.kde.org/?p=kde-dev-scripts.git&a=tree&h=9a9e757e6f99c01d102a01187a05e5742e87c839&hb=37b29e4fb257f8fa361510b0c5efaca7603aa855&f=kf5).

For more information, please refer to the announcement by Kevin: http://mail.kde.org/pipermail/kdevelop-devel/2014-August/048693.html

Enjoy! I for one welcome our new Qt5/KF5 overlords :)