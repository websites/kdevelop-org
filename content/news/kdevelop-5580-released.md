---
title: "KDevelop 5.6 beta 1 released"
date: "2020-08-17 16:16:32+00:00"
lastmod: "2020-08-17 17:39:21+00:00"
author: "kossebau"
aliases:
- /news/kdevelop-5580-released
tags:
- release
categories:
- News
---
We are happy to announce the release of KDevelop 5.6 Beta 1!


5.6 as a new feature version of KDevelop will bring half a year of small improvements to features across the application. Full details will be given in the announcement of the KDevelop 5.6.0 release, which is currently scheduled for in 3 weeks.


Should you have any remarks or in case you find any issues in KDevelop 5.6 Beta 1, please let us [know](https://www.kdevelop.org/support).


## Downloads


You can find the Linux AppImage ([learn about AppImage](https://appimage.org/)) here: [KDevelop 5.6 beta 1 AppImage (64-bit)](https://download.kde.org/unstable/kdevelop/5.5.80/bin/linux/KDevelop-5.5.80-x86_64.AppImage) (verify by: [GPG signature](https://download.kde.org/unstable/kdevelop/5.5.80/bin/linux/KDevelop-5.5.80-x86_64.AppImage.sig), keys linked on [Download](/download) page)
Download the file and make it executable (chmod +x KDevelop-5.5.80-x86\_64.AppImage), then run it (./KDevelop-5.5.80-x86\_64.AppImage).


The source code can be found here: [KDevelop 5.6 beta 1 source code](https://download.kde.org/unstable/kdevelop/5.5.80/src/)


Windows installers are currently not offered, we are looking for someone interested to take care of that.


