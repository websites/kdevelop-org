---
title: "Mailing Lists Moved to KDE Infrastructure"
date: "2012-11-29 19:05:51+00:00"
lastmod: "2016-02-28 21:10:54+00:00"
author: "milian"
aliases:
- /news/mailing-lists-moved-kde-infrastructure
tags:
- mailing lists
- kdevelop
- community
- kde
- infrastructure
categories:
- News
---
Hello all!

Today it seems as if our mailing list server has died. Thankfully, we where in the process of moving them over to the KDE infrastructure in conformance to the [KDE manifesto](http://manifesto.kde.org/).

You can find the new addresses on the [mailing list](/mailinglists) site.

**Note:** No users where migrated, thus you have to register again! To do that, sent an email to kdevelop-request@kde.org and/or kdevelop-devel-request@kde.org with the subject `subscribe`.

Thanks to the [University of Potsdam](http://www.cs.uni-potsdam.de/) for hosting the mailing lists until now. And many thanks to the awesome KDE sysadmin team for providing us with the required infrastructure!