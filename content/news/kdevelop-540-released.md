---
title: "KDevelop 5.4 released"
date: "2019-08-06 10:00:00+00:00"
lastmod: "2020-02-02 02:33:11+00:00"
author: "kossebau"
aliases:
- /news/kdevelop-540-released
tags:
- release
categories:
- News
---
We are happy to announce the availability of KDevelop 5.4 today featuring support for a new build system, a new scratchpad feature, analyzer support from Clang-Tidy plus a bunch of bug fixes and wee improvements.


## Meson


Projects using the new rising star in the build system scene, [Meson](https://mesonbuild.com/), can now also be managed with KDevelop, thanks to the work of Daniel Mensinger.


Current features are:


1. Native support for Meson projects (configuring, compiling, installing)
2. Support for KDevelop code autocompletion (plugin reads Meson introspection information)
3. Initial support for the [Meson rewriter](http://mesonbuild.com/Rewriter.html): modifying basic aspects of the project (version, license, etc.)


![Meson project configuration](/sites/www.kdevelop.org/files/inline-images/meson.png)


Support for adding / removing files from a build target will follow in future releases of KDevelop.


### Scratchpad


Thanks to the work of Amish Naidu there is now a tool to keep "scratches" of code or text to experiment or quickly run something without the need to create a full project.


The plugin adds a new tool view, which maintains a list of your scratches which you can compile and run. The data from scratches is managed and stored by KDevelop internally but is presented as regular documents in the editor giving all the editing convenience of e.g. code-completion and diagnostics. Commands used to run the scratches are saved for each scratch, while new scratches are pre-set with the last command used for that file type.


![Scratchpad](/sites/www.kdevelop.org/files/inline-images/scratchpad.png)


### Clang-Tidy


The plugin for [Clang-Tidy](http://clang.llvm.org/extra/clang-tidy/) had been developed and released independently so far, but starting with version 5.4 is now part of KDevelop's default plugins. Learn more about the plugin on [Friedrich Kossebau's blog](https://frinring.wordpress.com/2018/08/30/improve-your-cpp-code-in-kdevelop-with-clang-tidy/).


## C++


More work was done on stabilizing and improving our C++ language support, which uses a Clang based backend. Notable fixes include:


* Add working directory to clang parser. ([commit.](https://commits.kde.org/kdevelop/ad7ea392d9e25019cf9967278b332b61c299d94e) code review [D22197](https://phabricator.kde.org/D22197))
* Clang Plugin: Report some problems from included files. ([commit.](https://commits.kde.org/kdevelop/4c1a518211d9abd092a2afebb8090f67884d3061) code review [D18224](https://phabricator.kde.org/D18224))
* Make it possible to select -std=c++2a for our language support. ([commit](https://commits.kde.org/kdevelop/8200efc01e425a117690414cfeda0b7dd6294dac))
* Rename c++1z to C++17. ([commit](https://commits.kde.org/kdevelop/742cef92d22d0074b79b8583d67978a47b97a273))
* Clang CodeCompletion: No auto-completion for numbers. ([commit.](https://commits.kde.org/kdevelop/e86c5a7211db13e40b1ba98235b49423d45ade34) code review [D17915](https://phabricator.kde.org/D17915))
* Add assistant to generate header guards. ([commit.](https://commits.kde.org/kdevelop/a54c46524b8ea38f55041e164e0ece63a0836904) code review [D17370](https://phabricator.kde.org/D17370))


## PHP


* Always set maximum file size for internal parse job. ([commit](https://commits.kde.org/kdev-php/8ebdd796c7ffc5b6adbb8834f56a1c9efa3bf72a))
* Bypass the 5 MB maximum file size limit for the phpfunctions.php internal file. ([commit](https://commits.kde.org/kdev-php/3431ef364495f19374806d7ffc8e3ba614f20e92))
* Fix linking with ld.lld. ([commit](https://commits.kde.org/kdev-php/8c3648a4a8eea95701881affa41d06bd95f45c54))


## Python


The developers have been concentrating on fixing bugs, which already have been added into the 5.3 series.


There are no new features compared to 5.3.


## Other Changes


* [Documentation] Set size policy of providers combobox to AdjustToContents ([commit](https://commits.kde.org/kdevelop/91191b8766dc83e7eb239bfb0a920bbb0cf4f9b1))
* Contextbrowser: Remove 'separated by only whitespace' possibility for showing the problem tooltip. ([commit](https://commits.kde.org/kdevelop/626954a45ab06fb6318ea991af613cd8845edd6e))
* Contextbrowser: Minor improvement to tooltip showing behavior. ([commit](https://commits.kde.org/kdevelop/c1e8604d7bf218ca212cefc058ce44736fe38aa4))
* CMake plugin: Also show an error message if the CMake configuration becomes invalid due to a change, and add an instruction to reload the project manually. ([commit](https://commits.kde.org/kdevelop/271257152f8fb29b2e64bc9b4ff44d5168995914))
* CMake plugin: Show a message box if configuration fails. ([commit](https://commits.kde.org/kdevelop/1ee9f8d5e958ddffcc53bdb0b2e22a11ee7b9e9a))
* Projectfilter: Include .clang-format by default. ([commit](https://commits.kde.org/kdevelop/58e6d4c60142552faf55b955b27574b14a281c72))
* Add a predefined clang-format custom script formater. ([commit](https://commits.kde.org/kdevelop/88861cafb369a62e8e6eb8a9c60fe42ace9d8834))
* Fix code completion for nameless structs/unions with the same member. ([commit.](https://commits.kde.org/kdevelop/d1f58562046494b78e17c50c2fdab5c2800d9aaf) fixes bug [#409041](https://bugs.kde.org/409041). code review [D22455](https://phabricator.kde.org/D22455))
* Support newer kdebugsettings .categories file format. ([commit](https://commits.kde.org/kdevelop/3baa2327a8d0d961720faf24db294109ddaa7928))
* Show session name in the Delete Session confirmation dialog. ([commit.](https://commits.kde.org/kdevelop/c133947b772611fa4a5fadbf67ba54165e32f482) code review [D22456](https://phabricator.kde.org/D22456))
* Remove invalid check from test\_projectload test. ([commit.](https://commits.kde.org/kdevelop/01e62e6cdcbb1c1c3e770afd2a20e09dd83a9ebb) code review [D22350](https://phabricator.kde.org/D22350))
* Document tree view close on middle button. ([commit.](https://commits.kde.org/kdevelop/4b5ff487f926fda2711bbb67c7c084c1bf48e457) code review [D22160](https://phabricator.kde.org/D22160))
* Follow KTextEditor changes for hidpi rendering of icon border. ([commit](https://commits.kde.org/kdevelop/0342ae5bfe427b2bdbe74e9a7ed959ac8491d116))
* Note visibilty tag also with signature of friend-declared method. ([commit](https://commits.kde.org/kdevelop/b3dbf57a20a44413a28006dc33f369fc688db017))
* Guard against crashes when IStatus object gets destroyed at bad times. ([commit](https://commits.kde.org/kdevelop/bd048e67f056b5be25ed57fb2be947444f68c24e))
* Attempt to fix a crash on shutdown. ([commit](https://commits.kde.org/kdevelop/dedcba67680a937113c337091e429881cda2c721))
* Astyle: support the system astyle library. ([commit.](https://commits.kde.org/kdevelop/b2d202225cdbbb22a6f15e4dfa4b68a6fec28636) code review [D17760](https://phabricator.kde.org/D17760))
* Renovate kdevelop bash completion file. ([commit](https://commits.kde.org/kdevelop/4987b64db834aa4226ef4121d0249fa61b9b8b2f))
* Fix deadlock exception in FileManagerListJob. ([commit](https://commits.kde.org/kdevelop/73fb3cd625b80705455d86f8b9919bb2991177ab))
* DVCS Branch Manager with filtering and sorting proposal. ([commit.](https://commits.kde.org/kdevelop/8f77f44b67c750865d0ac5e6b23ffaba52017c31) code review [D20142](https://phabricator.kde.org/D20142))
* Also find clang include path based on runtime libclang library path. ([commit](https://commits.kde.org/kdevelop/bc13f955f3f087f2c6c5b6e8ad299f3ca6bb1142))
* TestFile: On destruction, close associated document if open and stop the background parser. ([commit.](https://commits.kde.org/kdevelop/4acac3f79695b8fe0be93e0a8d503ec46be89b62) code review [D18567](https://phabricator.kde.org/D18567))
* CMake: discover more unit tests. ([commit.](https://commits.kde.org/kdevelop/d5e3e27bb2720d84adda95de07a728a9e61c866f) fixes bug [#405225](https://bugs.kde.org/405225). code review [D19673](https://phabricator.kde.org/D19673))
* Be less restrictive with failures while searching for LLVM. ([commit](https://commits.kde.org/kdevelop/b576771d0f8fe28b38886bdcc872932b7bfe6331))
* Allow the maximum file size of parse jobs to be configurable. ([commit](https://commits.kde.org/kdevelop/ecf5548543a408773e61274b3e3e2776c1eb7833))
* Optimize CMakeBuildDirChooser::buildDirSettings(). ([commit.](https://commits.kde.org/kdevelop/5bf2c3aae6ecfbbe9016d746d5185f39712a8e15) code review [D18857](https://phabricator.kde.org/D18857))
* [Sessions Runner] Use icon name. ([commit.](https://commits.kde.org/kdevelop/3c2725a67d3aa147885ed148b508d7399702350a) code review [D19159](https://phabricator.kde.org/D19159))
* Don't eat the backspace event when no alt modifier is set. ([commit](https://commits.kde.org/kdevelop/e2b2fd2c1d22123e6192ab6cfa790549f9c793b0))
* "Reparse Entire Project" action for the ProjectController. ([commit.](https://commits.kde.org/kdevelop/4ae0f487b9317dbe1bcfd143342fe94c60f69ecd) code review [D11934](https://phabricator.kde.org/D11934))
* Introduce QuickOpenEmbeddedWidgetCombiner. ([commit](https://commits.kde.org/kdevelop/d284d5bb5f0908fcc7462309992b640357cc0e22))
* Add 'back' to QuickOpenEmbeddedWidgetInterface. ([commit](https://commits.kde.org/kdevelop/35277af94498f76dad4737d43f3fb5c08e35570f))
* Update documentation: the keyboard shortcuts use ALT not SHIFT. ([commit](https://commits.kde.org/kdevelop/db9740ef9ae1726522d0046596dc0bd5292ba26b))
* Fix up/down keyboard navigation for 'Show documentation' links. ([commit](https://commits.kde.org/kdevelop/f633da0d9c678764de563c5d3516eab934dd465e))
* Lock duchain in AbstractIncludeNavigationContext::html. ([commit](https://commits.kde.org/kdevelop/38e910bab25c50ae96140933be7b6baaf51f085f))
* Don't crash when background listing outlasts file manager list job. ([commit](https://commits.kde.org/kdevelop/a0fd2014489cf3f5391b7c9b88959a7033dbced9))
* Don't crash when project is closed before it was fully opened. ([commit](https://commits.kde.org/kdevelop/3aed7ac41a67d6686dcfcdac6df616ed5906badf))
* Make sure we use the same compiler settings as the project is by default. ([commit.](https://commits.kde.org/kdevelop/62ae1929c5a5d1a6a200a07579a4cca635cd9364) code review [D11136](https://phabricator.kde.org/D11136))
* Debugger plugin fixes. ([commit.](https://commits.kde.org/kdevelop/02b042a923554a6530b03c5f3328b81322ba0877) code review [D18325](https://phabricator.kde.org/D18325))
* Kdevelop-msvc.bat finds VS-2017 based on a registry key on Windows. ([commit.](https://commits.kde.org/kdevelop/7e4294a8782402e5e2766646c9bc9fe35c778ade) code review [D17908](https://phabricator.kde.org/D17908))
* CMakeBuildDirChooser: avoid calling deprecated KUrlRequester::setPath(). ([commit.](https://commits.kde.org/kdevelop/4a219756059ecfd5a98ded0abc7c77884ee8081f) code review [D18856](https://phabricator.kde.org/D18856))
* Flatpak+cmake: put the cmake build directories into .flatpak-builder. ([commit](https://commits.kde.org/kdevelop/df9ce19157624920bc7847b347ab65ab3cdbc7bf))
* Allow KDEV\_DEFAULT\_INSTALL\_PREFIX specify a default install prefix. ([commit](https://commits.kde.org/kdevelop/0b39ed0de3f4e24f0abca3a2ad03468c2c109623))
* Flatpak: Improve runtime title. ([commit](https://commits.kde.org/kdevelop/da4e28ba8fcd7bea05c6d0413af3ac95b26b19d6))
* Adapt indentation mode after a new project was opened. ([commit](https://commits.kde.org/kdevelop/46a105b4bf1307740933e1327b36e25de376e7d0))
* Flatpak: Fix listing runtimes. ([commit](https://commits.kde.org/kdevelop/1eaf0a2e32d393dc54b2432e1c283054f505f9f7))
* Workaround the bug found by ASan, which can be seen on FreeBSD CI. ([commit.](https://commits.kde.org/kdevelop/fbb955c772ec619310ada6a5420a2b7c44b71ecd) code review [D18463](https://phabricator.kde.org/D18463))
* Properly cleanup FileManagerListJob when folder items are deleted. ([commit.](https://commits.kde.org/kdevelop/06b526e81f9f9065d1725b4f35985e71dc0dc2e3) fixes bug [#260741](https://bugs.kde.org/260741))
* Provide debugger name and pid when registering a debugger to DrKonqi. ([commit.](https://commits.kde.org/kdevelop/7789fcd7be81d85eaa8af7dd4254a476811e37a4) code review [D18511](https://phabricator.kde.org/D18511))
* Support for indent-after-parens astyle option. ([commit.](https://commits.kde.org/kdevelop/a2ab30ed7c09ec7cdcaceb90c268b4ca9b7f69f3) code review [D18371](https://phabricator.kde.org/D18371))
* Fix bug 389060 (Heaptrack analysis keeps firing /usr/bin/plasmoidviewer). ([commit.](https://commits.kde.org/kdevelop/7fd559bba4b6d401667a1b3f75d5e3e205986480) fixes bug [#389060](https://bugs.kde.org/389060). code review [D15565](https://phabricator.kde.org/D15565))
* Contextbrowser: Ability to show combined problems and decl tooltip. ([commit.](https://commits.kde.org/kdevelop/2071db63b3fb4172f112356b6d531aae75de5da8) code review [D18229](https://phabricator.kde.org/D18229))
* Properly display argument names of template functions. ([commit.](https://commits.kde.org/kdevelop/49d503f8b9c1f79c6bdf312685cae7554a5cba34) code review [D18218](https://phabricator.kde.org/D18218))
* Show size and alignment information in tooltips for typedef or alias. ([commit.](https://commits.kde.org/kdevelop/be49033105287848cb43eb33fe961c59155a01f3) code review [D18097](https://phabricator.kde.org/D18097))
* GrepView: Extend default file extensions to search. ([commit.](https://commits.kde.org/kdevelop/16c8d3c661a753b6e4fcfda0faf63d566e38d0a8) Implements feature [#402207](https://bugs.kde.org/402207). code review [D17892](https://phabricator.kde.org/D17892))
* Fix crash in documentation view. ([commit.](https://commits.kde.org/kdevelop/7419d27c604bb0e0e321946d8bddd84ba87f86ed) fixes bug [#402026](https://bugs.kde.org/402026))
* [clang-tidy] Fix context-menu crash for files not in a project. ([commit.](https://commits.kde.org/kdevelop/df225e7795cc986f9b5817ca995b235fe970f5d7) fixes bug [#401917](https://bugs.kde.org/401917))
* Polish Flatpak integration. ([commit](https://commits.kde.org/kdevelop/4ff8f6c4e4f3693f9c07177d225f3484f67345f3))
* Don't add 'override' specifier for non-modern project settings. ([commit.](https://commits.kde.org/kdevelop/0636cfdd8a93617cbcf2efe2c9d74eab21a0b97b) fixes bug [#372280](https://bugs.kde.org/372280). code review [D16773](https://phabricator.kde.org/D16773))
* [clang-tidy] Disable/Block Run actions in projects without buildsystem manager. ([commit](https://commits.kde.org/kdevelop/a2997933a806977c59523e2f7c0e85659868f8ed))
* Add VcsAnnotationItemDelegate, for control of rendering and tooltip. ([commit.](https://commits.kde.org/kdevelop/94751dcd009ba38cd4f16705ed4a5395c8cab374) code review [D8709](https://phabricator.kde.org/D8709))
* Qmljs: Update qmljs from QtCreator v4.7.2. ([commit](https://commits.kde.org/kdevelop/f420ee1e66fd95184f7f7979a6e433cb8197fc13))
* LoadedPluginsDialog: Fix initial size. ([commit](https://commits.kde.org/kdevelop/980f548da1e1a7dcf870e6312de3c8876212b171))
* Place cursor after opening brace for function implementation. ([commit.](https://commits.kde.org/kdevelop/48e0f084ab740f176ff68ab760597dbc690ec0c1) code review [D16386](https://phabricator.kde.org/D16386))
* Replace leading typed text when completing function implementation. ([commit.](https://commits.kde.org/kdevelop/4f2fc9e32452b3dc7f017d57c0bcc98b73cb0370) fixes bug [#384710](https://bugs.kde.org/384710). code review [D16326](https://phabricator.kde.org/D16326))
* Fix crashes when document gets destroyed directly after load. ([commit](https://commits.kde.org/kdevelop/8f9f8d1c4452f8d9291ae530f3720511fd203aef))
* Prevent QWebEngine from overriding signal handlers. ([commit.](https://commits.kde.org/kdevelop/ef0af08a9d4889ec5295a788900d1f38af67bb8a) code review [D16188](https://phabricator.kde.org/D16188))
* Add missing break in QmlJs code completion. ([commit](https://commits.kde.org/kdevelop/6b8912678e2faa6ae1771a86002c802a49b9d165))
* CMake: fix missing addition of policies to documentation index. ([commit.](https://commits.kde.org/kdevelop/2f0f3da749217bb0e9c2b5de9e4b662a3e549129) code review [D15882](https://phabricator.kde.org/D15882))
* Create action to jump to the current execution line in debug mode. ([commit.](https://commits.kde.org/kdevelop/59d823957de8f685d4cf94919bca2738328aa197) Implements feature [#361411](https://bugs.kde.org/361411). code review [D14618](https://phabricator.kde.org/D14618))
* Fix segfaults in OutputWidget. ([commit.](https://commits.kde.org/kdevelop/f856a9a63910a7e9f2fbcbcd63f85100599805ae) fixes bug [#398615](https://bugs.kde.org/398615). code review [D15326](https://phabricator.kde.org/D15326))
* Fix double delete bug in OutputWidget. ([commit.](https://commits.kde.org/kdevelop/7b2e5e404254fa9eed28d2e8b6f32c1dfe7f315c) code review [D15241](https://phabricator.kde.org/D15241))
* Cleanup Perforce test case, and thereby its output a little. ([commit.](https://commits.kde.org/kdevelop/2e98de947febd5297475c22b35ac390ed693de5b) code review [D14959](https://phabricator.kde.org/D14959))


## Get it


Together with the source code, we again provide a pre-built one-file-executable for 64-bit Linux as an AppImage. You can find it on our [download page](https://www.kdevelop.org/download).


The 5.4.0 source code and signatures can be downloaded from [download.kde.org](https://download.kde.org/stable/kdevelop/5.4.0/src/).


Should you find any issues in KDevelop 5.4, please let us know on the [bug tracker](https://bugs.kde.org/enter_bug.cgi?format=guided&product=kdevelop).


