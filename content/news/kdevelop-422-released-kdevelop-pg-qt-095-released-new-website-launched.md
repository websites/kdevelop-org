---
title: "KDevelop 4.2.2 released, KDevelop-PG-Qt 0.9.5 released, new website launched"
date: "2011-04-08 09:44:21+00:00"
lastmod: "2016-02-28 21:10:54+00:00"
author: "milian"
aliases:
- /kdevelop/kdevelop-422-released-kdevelop-pg-qt-095-released-new-website-launched
tags:
- release
- stable
- kdevelop
- php
- kdevelop-pg-qt
categories:
- News
---
After two months of hard work the KDevelop team is happy to announce the first bug-fix release of the KDevelop 4.2 branch. KDevelop 4.2.2 fixes dozens of bugs. Many thanks to all the contributors who made this possible. Every user is urged to upgrade to this new release. Packagers are notified, most people should hence just wait for an update in their distribution.


As usual this release also contains a new version of the PHP language plugin for KDevelop. New this time is another release of the build-dependency KDevelop-PG-Qt (the parser generator used in the PHP plugin). KDevelop-PG-Qt 0.9.5 fixes a severe bug and brings some other improvements. Please install this before you update your PHP language plugin.


### Download



KDevelop 4.2.2
[download KDevelop 4.2.2](http://download.kde.org/download.php?url=stable/kdevelop/4.2.2/src)
KDevelop-PG-Qt 0.9.5
[download KDevelop-PG-Qt 0.9.5](http://download.kde.org/download.php?url=stable/kdevelop-pg-qt/0.9.5/src)


PS: While this release bears the version 4.2.2, it is actually the first bug-fix release. The tarballs for version 4.2.1 where faulty, stay away from them!

### New Website



Finally, we are happy to announce that we finally re-launched [KDevelop.org](http://www.kdevelop.org) website. It runs on Drupal and should make it possible for us to keep it more up to date than the old website. Most of the obsolete KDevelop 3.5 content is gone, we will bring it back under a legacy domain. If you spot any issues with the website, or have suggestions - feel free to contact us. Furthermore, help is appreciated as always! Especially the documentation is now hosted on Userbase and Techbase - fell free to improve it!

So, thanks to all the contributors once more. Enjoy the new KDevelop release.
