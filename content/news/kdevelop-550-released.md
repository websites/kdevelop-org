---
title: "KDevelop 5.5 released"
date: "2020-02-02 20:02:00+00:00"
lastmod: "2020-04-30 11:37:11+00:00"
author: "kossebau"
aliases:
- /news/kdevelop-550-released
tags:
- release
categories:
- News
---
We are happy to announce the availability of KDevelop 5.5 today bringing half a year of work mainly on stability, performance, and future maintainability.


![KDevelop 5.5.0 in action](/sites/www.kdevelop.org/files/inline-images/KDevelop_5.5.0.png)
New features have not been added. The existing ones have received small improvements:


## Improved C++ language support


* Fix missing header guard warning for a standalone header being always present. ([commit](https://commits.kde.org/kdevelop/0698385be99492f8db37d30682fe38295c9f4f64))
* Don't crash when signatures don't match in AdaptSignatureAssistant. ([commit](https://commits.kde.org/kdevelop/2720f349d949d86dc0394b92d06d91af29692ea4))
* Clazy: add configurable predefined checkset selections. ([commit](https://commits.kde.org/kdevelop/12b7e57ed4a89ce564aba1eea0dfca27e9ca7015))
* Clang-tidy: add configurable predefined checkset selections. ([commit](https://commits.kde.org/kdevelop/2271921d6a82cdc5cfd792f1dc372c8fd9d55bd3))
* Don't get confused when encountering parse errors in default args. ([commit](https://commits.kde.org/kdevelop/e3883a89853ffe6456e2fb595b69bd50668f6e85). See bug [#369546](https://bugs.kde.org/369546))
* Fix ClangUtils::getDefaultArguments when encountering macros. ([commit](https://commits.kde.org/kdevelop/54585bcd0de5ca4ce0618accfa39967f6c7d22f0). fixes bug [#369546](https://bugs.kde.org/369546))
* Skip clang-provided override items from code completion. ([commit](https://commits.kde.org/kdevelop/f94e6d297673874d3d7253cbdd43884d1e719afd))
* Unbreak move-into-source for non-class functions. ([commit](https://commits.kde.org/kdevelop/275d8778bdc963e3d2dd2e23680069044d713b72))
* Lambda init captures are visited starting with clang 9.0.0. ([commit](https://commits.kde.org/kdevelop/84952f498d2eb014d6882fd582e167bdd5e5a5a0))
* Try a bit harder to find types for look-ahead completion. ([commit](https://commits.kde.org/kdevelop/9cbb6f2f84f92d8ee43c8aa5130fa82fa6efcd48))


## Improved PHP language support


* Fix uses of function call parameters after closures. ([commit](https://commits.kde.org/kdev-php/893d7c8dfacad72b21b23b93cee665f6bc08e4cb))
* Add support for PHP 7.4's typed properties. ([commit](https://commits.kde.org/kdev-php/54964af00e7e1e067a27e81ec26f2aa9dec8d7fb). code review [D26254](https://phabricator.kde.org/D26254))
* Support importing functions and constants from other namespaces. ([commit](https://commits.kde.org/kdev-php/01bee88947f6dc2a6ce7f2bb46d26d716c6f8ec6). fixes bug [#408609](https://bugs.kde.org/408609). code review [D25956](https://phabricator.kde.org/D25956))
* Fix rename of a variable. ([commit](https://commits.kde.org/kdev-php/f68154391a30139a8763c09fe1d103ab13565479). fixes bug [#317879](https://bugs.kde.org/317879). code review [D25587](https://phabricator.kde.org/D25587))
* Add support for "array of type". ([commit](https://commits.kde.org/kdev-php/53f30e5b9b2b45fc8c50724cd71a1b8e4f9d9aed). code review [D24921](https://phabricator.kde.org/D24921))
* Add support for class constant visibility. ([commit](https://commits.kde.org/kdev-php/4c46f097425cfa10296b1eb6c926cd3e524b68fe))
* I18n: update message to new default. ([commit](https://commits.kde.org/kdev-php/32f7a5717cde21d748cb457f60b0c13b61c1a98a))


## Improved Python language support


* Initial Python 3.8 support. ([commit](https://commits.kde.org/kdev-python/e23fa8f15af89a8bd4bd84dd96b5fd7017457516). fixes bug [#411956](https://bugs.kde.org/411956))


## Other Changes


* Welcome page: remove background in active window when plugin is disabled. ([commit](https://commits.kde.org/kdevelop/d06877ce1895425bcc140cefacd0ce13e72fc4f8))
* No longer install modeltest.h, not used externally and deprecated. ([commit](https://commits.kde.org/kdevelop/575be48bbd3a739d094b7637f3cbf700a3a6c056))
* Fix "invalid project name" hint not always showing. ([commit](https://commits.kde.org/kdevelop/49cf67b4dae78580ce30b7c73b5b86c5481efb09))
* Use default scheme option of KColorSchemeManager if available. ([commit](https://commits.kde.org/kdevelop/5634d3a8a9f8598ad5f39efb825d60acb0e37a9e))
* Read the global color scheme name from its file. ([commit](https://commits.kde.org/kdevelop/03bed527b45a3cab17f58fcb24b02a4ce82cdcf7))
* Fix qmljs comment parsing. ([commit](https://commits.kde.org/kdevelop/0a6c229a678e6f02b2319c9729e5df97fc11b730))
* This fixes the comment formatting for the Doxygen variants:. ([commit](https://commits.kde.org/kdevelop/0efef224fed4cd115dddce3e7cad5f4938f27faf))
* Qmakebuilder: remove unused kcfg files. ([commit](https://commits.kde.org/kdevelop/e962535f6b671583d7a78e90b0fce72b765937b4))
* Fix reformat for long strings. ([commit](https://commits.kde.org/kdevelop/8325944825d6d52d4d0879e57db6bb2c1e240347))
* Introduce shell-embedded message area, to avoid dialog windows. ([commit](https://commits.kde.org/kdevelop/7a1edb7676d8ec10639448330b554c9806e05d2a))
* Clazy, clang-tidy: share code via new private KDevCompileAnalyzerCommon. ([commit](https://commits.kde.org/kdevelop/ae56043f0d232f0c66c2313bfbba30af9acb1c99))
* Make tar archives reproducible by setting Pax headers. ([commit](https://commits.kde.org/kdevelop/25e78ffa27387cf2786f35a15c0ace0fb6684012). code review [D25494](https://phabricator.kde.org/D25494))
* Kdevplatform: remove About data feature. ([commit](https://commits.kde.org/kdevelop/381aec18ce775aa1fa6e7b6cee6f332757928b7c))
* Support for rebasing. ([commit](https://commits.kde.org/kdevelop/100d3e6656dba6ec39c499dfb3e5bb7cb1ae6892))
* Add a setting to disable the close buttons on tabs. ([commit](https://commits.kde.org/kdevelop/91112821e869ecabe31a45cdb6a49e7aa3ad115f))
* CMake: Show project name in showConfigureErrorMessage. ([commit](https://commits.kde.org/kdevelop/0f53e240065de915de9229ce4699a17a6f947c67))
* TemplatePreview: Enable word-wrap for messagebox and Lines Policy Label. ([commit](https://commits.kde.org/kdevelop/1dda8e10bd2b5923083da1c46a15adb80ced4d66))
* Filetemplates: load and show tooltip for custom options. ([commit](https://commits.kde.org/kdevelop/0df7362829436926d839b1e80efabf95dd62183b))
* Pass environment variables from process environment and set up with flatpak environment. ([commit](https://commits.kde.org/kdevelop/360bf1b51ca42238763577c13073b4de945b3630))
* Remove usage of columns argument in arch detection since old LTS systems may not have that flag. ([commit](https://commits.kde.org/kdevelop/f4b32496685836228dc5b31e84630d7edb189293))
* Pass the android toolchain file path to CMake as a local file path not as a URI. ([commit](https://commits.kde.org/kdevelop/815a84a7a59c1b0c47d92d348606d0184f25f39f). code review [D21936](https://phabricator.kde.org/D21936))
* Formater: Hide KTextEditor minimap for the formater preview. ([commit](https://commits.kde.org/kdevelop/17673b0d274831df45d026defd770e695ac0c848))
* Shell: use KAboutPluginDialog in LoadedPluginsDialog. ([commit](https://commits.kde.org/kdevelop/faded356b6af83e931e4f0cca4b5c8de73f5bea5))
* Mention all fetch project sources in the documentation. ([commit](https://commits.kde.org/kdevelop/eb913c94d29cab3b03cf5e8bb84757554a315aa8). fixes bug [#392550](https://bugs.kde.org/392550). code review [D25342](https://phabricator.kde.org/D25342))
* Script launcher: add env profile configure dialog button to config UI. ([commit](https://commits.kde.org/kdevelop/c325a78dc39c4204cde465d31f125f7ec24a23f6). fixes bug [#410914](https://bugs.kde.org/410914))
* Cmake: FindClang: Detect llvm-project.git checkout. ([commit](https://commits.kde.org/kdevelop/ad0cbbb82d8d8a57412048ec2585c5ea84fcf41b))


## Get it


Together with the source code, we again provide a pre-built one-file-executable for 64-bit Linux as an AppImage. You can find it on our [download page](https://www.kdevelop.org/download).


The 5.5.0 source code and signatures can be downloaded from [download.kde.org](https://download.kde.org/stable/kdevelop/5.5.0/src/).


Should you have any remarks or in case you find any issues in KDevelop 5.5, please let us [know](https://www.kdevelop.org/support).


