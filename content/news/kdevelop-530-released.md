---
title: "KDevelop 5.3 released"
date: "2018-11-14 09:00:00+00:00"
lastmod: "2020-02-02 02:31:56+00:00"
author: "kossebau"
aliases:
- /news/kdevelop-530-released
tags:
- release
- 5.3
categories:
- News
---
A little less than a year after the release of KDevelop 5.2 and a little more than [20 years after KDevelop's first official release](https://frinring.wordpress.com/2018/09/22/happy-20th-anniversary-kdevelop/), we are happy to announce the availability of KDevelop 5.3 today. Below is a summary of the significant changes.


We plan to do a 5.3.1 stabilization release soon, should any major issues show up.


## Analyzers


With 5.1, KDevelop got a new menu entry *Analyzer* which features a set of actions to work with analyzer-like plugins. For 5.2 the runtime analyzer Heaptrack and the static analyzer cppcheck have been added. During the last 5.3 development phase, we added another analyzer plugin which now is shipped to you out of the box:


### Clazy


[Clazy](https://cgit.kde.org/clazy.git/about/) is a clang analyzer plugin specialized on Qt-using code, and can now also be run from within KDevelop by default, showing issues inline.


[![Screenshot of KDevelop with dialog for project-specific Clazy settings, with clazy running in the background](/sites/www.kdevelop.org/files/inline-images/KDevelop%3A%20Clazy%20run%20on%20Okular.png)](/sites/www.kdevelop.org/files/inline-images/KDevelop%3A%20Clazy%20run%20on%20Okular.png)
The [KDevelop plugin for Clang-Tidy support](https://cgit.kde.org/kdev-clang-tidy.git/about/) had been developed and released independently until after the feature freeze of KDevelop 5.3. It will be released as part of KDevelop starting with version 5.4.


## Internal changes


With all the Analyzers integration available, KDevelop's own codebase has been subject for their application as well. Lots of code has been optimized and, where indicated by the analyzers, stabilized. At the same time modernization to the new standards of C++ and Qt5 has been continued with the analyzers aid, so it can be seen only in the copyright headers KDevelop was founded in 1998 .


## Improved C++ support


A lot of work was done on stabilizing and improving our clang-based C++ language support. Notable fixes include:


* Clang: include tooltips: fix range check. ([commit.](https://commits.kde.org/kdevelop/83d81680979b3c6b83afad4242db0933f06ed7ad) code review [D14865](https://phabricator.kde.org/D14865))
* Allow overriding the path to the builtin clang compiler headers. ([commit.](https://commits.kde.org/kdevelop/169371b5d3448949122d5846c7da29ab86b1a0e9) See bug [#393779](https://bugs.kde.org/393779))
* Always use the clang builtin headers for the libclang version we use. ([commit.](https://commits.kde.org/kdevelop/c791359763cd862f93ad9f91fa628d20e1e630f2) fixes bug [#387005](https://bugs.kde.org/387005). code review [D12331](https://phabricator.kde.org/D12331))
* Group completion requests and only handle the last one. ([commit.](https://commits.kde.org/kdevelop/e48c6265e844c8591357fe68e91a6738346f5f35) code review [D12298](https://phabricator.kde.org/D12298))
* Fix Template (Class/Function) Signatures in Clang Code Completion. ([commit.](https://commits.kde.org/kdevelop/782645190f44ca1dd147e45d18fc354ef6282186) fixes bug [#368544](https://bugs.kde.org/368544). fixes bug [#377397](https://bugs.kde.org/377397). code review [D10277](https://phabricator.kde.org/D10277))
* Workaround: find declarations for constructor argument hints. ([commit.](https://commits.kde.org/kdevelop/113deb25ac772ed2040e3fd0fec1a1a9b82adb95) code review [D9745](https://phabricator.kde.org/D9745))
* Clang: Improve argument hint code completion. ([commit.](https://commits.kde.org/kdevelop/203b02d480ffba4db731544643f8ac7ddd07311e) code review [D9725](https://phabricator.kde.org/D9725))


## Improved PHP language support


Thanks to Heinz Wiesinger we've got many improvements for the PHP language support.


* Much improved support for PHP Namespaces
* Added support for Generators and Generator delegation
* Updated and expanded the integrated documentation of PHP internals
* Added support for PHP 7's context sensitive lexer
* Install the parser as a library so it can be used by other projects (currently, umbrello can use it) (Ralf Habacker)
* Improved type detection of object properties
* Added support for the object typehint
* Better support for ClassNameReferences (instanceof)
* Expression syntax support improvements, particularly around 'print'
* Allow optional function parameters before non-optional ones (Matthijs Tijink)
* Added support for magic constants \_\_DIR\_\_ and \_\_TRAIT\_\_


## Improved Python language support


The developers have been concentrating on fixing bugs, which already have been added into the 5.2 series.


There are a couple of improved features in 5.3:


* Inject environment profile variables into debug process environment. ([commit.](https://commits.kde.org/kdev-python/6b7846263088b1c1ec803c445a1315dfa029f311) fixes bug [#322477](https://bugs.kde.org/322477). code review [D14870](https://phabricator.kde.org/D14870))
* Improve support for 'with' statements. ([commit.](https://commits.kde.org/kdev-python/d619a731dbcdc724630118d521583be41f0cf7d3) fixes bug [#399533](https://bugs.kde.org/399533), [#399534](https://bugs.kde.org/399534). code review [D16085](https://phabricator.kde.org/D16085))


## Support for other platforms


KDevelop is written with portability in mind, and relies on Qt for solving the big part there, so next to the original "unixoid" platforms like Linux distributions and the BSD derivatives, other platforms with Qt coverage are in good reach as well, if people do the final pushing. So far Microsoft Windows has been another supported platform, and there is some experimental, but maintainer-seeking support for macOS. Some porters of [Haiku](https://www.haiku-os.org/), the BeOS inspired Open Source operating system, have done porting as well, building on the work done for other Qt-based software. For KDevelop 5.3 the small patch still needed has been applied to KDevelop now, so the [Haiku ports recipe for KDevelop](https://github.com/haikuports/haikuports/tree/master/dev-util/kdevelop) no longer needs it.


KDevelop is already in the HaikuDepot, currently still at version 5.2.2. It will be updated to 5.3.0 once the release has happened.


[![KDevelop 5.3 Beta on Haiku](/sites/www.kdevelop.org/files/inline-images/KDevelop%205.3%20Beta%20on%20Haiku.png)](/sites/www.kdevelop.org/files/inline-images/KDevelop%205.3%20Beta%20on%20Haiku.png)
## Note to packagers


The Clazy support as mentioned above has a recommended optional runtime dependency, clazy, more specifically the clazy-standalone binary. Currently clazy is only packaged and made available to their users by a few distributions, e.g. Arch Linux, openSUSE Tumbleweed or OpenMandriva,


If your distribution has not yet looked into packaging clazy, please consider to do so. Next to enabling the Clazy support feature in KDevelop, it allows developers to easily fix and optimize their Qt-based software, resulting in a less buggy and more performant software again for you.


You can find more information in the the [release announcement](https://mail.kde.org/pipermail/kde-announce-apps/2018-September/005477.html) of the currently latest clazy release, 1.4.


## Get it


Together with the source code, we again provide a prebuilt one-file-executable for 64-bit Linux (AppImage), as well as binary installers for 32- and 64-bit Microsoft Windows. You can find them on our [download page](/download).


The 5.3.0 source code and signatures can be downloaded from [here](https://download.kde.org/stable/kdevelop/5.3.0/src/).


Should you find any issues in KDevelop 5.3, please let us know in the [bug tracker](https://bugs.kde.org/enter_bug.cgi?format=guided&product=kdevelop).


