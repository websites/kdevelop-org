---
title: "KDevelop 4.3.0 Beta 2 Released"
date: "2012-02-14 13:05:53+00:00"
lastmod: "2016-02-28 21:10:54+00:00"
author: "milian"
aliases:
- /43/kdevelop-430-beta-2-released
tags:
- release
- kdevelop
- beta
- 4.3
- unstable
categories:
- News
---
On the road to the final release of KDevelop 4.3.0, we - the KDevelop team - are proud to present you the second and probably last Beta release in the 4.3 series. It comes with additional performance improvements and bug fixes. Please try it out and give us feedback to ensure a smooth 4.3 release.

## Download


You can find the source code available to download on the KDE mirrors: [download KDevelop 4.3 Beta 2](http://download.kde.org/download.php?url=unstable/kdevelop/4.2.82/src/). There, you can also find the changelogs in case you wonder what exactly has happened since KDevelop 4.3 Beta 1.

## Hashes



To verify the correctness of your downloaded packages, you can use the following SHA1 and MD5 hashes:


kdevplatform-1.2.81.tar.bz2
MD5Sum: a5066f6f467dcdaa3c0e58dc8f6809f4
SHA1Sum: 7e46b291b5c2abaaf8095974ab3fc7e6654152dc
kdevelop-4.2.81.tar.bz2
MD5Sum: a2dd296408fd928a7ed320a8e5fb7f6a
SHA1Sum: 4fdc96af4a867740fb6656ebd01b09037dfd1dcb
kdevelop-custom-buildsystem-1.2.1.tar.bz2
MD5Sum: 9cce7ff6ae7ef9b9de62609d496e9b2c
SHA1Sum: dc6131999ae8845d49d23a9d58019840e1bac2c1
kdevelop-php-1.2.81.tar.bz2
MD5Sum: 86fb3e42ce64a8de2a759fcef254a050
SHA1Sum: e3bb0905e4e1d1f93f401b24bd98c0d30a5ae903
kdevelop-php-docs-1.2.81.tar.bz2
MD5Sum: 33ca5a3c570d98e07c68dc7c932d2f10
SHA1Sum: 2ab1a011b69875b0ee76f75add0e4d8494e34c75
