---
title: "kdev-python 1.4 released!"
date: "2012-11-25 18:55:16+00:00"
lastmod: "2016-02-28 21:10:54+00:00"
author: "sbrauch"
aliases:
- /44/kdev-python-14-released
tags:
- release
- stable
- 4.4
- kdev-python
categories:
- News
---
After about two years of development and 894 commits, I'm happy to announce the first stable release of kdev-python! kdev-python is the Python language support plugin for KDevelop, which provides syntax checking, code completion, quickopen support, and much more for the Python scripting language. For details, please refer to [the announcement on my blog](http://scummos.blogspot.de/2012/11/kdev-python-14-stable-released.html).

  

**Download the plugin sources from:**
<http://download.kde.org/stable/kdevelop/kdev-python/1.4.1/src/kdev-python-v1.4.1.tar.bz2.mirrorlist>


or wait for your distribution to package it. kdev-python 1.4 can be used with kdevplatform 1.4.x.  
  

**File checksums:**




```
SHA-256 8743844c6bcf09b3f9db05891539973633049470eb7c046b75b3cfe4542da1e2  kdev-python-v1.4.1.tar.bz2
SHA-1 b887811d9a79eee3323cf3ad1be093c5801d31d6  kdev-python-v1.4.1.tar.bz2
MD5 8980b2cdb955f8f34f7560ffc940ef1b  kdev-python-v1.4.1.tar.bz2
```

A big thanks to all the other KDevelop people who helped a lot with making this possible!



