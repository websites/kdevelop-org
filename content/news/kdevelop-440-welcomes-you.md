---
title: "KDevelop 4.4.0 Welcomes You!"
date: "2012-10-23 15:18:42+00:00"
lastmod: "2016-02-28 21:10:54+00:00"
author: "milian"
aliases:
- /44/kdevelop-440-welcomes-you
tags:
- release
- stable
- kdevelop
- 4.4
- usability
categories:
- News
---
The KDevelop team is happy to announce the immediate availability of KDevelop 4.4. As usual, this feature release comes packed with new features, bug fixes and improved performance. New in this release is a shiny welcome screen for improved usability and an easier entry into the KDevelop world.



## Welcome Screen



Long-time KDevelop contributor [Aleix Pol](http://www.proli.net) worked on a QML/Plasma based [welcome screen](http://www.proli.net/2012/04/27/youre-welcome-to-kdevelop/) for KDevelop. It is shown whenever you have no files opened and offers you some actions to get started. It is all context dependent, based on our Code/Debug/Review area concept. The code screen for example offers you a list of projects and sessions. The debug area screen on the other hand gives a fast way to figure out what your program is doing, whereas the review area gives you a hand in reviewing patches.


This all should make it much simpler for newcomers to get started with KDevelop. But also our long time users should appreciate the faster workflow.


## Other Changes


We admit that the list of shiny noteworthy changes in the 4.4 release is a bit small. Yet rest assured that as usual we have worked hard to give you an improved experience. Tons of bugs where fixed and quite some time spent on optimizing existing code paths - enjoy!


## Download


Your distributors should provide you with updated binary packages already. If you want to have a look at the changelog though or really want to compile KDevelop on your own, then head over to the KDE mirrors:


<http://download.kde.org/download.php?url=stable/kdevelop/4.4.0/src/>


### Hashes


To verify the correctness of your downloaded packages, you can use the following SHA1 and MD5 hashes:


**MD5 sums:**



```

b3c8e3032eb54a074b643d9521e52f5a kdevelop-4.4.0.tar.bz2
c787e13a9dc5beab5a53bd601f74091b kdevelop-php-1.4.0.tar.bz2
cd7f3d5faa299153305afe2e11d81a8e kdevelop-php-docs-1.4.0.tar.bz2
c20e8c1b16b4ae243704eb7eba636874 kdevplatform-1.4.0.tar.bz2

```

**SHA1 sums:**



```

271a6ab3dd6063175e9ce18810bed7437d451ecf kdevelop-4.4.0.tar.bz2
c899aae94755dbc3f4fb72edfcb5b01a9f0eca43 kdevelop-php-1.4.0.tar.bz2
d06df690ea70d02707388851b61077bea8f9db1d kdevelop-php-docs-1.4.0.tar.bz2
2db4fc073385c49d82de526273272da648a0af60 kdevplatform-1.4.0.tar.bz2

```

## Wrapup


A big thank you to all the contributors who made this release possible. Many thanks to our users as well, for reporting bugs and making it such a fun and rewarding task to work on KDevelop!

