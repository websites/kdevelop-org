---
title: "KDevelop 4.4.1 Available"
date: "2012-11-08 12:48:08+00:00"
lastmod: "2016-02-28 21:10:54+00:00"
author: "milian"
aliases:
- /44/kdevelop-441-available
tags:
- release
- stable
- 4.4
categories:
- News
---
After just two weeks the KDevelop team is happy to announce the availability of KDevelop 4.4.1! This release increases the stability and contains a few other bug fixes. Everyone is urged to upgrade as soon as possible.



## Download


As usual, you should be able to download the updated version through your normal distribution channels. If you want to have a look at the changelog though or really want to [compile KDevelop on your own](http://techbase.kde.org/KDevelop4/HowToCompile), then head over to the KDE mirrors:


**Download: <http://download.kde.org/stable/kdevelop/4.4.1/src/>**


### Hashes


To verify the correctness of your downloaded files, please use the following MD5 and SHA1 hash sums:


**MD5 sums:**



```

2ffa6a17019c8de09ce9da25b3b92925  kdevelop-4.4.1.tar.bz2
889ee5cd659fa69605bf31b1b150b01e  kdevelop-php-1.4.1.tar.bz2
93342b96ccd0df5d5b15dd1b276e997b  kdevelop-php-docs-1.4.1.tar.bz2
1ff0566c71a380a37921716dce4ca7c8  kdevplatform-1.4.1.tar.bz2

```

**SHA1 sums:**



```

31bd10ffdbf2a6f2b3a75ecc8c1877d1bcc5b685  kdevelop-4.4.1.tar.bz2
f3523acfc27d0fd68c2b1f970b4ac42dcda38ced  kdevelop-php-1.4.1.tar.bz2
3a1b20c4527abf489df20a23177dd9befc2e1d3c  kdevelop-php-docs-1.4.1.tar.bz2
48f2b40256e96aa015fa40978d5a9a93056fba75  kdevplatform-1.4.1.tar.bz2

```

## Wrapup


A big thank you to all the contributors who made this release possible. Many thanks to our users as well, for reporting bugs and making it such a fun and rewarding task to work on KDevelop!

