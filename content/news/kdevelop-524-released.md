---
title: "KDevelop 5.2.4 released"
date: "2018-08-21 09:45:00+00:00"
lastmod: "2018-08-21 09:41:13+00:00"
author: "sbrauch"
aliases:
- /news/kdevelop-524-released
tags:
- release
categories:
- News
---
As the last stabilization and bugfix release in the 5.2 series, we today make KDevelop 5.2.4 available for download. This release contains a few bug fixes and a bit of polishing, as well as translation updates, and should be a very simple transition for anyone using 5.2.x currently.


Notable changes: 


* Fix resizing of variable tooltip (D14879)
* Fix various problems with filters in the output view (bug 343124)
* Fix a crash which could happen when using the class browser in debug builds with Qt >= 5.11 (D14840)
* Only show cppcheck menu for cpp files (D14525)
* PHP support: determine correct type for relational and equality expressions (bug 305341)
* PHP support: fix a bug with rescheduling jobs (D14113)


KDevelop 5.2.4 can as usual be downloaded from our [Downloads](/download) page. In addition to the source code, we also provide 32- and 64-bit Windows installers, as well as pre-built binaries for 64-bit Linux as an AppImage.


After closing out the 5.2 series, we also plan to release KDevelop 5.3 soon with lots of improvements.


