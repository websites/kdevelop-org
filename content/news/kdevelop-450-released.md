---
title: "KDevelop 4.5.0 Released"
date: "2013-04-26 15:46:48+00:00"
lastmod: "2016-02-28 21:10:54+00:00"
author: "apol"
aliases:
- /45/kdevelop-450-released
tags:
- release
- kdevelop
- php
- c++11
- 4.5
- unit
- tests
- wizard
- plasmoid
categories:
- News
---
We are happy to announce that KDevelop 4.5.0 is now available. For those who don't know, KDevelop is an IDE for all those developers who want to integrate the tools they need for developing, in a comfortable and simple solution. We have a focus on C++ and CMake, but you can find it useful when using other languages such as PHP and Python.

## What's new


KDevelop is getting more mature in every release and this new version is a proof. Firstly, in this new version you will find brand new integration for Unit Tests, so that you can easily run and debug them while working on your projects. Furthermore, you'll find an iteration of our New Class wizard, many changes regarding polishing the UI in different places, better support for C++ 11 features and some other things you'll find along the way.

You can find a bit more information about what we have been working on in the last sprint's wrap-up page:
http://dot.kde.org/2012/12/12/katekdevelop-october-sprint-whats-new-kdevelop

## Get KDevelop now!


If you want to try it, you can either compile it from our tarballs or use your distribution's packages. You can find more information about how to install it in your distro on this wiki page: http://userbase.kde.org/KDevelop/Install4.5
Download the source tarballs from the KDE mirrors: <http://download.kde.org/stable/kdevelop/4.5.0/src/>

Have fun using it, looking forward to your feedback!

The KDevelop Team