---
title: "kdev-python 1.5.1 released"
date: "2013-05-30 21:34:17+00:00"
lastmod: "2016-02-28 21:10:54+00:00"
author: "sbrauch"
aliases:
- /kdev-python-151-released
tags:
- release
- stable
- kdev-python
categories:
- News
---
kdev-python 1.5.1 has been released (a few days ago already, actually), which contains a few bug fixes over 1.5.0.

**Grab the tarball from: <http://download.kde.org/stable/kdevelop/kdev-python/1.5.1/src/kdev-python-v1.5.1.tar.bz2.mirrorlist>**.