---
title: "KDevelop Hacksprint Vienna 2012 Report"
date: "2012-12-17 19:01:34+00:00"
lastmod: "2016-02-28 21:10:54+00:00"
author: "milian"
aliases:
- /kdevelop/kdevelop-hacksprint-vienna-2012-report
tags:
- kdevelop
- sprint
categories:
- News
---
After some delay, the is the second part of the report on the joint Kate/KDevelop development sprint that took place in Vienna from the 23rd to 29th of October this year is finally available on the dot:

http://dot.kde.org/2012/12/12/katekdevelop-october-sprint-whats-new-kdevelop

It highlights the changes that took place in the KDevelop world during that week. Enjoy!