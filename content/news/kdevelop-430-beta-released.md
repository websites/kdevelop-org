---
title: "KDevelop 4.3.0 Beta Released"
date: "2012-01-16 21:19:51+00:00"
lastmod: "2016-02-28 21:10:54+00:00"
author: "apol"
aliases:
- /43/kdevelop-430-beta-released
tags:
- kdevelop
- beta
- 4.3
categories:
- News
---
The KDevelop team is proud to bring to you the first release in the 4.3 series, for our adventurous users who want to try what is coming in the new KDevelop 4.3 release.

This release is centered in stabilization more than new features development, but there are quite some new features that you'll find interesting, like improved version control system integration and improved C++11 support.

### Download


You can find the source code available to download [here](http://download.kde.org/download.php?url=unstable/kdevelop/4.2.81/src/), together with some lengthy changelogs in case you wonder what exactly happened since KDevelop 4.2.

Note furthermore, that you need [KDevelop-PG-Qt 0.9.82](download.kde.org/download.php?url=unstable/kdevelop-pg-qt/0.9.80/src/) if you intend to build the PHP language support plugin for KDevelop.

## Hashes



To verify the correctness of your downloaded packages, you can use the following SHA1 and MD5 hashes:


kdevplatform-1.2.81.tar.bz2
MD5Sum: ee67bcf14036a3ee528902273ad7e411
SHA1Sum: 2ac10213084a0034617d0a4a1cc96a489c1cc338
kdevelop-4.2.81.tar.bz2
MD5Sum: 5114730407d4a16b88ab2fc633419c21
SHA1Sum: f4087c6fec636efaa9eaf56ca5e215f64b1dcdb5
kdevelop-custom-buildsystem-1.2.1.tar.bz2
MD5Sum: 9cce7ff6ae7ef9b9de62609d496e9b2c
SHA1Sum: dc6131999ae8845d49d23a9d58019840e1bac2c1
kdevelop-php-1.2.81.tar.bz2
MD5Sum: d778a41575d644c8578dbd38c189dfcc
SHA1Sum: 1681b4e41ec101a7fcaba2a9ce30d1b2c876876d
kdevelop-php-docs-1.2.81.tar.bz2
MD5Sum: 07868abd1aa114528b015283afc0cba0
SHA1Sum: ec62fd1c471e668fc8835b9067bf418e65d184c5
