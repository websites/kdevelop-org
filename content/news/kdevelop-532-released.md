---
title: "KDevelop 5.3.2 released"
date: "2019-03-07 08:30:00+00:00"
lastmod: "2019-03-07 15:59:17+00:00"
author: "kfunk"
aliases:
- /news/kdevelop-532-released
tags:
- release
- 5.3
categories:
- News
---
We today provide a stabilization and bugfix release with version 5.3.2. This is a bugfix-only release, which introduces no new features and as such is a safe and recommended update for everyone currently using KDevelop 5.3.1.


You can find the updated Windows 32- and 64 bit installers, the Linux AppImage, as well as the source code archives on our [download](/download) page.


### [kdevelop](https://commits.kde.org/kdevelop)


* Don't call clear() on a shared pointer we don't own. ([commit.](https://commits.kde.org/kdevelop/fba39d4a06a4b2a1fa551383d7dfc0af4fdb82c9) fixes bug [#403644](https://bugs.kde.org/403644))
* Workaround the bug found by ASan, which can be seen on FreeBSD CI. ([commit.](https://commits.kde.org/kdevelop/af569639249301e9669b56a434d966a3dfc8ee4d) code review [D18463](https://phabricator.kde.org/D18463))
* Kdev-clazy: use canonical paths. ([commit.](https://commits.kde.org/kdevelop/a7c053a39218db49c50d989d3d7bd48ab12276a6) code review [D15797](https://phabricator.kde.org/D15797))
* Prevent the Extra Arguments ComboBox to Stretch Too Much. ([commit.](https://commits.kde.org/kdevelop/f7571e8b0589306e9bd9d872f9cff4f5f8c49298) code review [D18414](https://phabricator.kde.org/D18414))
* CMake plugin: don't hardcode a default install prefix. ([commit.](https://commits.kde.org/kdevelop/c088890cefbd8a4a30fe494e148f1bf7038f1678) code review [D17255](https://phabricator.kde.org/D17255))
* Appimage: skip unneeded cp of cmake, removed later again. ([commit.](https://commits.kde.org/kdevelop/ff326bb2061a996e1ba914de15cd8b20d7a31561) code review [D18175](https://phabricator.kde.org/D18175))
* Clang plugin: Handle CUDA files better. ([commit.](https://commits.kde.org/kdevelop/71c65a65917a9e1b918ab425e5d482a6cd3af081) code review [D17909](https://phabricator.kde.org/D17909))
* Clang: detect Clang builtin dirs at runtime on Unix. ([commit.](https://commits.kde.org/kdevelop/41a0f0ce51979363c795919e00f914048d61326e) code review [D17858](https://phabricator.kde.org/D17858))
* Actually cleanup the duchain from the background thread. ([commit.](https://commits.kde.org/kdevelop/48e5892f6cf7b398c5175015e0fb207d9baed667) fixes bug [#388743](https://bugs.kde.org/388743))
* Appimage: add okteta libs, as used by the debugger memory view. ([commit.](https://commits.kde.org/kdevelop/b8b266d111c3123cfe6c4876f1b62630edd46cdb) code review [D17888](https://phabricator.kde.org/D17888))
* Grewpview: Fix potential crash in "Find in Files". ([commit.](https://commits.kde.org/kdevelop/1dbebbab5f34d4b9b1ca37d5f4c1e2cd41616fd2) fixes bug [#402617](https://bugs.kde.org/402617))
* Add All Top-Level Targets to the Menu. ([commit.](https://commits.kde.org/kdevelop/a3167ce6362b2d87e143f47458868f69355daf42) code review [D18021](https://phabricator.kde.org/D18021))
* Show "Move into Source" action in code menu. ([commit.](https://commits.kde.org/kdevelop/05efd841d4f55e4370a364f5c7b55626af580b44) code review [D17525](https://phabricator.kde.org/D17525))
* QuickOpen: Trim whitespace from input. ([commit.](https://commits.kde.org/kdevelop/263a57379ef68f7d0471009f1163821ecf5556e4) code review [D17885](https://phabricator.kde.org/D17885))
* Update kdevelop app icon to latest breeze-icons version. ([commit.](https://commits.kde.org/kdevelop/57ec89f99a49be439b5f48cde4d1e9463cc8ec5b) code review [D17839](https://phabricator.kde.org/D17839))
* Appimage: have only kdevelop appdata in the appimage. ([commit.](https://commits.kde.org/kdevelop/7ab1c6efd7fec901a2df26b0c67df9c07b19252a) code review [D17582](https://phabricator.kde.org/D17582))
* Fix first run of appimage creation: get install\_colorschemes.py via $SRC. ([commit.](https://commits.kde.org/kdevelop/a645e513688ac8e43f5da26516dac7cb8dd9af75) code review [D17581](https://phabricator.kde.org/D17581))
* Fix crash in documentation view. ([commit.](https://commits.kde.org/kdevelop/8ef6a82bd870b155996c5e4231ea36d34eafd40f) fixes bug [#402026](https://bugs.kde.org/402026))
* CMake: skip server entries without empty build system information. ([commit.](https://commits.kde.org/kdevelop/2da988ae2a36bc48a864e96c124a7a5412fb31a2) code review [D17679](https://phabricator.kde.org/D17679))
* 2 missing KTextEditorPluginIntegration::MainWindow slots. ([commit.](https://commits.kde.org/kdevelop/dc6a95e9628cd1f765d5c0c5cfdff7b3126d72d9) code review [D17465](https://phabricator.kde.org/D17465))
* Polish Purpose integration in the PatchReview plugin. ([commit.](https://commits.kde.org/kdevelop/a5ca3cd1b0a37ab63a6d68a6271934a36f9b0b90) code review [D17424](https://phabricator.kde.org/D17424))
* Hex editor plugin: prepare for incompatible API change of libraries from upcoming Okteta 0.26.0


### [kdev-python](https://commits.kde.org/kdev-python)


* Fix crash when finding context inside annotated assigments. ([commit.](https://commits.kde.org/kdev-python/cabf41fb18ade07043007c844a21966830ac443a) fixes bug [#403045](https://bugs.kde.org/403045))


### [kdev-php](https://commits.kde.org/kdev-php)


*No changes*


