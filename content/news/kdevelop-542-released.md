---
title: "KDevelop 5.4.2 released"
date: "2019-09-02 21:00:00+00:00"
lastmod: "2019-09-02 22:00:58+00:00"
author: "kossebau"
aliases:
- /news/kdevelop-542-released
tags:
- release
categories:
- News
---
We today provide a stabilization and bugfix release with version 5.4.2. This is a bugfix-only release, which introduces no new features and as such is a safe and recommended update for everyone currently using a previous version of KDevelop 5.4.


You can find the updated Linux AppImage as well as the source code archives on our [download](/download) page.


## ChangeLog


### [kdevelop](https://commits.kde.org/kdevelop)


* All debuggers: fix VariableCollection to unregister as texthinter provider. ([commit.](https://commits.kde.org/kdevelop/0b4ad38441d6a3ce558102cc1ae329d8553ad025) See bug [#411371](https://bugs.kde.org/411371))
* Contextbrowser: register as texthint provider to existing views on creation. ([commit](https://commits.kde.org/kdevelop/36ab233802ee22c663bd7f32e315036472811c26))
* Fix crash on text hint being triggered after disabling code browser plugin. ([commit.](https://commits.kde.org/kdevelop/37d08f752e261f68cc89fd1d0fe6a55b2ee40a22) See bug [#411371](https://bugs.kde.org/411371))
* Avoid possible dereference of an invalid iterator. ([commit.](https://commits.kde.org/kdevelop/69e1ed669289a6927d2cf3e3b7096a649a095d8c) fixes bug [#411323](https://bugs.kde.org/411323))
* Kdevplatform/shell: fix outdated window title once project of document loaded. ([commit](https://commits.kde.org/kdevelop/a580d92031891a92610d707f70e50dafb7d01cf2))
* Kdevplatform/shell: work-around for Qt 5.9/macOS bug showing modified indicator. ([commit](https://commits.kde.org/kdevelop/bc06b109f21016a27262f90a94c3f72ddc71f8b8))
* Kdevplatform/shell: restore document modified flag in mainwindow title. ([commit](https://commits.kde.org/kdevelop/5a286ea5125cb30d38b9c7477b20e6f6ac9c103e))
* Kdevplatform/shell: do not repeat query & differently for current document. ([commit](https://commits.kde.org/kdevelop/d6f63c0d898bcf7a70947a60a14b07cee9d0119e))
* Indicate appsteam the ps desktop file isn't a separate application. ([commit.](https://commits.kde.org/kdevelop/12bf83b0abd064f22c0a137b0ec7123f155c3097) code review [D23321](https://phabricator.kde.org/D23321). fixes bug [#410687](https://bugs.kde.org/410687))
* Clang: fix tooltip missing closing bracket with default argument calls. ([commit](https://commits.kde.org/kdevelop/6367608d61b4dc84e0a6334ee08909735c94ef28))
* Include more hidden files in projectfilter plugin (CI, Lint configs...). ([commit](https://commits.kde.org/kdevelop/925bd4c034d7d904035d95dc250fd9e9cffd28bb))


### [kdev-python](https://commits.kde.org/kdev-python)


No user-relevant changes.


### [kdev-php](https://commits.kde.org/kdev-php)


No user-relevant changes.


