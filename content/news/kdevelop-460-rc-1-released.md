---
title: "KDevelop 4.6.0 RC 1 Released"
date: "2013-11-26 11:18:46+00:00"
lastmod: "2016-02-28 21:10:54+00:00"
author: "milian"
aliases:
- /46/kdevelop-460-rc-1-released
tags:
- release
- kdevelop
- unstable
- release-candidate
- 4.6
categories:
- News
---
Hello all!

After two more weeks of stabilizing KDevelop, we are happy to announce the availability of KDevelop 4.6.0 RC 1! If no blockers are found, this will lead to the release of 4.6.0 final in two more weeks.

## Download



As usual, you can download the source code from the KDE mirrors: http://download.kde.org/unstable/kdevelop/4.5.90/src/
The hashes to verify the validity of your download are:

### MD5



```
a5f9f66208bfb5e4cd31533d61dbda21  kdevelop-4.5.90.tar.xz
8baa127950a24e18154d7e9e50aab990  kdevelop-php-1.5.90.tar.xz
0656ac57e66d4c1519e24a28ebba71ca  kdevelop-php-docs-1.5.90.tar.xz
3346b050fecffd350e968016a639e99d  kdevplatform-1.5.90.tar.xz
```

### SHA1



```
f4a71eaf3d12057ea08699ba5cb9e1a745ea63eb  kdevelop-4.5.90.tar.xz
f18216e281d9df004fa68876660e22081a72efe6  kdevelop-php-1.5.90.tar.xz
e9162db5e2e39fce4a4d578a3dff92d3899cc636  kdevelop-php-docs-1.5.90.tar.xz
74a19a48ad009b657eb769f39298fa74a393ac65  kdevplatform-1.5.90.tar.xz
```

## Changes



The changes since [4.6.0 Beta 1](/46/kdevelop-46-beta-1-available) include bug fixes in various areas. Read the following changelogs for more details:

### KDevplatform



```
* Milian Wolff: Preallocate containers to size of return value.
* Milian Wolff: Cleanup includes.
* Milian Wolff: Give file-local linkage to comparison function.
* Kevin Funk: Fix assert in projectcontroller.cpp
* Niko Sams: Fix deleting empty folders in git versioned project
* Andrea Scarpino: The vcs_diff icon was part of Cervisia 4.6. Since KDE 4.7 it is named vcs-diff-cvs-cervisia.
* Kevin Funk: Minor: Fix documenation
* Steffen Ohrendorf: Save correct combo box value in grepview plugin
* Kevin Funk: Minor: Cleanup code
* Aleix Pol: Add missing i18n call
* Milian Wolff: Cleanup API by removing redundant qualification and adding const&.
* Milian Wolff: Add missing file from last commit...
* Milian Wolff: Backport fixes to json test suite from master.
* Milian Wolff: Remove the dashboard from KDevplatform 1.6, it is not yet ready.
* Milian Wolff: Don't crash when trying to access currentType with empty stack.
* Kevin Funk: Minor: Remove unnecessary fwd decl
* Kevin Funk: Minor: Make kdevelop parser happy
* Kevin Funk: Add LGPL v3 license template
* Kevin Funk: Update .gitignore
```

### KDevelop



```
* Milian Wolff: Set version to 4.5.90 for 4.6.0 RC1
* Milian Wolff: Don't abort the test suite if an exception is catched.
* Gabo Menna: Fix list of primitive types for correct pretty-printing of QList.
* Milian Wolff: Further improve handling of defines in the CMake manager.
* Milian Wolff: Visit children of ConditionAST node in ExpressionVisitor.
* Milian Wolff: Give file-local linkage to helper functions.
* Milian Wolff: Honor definitions set via add_definitions, not only via set_property.
* Milian Wolff: Cleanup code, use typedefs and smart pointers where possible.
* Milian Wolff: Also take constructors in initdeclarator use association into accont.
* Milian Wolff: Cleanup code: Return by value instead of taking input argument.
* Milian Wolff: Use isPointerType to also cover type aliases.
* Milian Wolff: Apply pointer ops to last type and honor it before creating uses.
* Milian Wolff: Cleanup code: remove unused variable.
* Milian Wolff: Do not delete anonymous contexts of instantiations.
* Milian Wolff: Make it possible to disable the splash.
* Milian Wolff: Remove dead code.
* Milian Wolff: Properly copy over the properties from the bootstrap phase.
* Milian Wolff: Add support for REALPATH in get_filename_component.
* Milian Wolff: Also use the paths from building in CMakeLoadProjectTest.
* Milian Wolff: Properly support CMAKE_{PREFIX,INCLUDE,LIBRARY}_PATH env vars.
* Milian Wolff: Support lib64 locations and also search packages based on PATH env.
* Milian Wolff: Fix cmake manager to not rely on undefined evaluation order.
* Kevin Funk: Generate kdevghprovider.desktop from cmake template
* Kevin Funk: Minor: Fix -Wtype-limits warning
* Aleix Pol: --warnings
* Aleix Pol: --warnings
* Aleix Pol: Only escape strings when constructed for the first time
* Milian Wolff: Also look in CMAKE_PLATFORM_IMPLICIT_LINK_DIRECTORIES for libraries.
* Kevin Funk: Really fix all file templates
* Milian Wolff: Assert validity of item and index in MakeJob.
* Milian Wolff: Cleanup code and fix building of single files in CMake/Make projects.
* Kevin Funk: Update .gitignore
* Milian Wolff: Disable test depending on availability of Qt5.
* Kevin Funk: Minor: Fix warning
* Kevin Funk: Minor: Cleanup: Don't obfuscate code
* Kevin Funk: Also parse REVISION from Q_PROPERTY
* Kevin Funk: Get rid off uninteresting properties of Q_PROPERTY
* Kevin Funk: Parse MEMBER from Q_PROPERTY decl
* Lasse Liehu: Don't translate root installation commands in Ninja configuration
```
