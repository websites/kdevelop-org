---
title: "KDevelop 4.2.0 Released"
date: "2011-01-26 14:00:00+00:00"
lastmod: "2016-02-28 21:10:54+00:00"
author: "milian"
aliases:
- /420/kdevelop-420-released
tags:
- release
- 4.2
- stable
- kdevelop
- php
categories:
- News
---
Only three months after the last feature release, the KDevelop hackers are proud and happy to announce the release of KDevelop 4.2. As usual, we also make available updated versions of the KDevelop PHP plugins.

You should find that KDevelop 4.2 is significantly more stable and polished than 4.1, with some important changes under the hood; we suggest that everyone updates to this version, if possible.

Please note that this version is required for users of KDE Platform 4.6 or higher. It will also work with KDE Platform 4.5 but Platform 4.4 or older are not supported.

Read the full release announcement on the Dot: http://dot.kde.org/2011/01/31/kdevelop-42-supports-latest-kde-releases