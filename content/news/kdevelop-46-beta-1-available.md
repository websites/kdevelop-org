---
title: "KDevelop 4.6 Beta 1 Available"
date: "2013-11-11 15:05:13+00:00"
lastmod: "2016-02-28 21:10:54+00:00"
author: "milian"
aliases:
- /46/kdevelop-46-beta-1-available
tags:
- release
- kdevelop
- unstable
- kde
- 4.6
categories:
- News
---
The KDevelop team is pleased to announce the availability of the first beta release of KDevelop 4.6! This is a testing release and we would be happy about any feedback from early adopters. Please report any issues you find at http://bugs.kde.org.

# Download



You can download the tarballs for testing from the official KDE mirrors: http://download.kde.org/unstable/kdevelop/4.5.80/src/

## MD5 Hash Sums



```
ee7d4cb2cf48b927f08e2edc85fb53c1  kdevelop-4.5.80.tar.xz
c7ee4297ccae4ca2fdcdd05bb1e0be77  kdevelop-php-1.5.80.tar.xz
9240ddb09974147aadc2c57880a05e13  kdevelop-php-docs-1.5.80.tar.xz
056561e30ab4eea1aec1367a6eb41777  kdevplatform-1.5.80.tar.xz
```

## SHA1 Hash Sums



```
bfb8b665d15d97f7a384e55ccf4d8b84792ce11f  kdevelop-4.5.80.tar.xz
a0120bd442216a1d56c58c234d536aee134aa0dd  kdevelop-php-1.5.80.tar.xz
a4a7054350e8e4f4de7697cb532463530f1c8a2e  kdevelop-php-docs-1.5.80.tar.xz
10f4cf6bbd87b9fc03ad54488a2a9302cbfded47  kdevplatform-1.5.80.tar.xz
```

# Changes


The changes compared to 4.5.2 are numerous as can be seen from the following changelogs:


* [KDevplatform 1.5.80 Changelog](/sites/kdevelop.org/files/kdevplatform-1.5.80.CHANGELOG)
* [KDevelop 4.5.80 Changelog](/sites/kdevelop.org/files/kdevelop-4.5.80.CHANGELOG)
* [KDevelop PHP 1.5.80 Changelog](/sites/kdevelop.org/files/kdevelop-php-1.5.80.CHANGELOG)
* [KDevelop PHP Docs 1.5.80 Changelog](/sites/kdevelop.org/files/kdevelop-php-docs-1.5.80.CHANGELOG)


Overall nearly one thousand commits entered this release. A few highlights are presented below.


## UI Cleanups


Quite a few dialogs saw smaller changes to simplify and cleanup the UI. The most notable change though is the removal of the "area" tabs at the top right. Instead, they are replaced by a single button which hopefully clarifies and simplifies the workflow in KDevelop.


## Ninja Support


KDevelop 4.6 is the first release to officially support the Ninja build system in addition to Make. Ninja promises higher build performance, so try it out! Note that CMake can be configured to use Ninja instead of Make, thus switching to Ninja is trivial.


## Project Filter
The files and folders that are listed in your project folder and below is now [properly filterable.](http://milianw.de/blog/kdevelop-project-filters) The old line edit above the project tree view was removed in favor of this more advanced and efficient filtering approach. If you used the filter line edit to find files based on a pattern in your project, try out QuickOpen of files, which handles this job much better.
Stability and Performance
Besides the big new features listed above, much work was spent on improving the code base to yield a better stability and performance and maintainability. This includes work on QuickOpen to make it even faster for large projects. The output view of processes or compilation output is now filtered on a background thread thus preventing UI freezes under some conditions.
Various
Other areas that where improved are the version control integration and the GDB integration, which got the ability to browse through registers. The CMake support was also greatly improved for this release. It should now be much more reliable and come with support for more features than ever. The C++ language support was also improved in some areas as usual. The PHP support finally was improved to support various new features of PHP 5.3, 5.4 and 5.5.

