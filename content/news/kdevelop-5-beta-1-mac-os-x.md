---
title: "KDevelop 5 Beta 1 on Mac OS X"
date: "2015-10-26 15:40:12+00:00"
lastmod: "2017-01-03 09:46:50+00:00"
author: "milian"
aliases:
- /screenshots/kdevelop-5-beta-1-mac-os-x
tags:
- KDevelop 5
---
A preliminary screenshot showing KDevelop 5 Beta 1 running on Mac OS X.

If you are interested on polishing KDevelop for Mac, please [get in touch with us!](https://www.kdevelop.org/contribute-kdevelop)