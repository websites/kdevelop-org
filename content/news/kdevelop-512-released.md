---
title: "KDevelop 5.1.2 released"
date: "2017-08-28 21:00:00+00:00"
lastmod: "2017-08-28 21:29:39+00:00"
author: "sbrauch"
aliases:
- /news/kdevelop-512-released
tags:
- kdevelop
- release
- 5.1
categories:
- News
---
We are pleased to announce the release of KDevelop version 5.1.2, the second bug-fix release for the 5.1 series. This update contains bug fixes only, and we highly recommend all users of KDevelop 5.1.x to switch to this version. Given that it has been a few months since the release of KDevelop 5.1.1, this version contains quite a lot of changes.


## Get it


As always, source code, the AppImage and Windows installers for 32 bit and 64 bit systems can be found on our [download page](https://www.kdevelop.org/download). Your distribution should provide updated packages soon.


The 5.1.2 source code and signatures can be downloaded from [here](http://download.kde.org/stable/kdevelop/5.1.2/src/).


Please give this version a try and as always let us know about any issues you find via our [bug tracker](https://bugs.kde.org).


## Changelog


### Prebuilt binaries


* AppImage: Various improvements; now ships LLVM 3.9.1
* Windows installers: Various improvements; now ship Qt 5.9.1


### Changes in kdevplatform, kdevelop, kdev-php and kdev-python


An incomplete list of things fixed in 5.1.2 is below:


* Fixed a crash in the cmake lexer ([bug 363269](http://bugs.kde.org/363269))
* Various small improvements in cmake and C++ code completion
* Fix placement of C++ #include completions (<https://phabricator.kde.org/D6230>)
* Fix a crash with JS projects on remote directories ([bug 369573](http://bugs.kde.org/369573))
* Fix a possible crash while parsing PHP code ([bug 381123](http://bugs.kde.org/381123))
* Fix a hang-on-exit bug ([bug 379669](http://bugs.kde.org/379669))
* Source formatting preview is back
* Fix duplicated context menu on icon border (<https://phabricator.kde.org/D6838>)
* Fix code display in the uses widget on some systems
* Fix "Format Files" context menu action
* Fix a possible crash when parsing Python code with old-style Mac line endings ('\r' only) ([bug 378827](http://bugs.kde.org/378827))
* Fix jumping to breakpoint location in the breakpoint model ([bug 382652](http://bugs.kde.org/382652))
* Fix crash on CPUs without SSE2
* Fix launching executables with spaces in paths on windows
* Fix possible crash on clicking "show documentation" in tooltip (<https://phabricator.kde.org/D6436>)
* Various cleanup work
* ... and many others, too many to list them here -- run "git log v5.1.1..v5.1.2" in one of the repositories to see all changes.


## Other notes


We plan to release KDevelop 5.2.0 in the near future, with more nice updates like much improved performance for C++ code completion, a unified repository for kdevelop and kdevplatform (making it easier to work on KDevelop, especially for new contributors), a first version of support for "Runtimes" like docker containers, as well as improvements for the PHP and Python language support plugins.


