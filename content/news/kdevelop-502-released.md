---
title: "KDevelop 5.0.2 released for Windows and Linux"
date: "2016-10-17 11:15:00+00:00"
lastmod: "2016-10-19 07:07:02+00:00"
author: "sbrauch"
aliases:
- /news/kdevelop-502-released
tags:
- kdevelop
- KDevelop 5
- windows
- release
categories:
- News
---
Four weeks after the release of KDevelop 5.0.1, we are happy to announce the availability of KDevelop 5.0.2, a second stabilization release in the 5.0 series. We highly recommend to update to version 5.0.2 if you are currently using version 5.0.1 or 5.0.0.


Along with the source code, we release an updated 64-bit **AppImage for Linux** as well as updated **installers for Microsoft Windows**. We've got the pleasure to announce a **32-bit version of KDevelop for Windows** now, too!


You can find all the downloads on our [download page](https://www.kdevelop.org/download).


![KDevelop with two editors open in a split view](/sites/www.kdevelop.org/files/inline-images/kdevelop-py-cpp.png)


Notable issues fixed in 5.0.2 are:


* Fix a locking issue in the background parser, causing frequent crashes on Windows (3c395340d)
* Fix broken search in the documentation view (0602281c)
* Fix various issues with the breakpoints view (cba54572)
* Fix a possible crash when activating a clang FixIt (BR: 369176)
* Fix a crash when performing various actions with the Make plugin disabled (BR: 369326)
* Fix text files being parsed as CMake under some circumstances, leading to bad performance and crashes
* Use correct font in documentation view (BR: 285162)
* Fix a crash when clicking "Rename declaration" without an editor being open (22bdccb1)
* Fix "Download more ..." feature not working on some systems (4c4500bf)
* Fix "Select next/previous toolview" behaving incorrectly in some cases (24d3e3bb)
* Fix "Hide/Restore docks" behaving incorrectly in some cases (daeed5f1)
* Fix "Install as root" action not working (30a66c3f)
* Fix CMake build plugin sometimes rebuilding everything when it should not (17b6499e)
* Various UI improvements.


Fixes in the Windows installers:


* Update Sonnet in the Windows installer which led to crashes when turning on Automatic Spell Checking [BR: 370470]
* Fix heap corruption after start / immediately after opening folder [BR: 370495]


The **source code** can be downloaded from <http://download.kde.org/stable/kdevelop/5.0.2/src/>.


You can find the **binaries for Windows and Linux** [on our download page](http://kdevelop.org/download).


The source code archives and their sha-256 checksums are



```

24ec89b4edc854808ce11a8e8b0aeb853f11926b26029bc46c80f901da00aec7  kdev-php-5.0.2.tar.xz
5d160951933e2f6742a443e19d24e0c93a82567244500e4bb6a3124e5e4e11ff  kdev-python-5.0.2.tar.xz
9b017901167723230dee8b565cdc7b0e61762415ffcc0a32708f04f7ab668666  kdevelop-5.0.2.tar.xz
a7f311198bb72f5fee064d99055e8df39ecf4e9066fe5c0ff901ee8c24d960ec  kdevplatform-5.0.2.tar.xz
```

For verifying integrity and authenticity of the files, the preferred method is to use the provided GPG signature files (.sig).  

All downloads are signed with the GPG key of Sven Brauch, fingerprint 329F D02C 5AA4 8FCC 77A4  BBF0 AC44 AC6D B297 79E6.


