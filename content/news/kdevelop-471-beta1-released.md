---
title: "KDevelop 4.7.0 Beta 1 Released"
date: "2014-07-03 20:48:44+00:00"
lastmod: "2016-02-28 21:10:54+00:00"
author: "milian"
aliases:
- /news/kdevelop-471-beta1-released
tags:
- release
- kdevelop
- php
- unstable
- kdev-python
- 4.7
categories:
- News
---
Finally, after months of work, the KDevelop team is happy to release a first beta of the 4.7 version. It comes packed with new features, lots of bug fixes as well as many performance improvements.

The CMake support saw several improvements, which ensure that all idioms needed for KF5 development are available. The unit test support UI was polished and several bugs fixed. In the same direction, some noteworthy issues with the QtHelp integration were addressed.

Feature-wise, KDevelop now officially supports the Bazaar (bzr) version control system. KDevelop now handles PHP namespaces better and can understand traits aliases. Furthermore, some first fruits of the ongoing Google summer of code projects are included in this release, which pave the path toward better support for cross compile toolchains.

On the performance front, it was possible to greatly reduce the memory footprint when loading large projects with several thousand files in KDevelop. Additionally, the startup of KDevelop should now be much faster.

As always, many thanks to the various contributors that made this release possible!

Stay tuned for the release of KDevelop 4.7.0 in a few weeks from now.

## Download KDevelop 4.7.0 Beta 1


You can download the source code from one of the various KDE mirrors:


[kdevplatform-1.6.90.tar.xz](http://download.kde.org/unstable/kdevelop/4.6.90/src/kdevplatform-1.6.90.tar.xz.mirrorlist)  

[kdevelop-4.6.90.tar.xz](http://download.kde.org/unstable/kdevelop/4.6.90/src/kdevelop-4.6.90.tar.xz.mirrorlist)  

[kdev-php-1.6.90.tar.xz](http://download.kde.org/unstable/kdevelop/4.6.90/src/kdevelop-php-1.6.90.tar.xz.mirrorlist)  

[kdev-php-docs-1.6.90.tar.xz](http://download.kde.org/unstable/kdevelop/4.6.90/src/kdevelop-php-docs-1.6.90.tar.xz.mirrorlist)  

[kdev-python-1.6.90.tar.xz](http://download.kde.org/unstable/kdevelop/4.6.90/src/kdev-python-1.6.90.tar.xz.mirrorlist)


The hashes to verify the validity of your download can be found below.


### md5 checksums



```
6f3db3b10483cbc39f303344956fff08  kdevelop-4.6.90.tar.xz
271085161bc854a62ce45fb122b59813  kdevelop-php-1.6.90.tar.xz
01dbd268306bc341910a9a9ac30e08f8  kdevelop-php-docs-1.6.90.tar.xz
7346236a29629b0a28bffccae4bf0c30  kdevplatform-1.6.90.tar.xz
b694db849fd572e443e47bf5c582ce14  kdev-python-1.6.90.tar.xz
```

### sha1 checksums



```
57c6425fdaf107225de0f33c305ee7e90c401bd6  kdevelop-4.6.90.tar.xz
0fb725ad32898cd56711cb52839975e0446f6a08  kdevelop-php-1.6.90.tar.xz
1c176fb4783c951cb555f2e2baf9a6a873cdc9e9  kdevelop-php-docs-1.6.90.tar.xz
8784a8de354d17afec4e58b9c78fde4bd66e1593  kdevplatform-1.6.90.tar.xz
13cfff38e2239db153fe861382799c96384be32d  kdev-python-1.6.90.tar.xz
```

### sha256 checksums



```
07909ce0696f17c8e7b047151d96a3852402182ba38bb79d9b811225add4dcb7  kdevelop-4.6.90.tar.xz
75fddae6b6d8e87f6ac7407500d1c141ae1d96ebe4327bb7d91de5a1b842ed6b  kdevelop-php-1.6.90.tar.xz
b86a90c85cdc3b64a45fd77f921662a0b7c39612f7e04b4851873aefef5b4746  kdevelop-php-docs-1.6.90.tar.xz
e74c0cb4b2782400c6ccd78fd74670c40e701ee73dd351a1f069ceb1d07104f2  kdevplatform-1.6.90.tar.xz
b5d34d9d05de59cec19f827f6339175e016ad7108bfea638ff2c67205d176baa  kdev-python-1.6.90.tar.xz
```

## Changelog



The changelog files for this release are attached to this news announcement, you can find them below.