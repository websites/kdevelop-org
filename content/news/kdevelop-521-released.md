---
title: "KDevelop 5.2.1 released"
date: "2017-11-24 10:00:00+00:00"
lastmod: "2017-11-24 10:08:29+00:00"
author: "sbrauch"
aliases:
- /news/kdevelop-521-released
tags:
- kdevelop
- release
- stable
categories:
- News
---
Just a few days after the release of KDevelop 5.2.0, we today provide a stabilization and bugfix release with version 5.2.1. This is a bugfix-only release, which introduces no new features and as such is a safe and recommended update for everyone currently using KDevelop 5.2.0.


You can find the updated Windows 32- and 64 bit installers, the Linux AppImage, as well as the source code archives on our download page.


Issues fixed:


* Fix a crash which often happened when switching git branches in the background in C++ projects. This temporarily disables documentation warnings, as well as parsed documentation display in the tooltips, until the underlying issue is fixed upstream. ([bug](https://bugs.kde.org/show_bug.cgi?id=358205), [upstream bug](https://bugs.llvm.org/show_bug.cgi?id=35333))
* Fix no compiler being set by default on Windows, causing KDevelop to fail finding the C++ standard library. ([bug](https://bugs.kde.org/show_bug.cgi?id=377045))
* Fix standard library paths not being passed as include paths when using clang as compiler on windows. ([review](https://phabricator.kde.org/D8831))
* Fix a crash happening on some systems when opening a session. ([bug](https://bugs.kde.org/show_bug.cgi?id=384162))
* Fix include path completion not showing project-defined system includes (i.e. includes added as system includes but by your project). ([bug](https://bugs.kde.org/show_bug.cgi?id=386421))
* Fix crash when batch-editing defines in the define editor. ([bug](https://bugs.kde.org/show_bug.cgi?id=386709))
* Fix an assert (only happened in debug mode) and potential crash when showing problem tooltip. ([bug](https://bugs.kde.org/show_bug.cgi?id=386901))
* Fix links in documentation browser not working in the AppImage. ([bug](https://bugs.kde.org/show_bug.cgi?id=386929))
* Fix build failure when building in-source when translations are present. ([bug](https://bugs.kde.org/show_bug.cgi?id=386946))
* Fix a crash when showing navigation tooltip in PHP projects in some situations. ([bug](https://bugs.kde.org/show_bug.cgi?id=386969))
* Fix targets being duplicated when using CMake subprojects. ([bug](https://bugs.kde.org/show_bug.cgi?id=387095))
* Fix console toolview asking to delete a temporary file interactively. ([bug)](https://bugs.kde.org/show_bug.cgi?id=379652)
* Fix a lot of AUTOMOC warnings while compiling with CMake 3.10
* Fix some tool view context menus having empty entries. ([bug](https://bugs.kde.org/show_bug.cgi?id=386911))
* Fix the progress widget in the statusbar not showing in some sessions. ([review](https://phabricator.kde.org/D8929))
* Adapt cache clear logic to ensure that the code model cache (~/.cache/kdevduchain) is *always* cleared when changing to a different KDevelop version (including patch versions). Unsuitable data in the cache has caused a range of issues in the past, including crashes. If you *really* need to keep the cache, you can create an (empty) file with the version suffix of the target version in the cache folder.
* Fix KDevelop not starting up with the welcome page enabled (which it is by default) on systems without OpenGL support. ([bug](https://bugs.kde.org/show_bug.cgi?id=386527))
* Fix build with Qt version 5.5.
* Fix file templates not working on Windows (this fix should be in the Windows 5.2.0 installers already, but was not in the source tarballs).


Please let us know of any issues you encounter when using KDevelop on the [bug tracker](http://bugs.kde.org), in the mailing lists for development or users (kdevelop@kde.org, kdevelop-devel@kde.org), or in the comment section of this release announcement.


We think KDevelop 5.2.1 should work quite well now, but if more issues show up, we will provide another bugfix release shortly.


