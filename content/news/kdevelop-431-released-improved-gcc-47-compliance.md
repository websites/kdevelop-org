---
title: "KDevelop 4.3.1 Released with improved GCC 4.7 compliance"
date: "2012-04-19 09:16:07+00:00"
lastmod: "2016-02-28 21:10:54+00:00"
author: "milian"
aliases:
- /kdevelop/kdevelop-431-released-improved-gcc-47-compliance
tags:
- release
- stable
- kdevelop
- 4.3
- gcc
categories:
- News
---
Just one month after [the KDevelop 4.3.0](/kdevelop/kdevelop-430-final-released-basic-c11-support) release, we are happy to announce the immediate availability of KDevelop 4.3.1 - a stable bugfix release.



## Noteworthy Changes


There were a few important bugfixes since KDevelop 4.3.0, hence you should update as soon as possible.


### Improved C++ support


On machines that already use the recently released GCC 4.7, you need KDevelop 4.3.1 to ensure proper support for many C++ `stdlib` classes in our language support. This is due to the usage of the new C++11 `noexecept` keyword in the `stdlib` header files, which was not supported by KDevelop until now.


Furthermore we managed to resolve a nasty bug in our template support, which so far could freeze our parser when processing specific template code. This means you can finally use the famous [Eigen](http://eigen.tuxfamily.org) library among others in KDevelop again.


### Other Bugfixes


Beside the notable changes above, KDevelop 4.3.1 comes with lots of bug fixes for our PHP and QtHelp documentation plugins, the PHP support and other areas. As usual, we also managed to resolve a few crashes. For the full overview, take a look at [the changelog files](http://download.kde.org/stable/kdevelop/4.3.1/src/).


Many thanks as always to the various contributors who made this release possible.


## Download


Thanks to the help of various packagers, you should already be able to update to KDevelop 4.3.1 without hassle. If you want to download the sources or have a look at the changelogs, you can find both on the official KDE mirrors:


[Download KDevelop 4.3.1](http://download.kde.org/stable/kdevelop/4.3.1/src/)


### Hashes


**md5 sums:**



```

6126e49562dcfb8e78a0f1b052a1ce19  kdevelop-4.3.1.tar.bz2
8f638b6f9e781092f38cd7c2dcebb1b8  kdevelop-php-1.3.1.tar.bz2
7d277fed23f2a5d2b9c2186a938304cd  kdevelop-php-docs-1.3.1.tar.bz2
2c067565dc8c34953ff7030336b01b7e  kdevplatform-1.3.1.tar.bz2

```

**sha1 sums:**



```

99596bf333bf5a62547f3dc0800674db146eb401  kdevelop-4.3.1.tar.bz2
c5d31de0bf0ae4b242b6265851807e5b44606525  kdevelop-php-1.3.1.tar.bz2
2b5c3dd2b26196a83bc95cfe8ee595e160f595f0  kdevelop-php-docs-1.3.1.tar.bz2
bba0cefdac6de83c8d3354534a94e8500dccda37  kdevplatform-1.3.1.tar.bz2

```
