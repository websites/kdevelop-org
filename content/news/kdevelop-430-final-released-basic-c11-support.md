---
title: "KDevelop 4.3.0 Final Released With Basic C++11 Support"
date: "2012-03-19 10:27:42+00:00"
lastmod: "2016-02-28 21:10:54+00:00"
author: "milian"
aliases:
- /kdevelop/kdevelop-430-final-released-basic-c11-support
tags:
- release
- stable
- kdevelop
- 4.3
- c++11
categories:
- News
---
After about nine months of extensive development, the KDevelop team is happy to announce the immediate availability of KDevelop 4.3. As usual, this feature release comes packed with new features, bug fixes and improved performance.



Here are some statistics about the 4.3 release to entice you to upgrade as soon as possible - of course something we recommend everyone should do!




| Package | Commits since 4.2.3 | Diffstat |
| --- | --- | --- |
| kdevplatform | 766 | 615 files changed, 22194 insertions(+), 8377 deletions(-) |
| kdevelop | 542 | 376 files changed, 29167 insertions(+), 15793 deletions(-) |
| kdev-php | 39 | 33 files changed, 565 insertions(+), 381 deletions(-) |


## New Features and Other Notable Changes


This new release has seen lots of work from various contributors. A few noteworthy items are highlighted in the following but this list is far from complete. Try out the new release and see for yourself!


### Basic C++11 Support


The new C++ standard, which was released last year, is now partially suppported in KDevelop. At least the parser should not trip over new language features such as initializer lists, lambdas, range-based for loops or variadic templates. Similarily, explicitly defaulted or deleted methods, auto, rvalue-references and many more features are supported. Many of the new `stdlib` classes can be used as well. Keep in mind though that the new C++ standard contains lots of new features, and we are still lacking support for quite a few things. But we will continue to improve this in the next releases, and provide you with a rock-solid C++11 coding environment.


### Editor Restoration


With KDevelop 4.3 we finally catch up with Kate when it comes to closing and reopening files: folded code regions, bookmarks etc. are now properly restored for the last 20 opened files.


### Enhanced VCS Integration


Some work went into improving the Version Control System (VCS) integration too, mostly on two fronts—creating a [VCS Changes tool view](/screenshots/vcs-changes-view) and improving the Review Mode.


VCS Changes show you what files have changed in your project since the last commit. It is useful to keep track of what you are working on and to decide if there should be a commit.


We improved the Review mode by making it more responsive to the user by updating the view while the user is working on the changes.


### KDE Projects Integration


The KDE Projects infrastructure was adapted to support [projects.kde.org](http://projects.kde.org). This provides you with a list of all KDE projects together with the ability to retrieve them in order to start contributing to KDE as fast as possible.


### Improved Konsole Integration


The embedded Konsole in KDevelop has seen some improvements—when you use Bash, it is now possible to control the surrounding KDevelop session, i.e. to open files, create new ones, search through files and more. Type `help!` to find out what you can do there now.


### Source Formatting


Integrated source formatting got a bit better—you can now let it override the indentation settings of the embedded editor. Furthermore the "Custom Script Formatter", formerly known to support Gnu Indent, was extended to make it even easier to support custom formatting scripts. One example is the new `kdev_format_source.sh` script, shipped with KDevelop, which allows fine-grained formatting rules by placing format\_sources meta-files into the project's file system. Especially paired with the powerful uncrustify formatter, this enables you to work seamlessly on big, heterogeneously formatted projects.


### Various Bugfixes


We fixed [over 170 bugs](https://bugs.kde.org/buglist.cgi?list_id=7184&resolution=FIXED&chfieldto=Now&chfield=resolution&query_format=advanced&chfieldfrom=2011-06-25&chfieldvalue=FIXED&bug_status=RESOLVED&product=kdevelop&product=kdevplatform) since KDevelop 4.2.3. Among others, SVN 1.7 is now properly supported, various pieces of C++ code got improved, the GDB plugin got better and lots of crashes and other issues were resolved.


### Optimizations


Besides adding new features and improving stability, this release also comes with some noteworthy performance improvements—opening large projects with tons of files should be considerably faster now. Similarily, Quickopen is now even faster, and searching with it is more fluid when dealing with large projects.


### Forum


Our new [forum](http://forum.kde.org/viewforum.php?f=218) is the place to go if you need KDevelop support. Our [mailing lists](/mailinglists) as well as the #kdevelop IRC channel on freenode are also available, and we are happy to take your questions.


## Download


Your distribution should provide you with updated binary packages. If you want to have a look at changelogs though or really want to compile it on your own, then head over to the KDE mirrors:


<http://download.kde.org/download.php?url=stable/kdevelop/4.3.0/src/>


### Hashes


To verify the correctness of your downloaded packages, you can use the following SHA1 and MD5 hashes:


**MD5 sums:**



```

24915ac7c0340d6848abd3ba6a355df6  kdevelop-4.3.0.tar.bz2
d8c10d9d42ae412b8a968b7ab1fbffd9  kdevelop-php-1.3.0.tar.bz2
866e597b1f843ed48edbd48331a1d6cf  kdevelop-php-docs-1.3.0.tar.bz2
0db26d2927abac015ae960d67a144c95  kdevplatform-1.3.0.tar.bz2

```

**SHA1 sums:**



```

06a91f9ae0fc15bcec12a92bd9c30a7517b3b8e0  kdevelop-4.3.0.tar.bz2
8ae280cf5085c473b6b95b3683629b49d19dfe12  kdevelop-php-1.3.0.tar.bz2
398001b7d192af4c71bddb246dfd59d61db02618  kdevelop-php-docs-1.3.0.tar.bz2
990ec335418445ae11b8ee11aa9a25729994de59  kdevplatform-1.3.0.tar.bz2

```

## Wrapup


A big thank you to all the contributors who made this release possible. Many thanks to our users as well, for reporting bugs and making it such a fun and rewarding task to work on KDevelop!

