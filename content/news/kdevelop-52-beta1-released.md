---
title: "KDevelop 5.2 beta 1 released"
date: "2017-10-02 17:00:00+00:00"
lastmod: "2017-10-02 18:17:25+00:00"
author: "sbrauch"
aliases:
- /news/kdevelop-52-beta1-released
tags:
- release
- kdevelop
- unstable
- qt5
- clang
- windows
categories:
- News
---
We are happy to announce the release of KDevelop 5.2 Beta! Tons of new stuff entered KDevelop 5.2, a bigger blog post show-casing all the features in 5.2 will follow when we release the final version. Here's a brief summary of what's new in this version:


## Internal changes


After someone triggered discussions at Akademy, KDE's annual developer conference, about why there are still two different repositories -- kdevelop.git and kdevplatform.git -- for the main functionality of KDevelop, the KDevelop team decided that it's time to ditch the idea of having two separate repositories and **merged kdevplatform.git into kdevelop.git**. This simplifies maintenance of the core bits of KDevelop, makes it easier for newcomers to understand where to find core KDevelop plugins, makes it easy to keep up with code changes, etc. pp.. In the past years, we've noticed that the separation caused more confusion than anything else to all stakeholders.


For end users, nothing will change. This is mostly interesting to developers and packagers: the kdevplatform package can now simply be removed.


## Analyzers


With 5.1, KDevelop got a new menu entry *Analyzer* which features a set of actions to work with analyzer-like plugins. During the last 5.2 development phase, we merged more analyzer plugins into kdevelop.git which are now shipped to you out of the box:


### Heaptrack


[Heaptrack](https://www.kdab.com/heaptrack-v1-0-0-release/) is a heap memory profiler for C/C++ Linux applications.


![heaptrack screenshot](/sites/www.kdevelop.org/files/inline-images/heaptrack.png)
### cppcheck


cppcheck is a well-known static analyzer for C++, and can now also be run from within KDevelop by default, showing issues inline.


[![KDevelop with Cppcheck integration](/sites/www.kdevelop.org/files/inline-images/kdevelop-cppcheck.png)](/sites/www.kdevelop.org/files/inline-images/kdevelop-cppcheck.png)
## Improved C++ support


A lot of work was done on stabilizing and improving our clang-based C++ language support. Notable fixes include:


* Properly pass on some categories of compiler flags from the build system to the analyzer, fixing e. g. parse errors in some Qt header files which cannot be parsed if a certain compiler configuration is not respected
* Improve performance of C++ code completion in some situations
* Restore some completion features from 4.x, such as automatic insertion of semicolons in some cases


## Improved PHP language support


Thanks to Matthijs Tijink we've got many improvements for the PHP language support. The number of syntax warnings with modern PHP code should be greatly reduced, and the type inference is better.


For lack of a better summary, we're listing the changes here with their accompanying change messages:


* Add callable type to PHP. ([commit.](https://commits.kde.org/kdev-php/bfc97e1e0f5feebfb6f2a7bb3dc59013a4b11de5) code review [D7034](https://phabricator.kde.org/D7034))
* Process member properties/calls for unsure types. ([commit.](https://commits.kde.org/kdev-php/ac25815f77bb9e0006cccedbdd4ae53c5d479cc5) code review [D6923](https://phabricator.kde.org/D6923))
* Use null default types for determining argument type. ([commit.](https://commits.kde.org/kdev-php/9730beeb1eaa1360a94d32fa96cb071199660c0d) code review [D6915](https://phabricator.kde.org/D6915))
* Fix uses of class in closure parameters and default values for functions. ([commit.](https://commits.kde.org/kdev-php/2c863b8a3071f057126d49dc55438020a821e2e4) code review [D6690](https://phabricator.kde.org/D6690))
* Fix bug 381123: crash while parsing PHP code. ([commit.](https://commits.kde.org/kdev-php/8862dde3b4ee101395c900e4041cc2a2534f153e) code review [D6837](https://phabricator.kde.org/D6837))
* Use return typehints in typebuilder. ([commit.](https://commits.kde.org/kdev-php/1b9bbdb7ff55323b1b764828bed24086bced61d1) code review [D6680](https://phabricator.kde.org/D6680))
* Mark return types as uses. ([commit.](https://commits.kde.org/kdev-php/cdd1a8e8a9509d1431e9dfc63f2e3cfd3c99abb3) code review [D6675](https://phabricator.kde.org/D6675))
* Allow non-scalar constants. ([commit.](https://commits.kde.org/kdev-php/3a8ad5cc279b6996ca29f622c195c7893566d805) code review [D6670](https://phabricator.kde.org/D6670))
* Add spaceship and null coalesce operators. ([commit.](https://commits.kde.org/kdev-php/8da5db996bf9bd8432f73335b30fe85db05decfa) code review [D6645](https://phabricator.kde.org/D6645))
* Better support iterable typehint. ([commit.](https://commits.kde.org/kdev-php/dad970d96beeb50b67c4af3040c79f41bd6307c3) code review [D6644](https://phabricator.kde.org/D6644))
* Add return typehint support. ([commit.](https://commits.kde.org/kdev-php/68f07d3a0098e9457156e156655b1afba0d0387e) code review [D6643](https://phabricator.kde.org/D6643))
* Add more typehints to arguments. ([commit.](https://commits.kde.org/kdev-php/7a6c8299090847f32946b9b2e0c5f1ff753ca177) code review [D6342](https://phabricator.kde.org/D6342))
* Add unit test for variadic functions with typehints. ([commit.](https://commits.kde.org/kdev-php/d7c27dbe59e79da7ad128c8932842500346c5ee4) code review [D6271](https://phabricator.kde.org/D6271))
* Support more cases of function argument unpacking. ([commit.](https://commits.kde.org/kdev-php/2bc3f4d2e613f7a5daed47c9119de149e253927b) code review [D6271](https://phabricator.kde.org/D6271))
* Support for variadic functions in documentation popup. ([commit.](https://commits.kde.org/kdev-php/1cb4b3e37e5caec095a6e14ebbc561616d9897ad) code review [D6256](https://phabricator.kde.org/D6256))
* Implement syntax support for function argument unpacking. ([commit.](https://commits.kde.org/kdev-php/077b02757c301e95d2d2f1e0f74be6a2c63ba0ab) code review [D5908](https://phabricator.kde.org/D5908))
* Fix duchain unit tests. ([commit.](https://commits.kde.org/kdev-php/fd433b23319af61b6ef586e9a9a19ec6d8a6801b) code review [D5817](https://phabricator.kde.org/D5817))
* Add support for variadic functions. ([commit.](https://commits.kde.org/kdev-php/17202e149e5495766334bb7fb4ce821165d4bf79) code review [D5703](https://phabricator.kde.org/D5703))
* Support of Class::{expr}() syntax from Php 5.4. ([commit.](https://commits.kde.org/kdev-php/c99456ae037d58c96a1ecb5727eff68cd6a7cd2a) code review [D4902](https://phabricator.kde.org/D4902))
* Support for $this as an array when implementing ArrayAccess. ([commit.](https://commits.kde.org/kdev-php/1f15b3b33a8bcc25dbb2e714b63a50e16d38761c) code review [D4776](https://phabricator.kde.org/D4776))
* Php7 IIFE syntax parsing. ([commit.](https://commits.kde.org/kdev-php/2bcdaa11f4d37b14cc0e016df9017224ca03f8d4) fixes bug [#370515](https://bugs.kde.org/370515). code review [D4391](https://phabricator.kde.org/D4391))


## Improved Python language support


Mostly thanks to Francis Herne, some cleanup has been done in the Python language plugin as well.


* Fixed a[false-positive warning](https://bugs.kde.org/show_bug.cgi?id=378083) when a name used in a closure was defined later in the file.
* Fixed highlighting of local variables in comprehensions and of parameters in lambda definitions.
* Infer the correct type when slicing a tuple with constant integers.
* Infer the correct type from `and` or `or` expressions (Nicolás Alvarez).
* Internal code cleanups.


## Ongoing support for other platforms


We're continuously improving the Windows version of KDevelop. For the Windows version, we upgraded the Qt version to 5.9.1, the KF5 version to 5.37 and the LLVM/Clang version to 5.0.0. Also noteworthy, on Windows, we now also ship QtWebEngine instead of QtWebKit for the documentation browser.


## Get it


Together with the source code, we again provide a prebuilt one-file-executable for 64-bit Linux, as well as binary installers for 32- and 64-bit Microsoft Windows. You can find them on our [download page](/download#experimental_releases) in the *Experimental Releases* section.


The 5.1.80 source code and signatures can be downloaded from [here](http://download.kde.org/unstable/kdevelop/5.1.80/src/).


Please give this version a try and as always let us know about any issues you find via our [bug tracker](https://bugs.kde.org).


