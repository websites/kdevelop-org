---
title: "KDevelop 4.7.3 Release"
date: "2016-01-31 21:41:48+00:00"
lastmod: "2016-02-28 21:10:54+00:00"
author: "milian"
aliases:
- /news/kdevelop-473-release
tags:
- release
- stable
- kde4
categories:
- News
---
Hello!

I have the pleasure to announce the new stable release of KDevelop 4.7.3. This is a bug fix
release increasing the stability of our KDE 4 based branch. Please update to
this version if you are currently using 4.7.2 or older. You can download the sources from the KDE mirrors at:

<http://download.kde.org/stable/kdevelop/4.7.3/src/>

Many thanks to everyone involved in this new release!

### ChangeLog for kdevplatform v1.7.3


* Kevin Funk: Silence some Clang warnings
* Kevin Funk: TopContextDynamicData: Fix bug in loadPartialData
* Pino Toscano: cmake: fix macro\_log\_feature for subversion
* René J.V. Bertin: Create m\_finishReview as a KAction instead of as a QAction, in order to prevent error messages.
* Milian Wolff: Make function a slot such that its invokation can be delayed.
* Kevin Funk: Dont hide/restore docks on double-click with RMB
* René J.V. Bertin: more sensible workaround for code review crash on OS X
* Zhang HuiJie: Revert "ensure topData->m\_importers/topData->m\_importedContexts is not nullptr, this fix crash while finding usage of var/function/class."
* Sven Brauch: backport null check from ad8947a2


### ChangeLog for kdevelop v4.7.3


* Milian Wolff: Make compile, sorry...!
* Milian Wolff: Don't access declaration without holding the DUChain lock.
* Kevin Funk: Silence some Clang warnings
* Zhang HuiJie: fix definite loop.


### ChangeLog for kdev-php v1.7.3


* Milian Wolff: Make sure we don't access a nullptr.


### ChangeLog for kdev-python v1.7.3-py3


* Pino Toscano: cmake: use CMAKE\_LIBRARY\_ARCHITECTURE
* Sven Brauch: do not require kdevplatform of the same patch version as kdev-python
* Sven Brauch: update version number to 1.7.3
* Sven Brauch: fix cmake macro name of python executable
