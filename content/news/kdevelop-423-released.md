---
title: "KDevelop 4.2.3 Released"
date: "2011-06-24 19:37:03+00:00"
lastmod: "2016-02-28 21:10:54+00:00"
author: "milian"
aliases:
- /kdevelop/kdevelop-423-released
tags:
- release
- stable
- kdevelop
- php
- kdevelop-pg-qt
categories:
- News
---
After two eventful months the KDevelop team is happy to announce the immediate availability of KDevelop 4.2.3. This is a stable release which fixes dozens of bugs, hence we urge every user to upgrade to this new release. Distributions are notified and will provide updated packages shortly.


As always many thanks to all the contributors who made this release possible.



### Download


The source code for KDevplatform 1.2.3, KDevelop 4.2.3 and PHP(-Docs) 1.2.3 is uploaded to the KDE FTP mirrors. To download, pick a mirror near you: [download KDevelop 4.2.3](http://download.kde.org/download.php?url=stable/kdevelop/4.2.3/src)


### Changes


#### ChangeLog for KDevplatform v1.2.3


* *Olivier JG:* Make declarations from macros get appended instead of prepended.
* *Milian Wolff:* make sure we don't crash on (broken?) declarations without internal context in create class wizard
* *Eike Hein:* Fix black scrollbars in context browser views.
* *Niko Sams:* step into/over instruction did the wrong thing
* *Nicolás Alvarez:* Fix more clang build errors.
* *Nicolás Alvarez:* In AppendedList, qualify call to KDevVarLengthArray::append with 'this'.
* *Christoph Thielecke:* calling PTHREAD\_MUTEX\_INITIALIZER is not nessary because pthread\_mutex\_init() does the work, also it causes compile error on some systems
* *Milian Wolff:* try to protect against crash triggered by nested event loops and failing svn jobs
* *Milian Wolff:* fix: left-hand operand of comma has no effect
* *Milian Wolff:* remove IRequired=IStatus from grepview


#### ChangeLog for KDevelop v4.2.3


* *Olivier JG:* Add testMacroDeclarationOrder to test\_cppcodegen.
* *Gerhard Stengel:* Make manpage plugin translateable, backport from master
* *Aleix Pol:* Don't clean the build directory if it's a source dir.
* *Aleix Pol:* Small code cleanup
* *Aleix Pol:* Make get\_filename\_component(PATH) return a path relative to the current directory like cmake does.
* *Aleix Pol:* little tweaking to simplify some unit test.
* *Aleix Pol:* Simplify cmake reloading logic. Add more assertions, should make it easier if there's anything crashing. I used kde-runtime to debug it and it didn't crash once after the changes.
* *Aleix Pol:* Simplify the code that calls the file(GLOB\*) commands.
* *Milian Wolff:* fix crash in ADL helper
* *Aleix Pol:* Properly support foreach IN LISTS and IN ITEMS.


#### ChangeLog for KDevelop-PHP v1.2.3


* *Milian Wolff:* remove custom target to track dependency on generated parser files
* *Milian Wolff:* properly support namespaced static variables
* *Milian Wolff:* backport from master: try unfiltered lookup as fallback when looking for structure type declaration
* *Milian Wolff:* backport from master: add unit test for bug, happens due to imported parent context getting removed when recompiling
