---
title: "KDevelop with Cppcheck Integration"
date: "2018-03-05 17:00:00+00:00"
lastmod: "2018-03-05 17:15:29+00:00"
author: "htower"
tags:
- kdevelop
- cppcheck
categories:
- Development
- News
---
Starting with 5.1 release KDevelop supports built-in integration with static-analysis tool [Cppcheck](http://cppcheck.sourceforge.net/). Cppcheck provides unique code analysis to detect bugs and focuses on detecting undefined behaviour and dangerous coding constructs. The goal is to detect only real errors in the code (i.e. have very few false positives). Such analysis is very useful for all projects, especially for projects with complex structure and large code volume. Convenient integration with the development environment greatly simplifies and speeds up the process of code checking, as there is no need to study the analyzer documentation, its manual configuration and code navigation when processing the analysis results.


To use cppcheck integration in KDevelop, you only need to:


1. Install cppcheck with using standard tools of your operating system.
2. Select the required check parameters in the project properties.
3. Run the check and analyze the code for found (potential) errors.


Let's take a closer look at the process of setting up and running the analyzer (we suppose that cppcheck is already installed).


## Configure cppcheck in KDevelop (optional)


First, you must verify that the path to the cppcheck executable is correct. The path should be automatically detected but if you install cppcheck to non-standard place or if you want to use non-system version such path must be setup through plugin configuration page: "Settings" -> "Configure KDevelop" -> "Analyzers" -> "Cppcheck":


![Cppcheck plugin config page](/sites/www.kdevelop.org/files/inline-images/Screenshot_20180305_163718.png)
By default "native" cppcheck's output it not shown during the check and also we hide it's XML output. This can be enabled with appropriate checkbox and output can be viewed manually in the standard "Test" tool view:


!["Test" tool view](/sites/www.kdevelop.org/files/inline-images/Screenshot_20180305_163901.png)
If cppcheck executable path is ok we can set up check parameters for the individual projects through project's configuration page.


## Configure check parameters for the individual projects (optional)


"Project" -> "Open Configuration" -> "Cppcheck":


![Project's configuration page](/sites/www.kdevelop.org/files/inline-images/Screenshot_20180305_164011.png)
The config page contains tabbar to control cppcheck behavior and auto-updated command line panel which displays the resulting cppcheck command line. First tab allows you enable/disable different types of analysis. Full description can be found in the cppcheck documentation and short version is displayed as tooltip for the selected checkbox.


Second page allows you setup include directories where cppcheck will try to find headers during analysis. By default we enable only "project" includes but you also enable "system" directories (like */usr/include/*). Note that enabling system directories can slowdown the analysis. You also can *block* some include directories if necessary:


![Include directories configuration](/sites/www.kdevelop.org/files/inline-images/Screenshot_20180305_164041.png)
Last tab allows you to pass custom extra parameters to the cppcheck. This may be useful since built-in GUI controls supports only commonly-used functionality. See cppcheck documentation for all supported parameters:


![Extra parameters settings](/sites/www.kdevelop.org/files/inline-images/Screenshot_20180305_164108.png)
When all configuration steps are finished press "OK"/"Apply" to save your changes. Next we are ready to start code analysis.


## Starting the cppcheck code analysis


This can be done with 3 ways:


1. From the main menu: "Code" -> "Analyze Current File/Project With".
2. From the editor context menu: "Analyze Current File/Project With".
3. From the "Projects" tool view context menu. You should select some element in the tree and call context menu: "Analyze With" -> "Cppcheck". Depending on the selection the check will be start for whole project, some directory or for the single file.


When the analysis is started the standard "Problems" tool view is activated and opens "Cppcheck" tab. All problems will be placed into the table and can be activated with mouse-click on appropriate line. When some problem is activated the corresponding source file will be open in the editor and cursor will be placed on error's line:


![Cppcheck errors view](/sites/www.kdevelop.org/files/inline-images/Screenshot_20180305_164614.png)
 


You should analyze the problem's code and fix it if necessary. Note that some errors are not errors but only cppcheck recommendations. Some errors can be false-positive cppcheck triggers therefore no fixes needed. Also some error lines displays information not associated with source code - when, for example, cppcheck can't find include paths for some headers. Note that cppcheck tool is not ideal and you should carefully analyze each error line in the report and decide to fix or ignore it.


After fixing the errors found by cppcheck the analysis can be restarted. This can be do as described earlier or by "one-click" on first button in the "Cppcheck" problems view ("Re-Run Last Cppcheck Analysis").


Presented cppcheck integration plugin provides simple and easy-to-use mechanism for checking your code for some common errors. Regular using of such analysis tool can help you to catch and fix many errors before they happens in the released version of your software.


