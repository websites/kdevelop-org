---
title: "KDevelop 4.4.0 Beta 1 Released"
date: "2012-08-14 16:06:09+00:00"
lastmod: "2016-02-28 21:10:54+00:00"
author: "milian"
aliases:
- /44/kdevelop-440-beta-1-released
tags:
- release
- kdevelop
- beta
- unstable
- 4.4
categories:
- News
---
After quite some downtime in the last summer weeks, the KDevelop team finally has a new release in the pipelines: KDevelop 4.4.0. Today we are happy to announce the availability of the first Beta release!

This release comes packed with bug fixes and improvements as usually. A special addition to the KDevelop experience is the [new welcome page](http://www.proli.net/2012/04/27/youre-welcome-to-kdevelop/), which should make it easier for newcomers to use KDevelop and improve the workflow for existing users as well. Looking at existing plugins, we note that among others, the CMake, CVS and Patch Review plugins have seen quite some improvements.

Please test this release and give us feedback to ensure a smooth 4.4 release.

## Download


You can find the source code available to download on the KDE mirrors: [Download KDevelop 4.4 Beta 1](http://download.kde.org/download.php?url=unstable/kdevelop/4.3.80/src/).

There, you can also find the changelogs in case you wonder what exactly has happened since KDevelop 4.3.1.

If you are a user of the Custom Buildsystem plugin, you will need to update it as well, see: [KDevelop Custom-Buildsystem 1.2.2](http://apaku.wordpress.com/2012/07/20/kdevelop-custom-buildsystem-1-2-2-released/).

## Hashes



To verify the correctness of your downloaded packages, you can use the following SHA1 and MD5 hashes:


kdevplatform-1.3.80.tar.bz2
MD5Sum: 92ab23c7267d88819bddc73ddac23bba
SHA1Sum: 790a597b1eba9f059a1f7c9a09beb237cb79b4b0
kdevelop-4.3.80.tar.bz2
MD5Sum: ba45caadc1a8022ef97a198ae9ef6e10
SHA1Sum: ab1c65139247f2e9744246b6fc1eb22b3bd53d51
kdevelop-php-1.3.80.tar.bz2
MD5Sum: 57622fafdd148a38d6bb14301ad638c0
SHA1Sum: 1fc4d0a479d239d393edcc9bd1995fd8e5da0692
kdevelop-php-docs-1.3.80.tar.bz2
MD5Sum: 81b9374dbedf4623d2b9d5a3888b979d
SHA1Sum: 7413fda5af9e68b8f7aced89f41decd9d43dc7cf


Many thanks to the contributors who made this release possible!