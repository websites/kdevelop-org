---
title: "KDevelop 5.6 released"
date: "2020-09-07 17:09:52+00:00"
lastmod: "2020-09-07 20:16:07+00:00"
author: "kossebau"
aliases:
- /news/kdevelop-560-released
tags:
- release
categories:
- News
---
We are happy to announce the availability of KDevelop 5.6 today.


This release brings half a year of work, focused mainly on stability, performance, and future maintainability. Many existing features have received small improvements again, and there is one highly-visible addition: optional display of inline notes for problems at the end of the line ([commit](https://commits.kde.org/kdevelop/b1a18c6644d756d70c9fbb2162a93048acf5658e)). Learn more at David Redondo's [blog post](https://blog.david-redondo.de/kde/2020/02/28/problems.html) introducing this feature.


![KDevelop 5.6.0 in action, showing inline problem notes](/sites/www.kdevelop.org/files/inline-images/KDevelop_5.6.0.png)
## Improved CMake project support


* Also highlight CMake 'Generating done' messages. ([commit](https://commits.kde.org/kdevelop/664b5496e13eea353cb58fd80648a519e2d149af))
* Do not remove project information when ChooseCMakeInterfaceJob fails. ([commit](https://commits.kde.org/kdevelop/f24761eebbd67806efcd1878fe8f434ad6d8f0f0))
* Fix a cmake test linker error with Qt 5.9. ([commit](https://commits.kde.org/kdevelop/01e9f1b618becae2d5190007602e8d3aa3e028c9))
* Optimize CMakeManager::fileInformation for path lookups. ([commit](https://commits.kde.org/kdevelop/5ce71a7adda34d19c9010bd48f696bf7e139b57b))
* CMake: trim build dir parameters we read from configuration files. ([commit](https://commits.kde.org/kdevelop/09f6bed85f3c8a75ac1695a8194cef4b4340900e))
* CMake: don't add .rule files to targets. ([commit](https://commits.kde.org/kdevelop/4075c5e5d08453ffec180986301d5af34f487803))
* CMake: skip utility targets with empty sources. ([commit](https://commits.kde.org/kdevelop/bbd36385c28c9ccc04d605b85d42534ca9ac7f40))
* Reload project when non-{generated,external} CMake file becomes dirty. ([commit](https://commits.kde.org/kdevelop/893b400bd2c65634c429a071efb25163ea837c3b))
* CMake-file-api: query for and parse cmakeFiles. ([commit](https://commits.kde.org/kdevelop/87265626d23a0f392e337bc1d0e93ae06a423884))
* Support grouping of cmake targets into sub folders. ([commit](https://commits.kde.org/kdevelop/ddca5c1fdd169aee750489958a38fb1c8edcee5f))
* Actually use the cmake-file-api for project import, if possible. ([commit](https://commits.kde.org/kdevelop/76ddc001950321190def89d2894cf452dd825870). fixes bug [#417202](https://bugs.kde.org/417202))
* Increase error reporting on CLI when cmake import fails. ([commit](https://commits.kde.org/kdevelop/10f3505c787c0f5ab1f2b35c1daf287fa812a8ad))
* Actually parse the cmake-file-api response files. ([commit](https://commits.kde.org/kdevelop/958bc087cc1804a054be553f1c81a325590afb12). See bug [#417202](https://bugs.kde.org/417202))
* Start implementation of cmake-file-api support. ([commit](https://commits.kde.org/kdevelop/d38ae65bc3b5304e13fd641b4971939e67f70d09). See bug [#417202](https://bugs.kde.org/417202))
* Improve error handling when failing to create cmake build dir. ([commit](https://commits.kde.org/kdevelop/23a57a29ad1a47133ae884954ae322dc3262483d))


## Improved C++ language support


* Take -U into account when extracting defines from compile flags. ([commit](https://commits.kde.org/kdevelop/6ed6cb31342f4f3d493e8dd440bb8f4fdc0fb9f8))
* Allow passing custom args to clang through clang-parser utility. ([commit](https://commits.kde.org/kdevelop/4f55b2e7bb9b30c1a919291822b24fe2f674b930))
* KDev-clang: don't skip unexposed declarations from different files. ([commit](https://commits.kde.org/kdevelop/a7f7c9ac6b8fc9254d7efe17adde7d95b613bb1c). fixes bug [#402066](https://bugs.kde.org/402066))


## Improved PHP language support


* Update phpfunctions.php to phpdoc revision 350290. ([commit](https://commits.kde.org/kdev-php/4d5ff9b290869e3676a25630b99558be1d8676ca))
* Support PHP 7.1's syntax for catching multiple exceptions. ([commit](https://commits.kde.org/kdev-php/8349069a884218d7fa5ff2c309a689217d20e581))


## Improved Python language support


* Support for Python 3.9. ([commit](https://commits.kde.org/kdev-python/05e95cac74b0bb3595a6d97da07a12b907385be5). fixes bug [#419290](https://bugs.kde.org/419290))
	+ Distro packages may be compiled against an earlier CPython version, and thus not include this.
* Set parser feature version correctly for Python 3.8+. ([commit](https://commits.kde.org/kdev-python/21dba4e71ffc56e96bf10893726ce319f2fc1ca3))
* Fix highlighting range bugs with CPython 3.8.2+ ([commit](https://commits.kde.org/kdev-python/f7950bafd0eb2cbc7632206ebb8588b641fefb9c))


## Other Changes


* Fix keyboard focus for plasmoid. ([commit](https://commits.kde.org/kdevelop/98e8ad4f5b2bfaf85288220feaf3eed48ebe64d6). fixes bug [#422774](https://bugs.kde.org/422774))
* Only reload sessions in dataengine when relevant files/paths change. ([commit](https://commits.kde.org/kdevelop/c6d53e9bb9c2ed57639be3c810f4b840f26c324a))
* Show empty sessions in data engine. ([commit](https://commits.kde.org/kdevelop/b16a1b64ea26367548c5980721d51b1d36197ff3))
* Sublime: fix tab bar base frame not being drawn between tabs & view status. ([commit](https://commits.kde.org/kdevelop/33e01168df9b5b9c2fe97e7583f3db1fc5bda561))
* Associate loaded breakpoints with a moving cursor. ([commit](https://commits.kde.org/kdevelop/ff7a0ea60d4d7546b09dafdfedfc0db18b59e722). fixes bug [#424431](https://bugs.kde.org/424431))
* Make sure DebugController is initialized before UI is shown. ([commit](https://commits.kde.org/kdevelop/cc3f98b89f30a2501fdb1d848714b9b3eedd0c85). fixes bug [#424430](https://bugs.kde.org/424430))
* Remove "text/x-diff" alias from supported MIME types. ([commit](https://commits.kde.org/kdevelop/3344e9bfa28a330ec48aa9c472b5a82225004f83))
* Support also new name KSysGuard of what was before named KF5SysGuard. ([commit](https://commits.kde.org/kdevelop/f5dd5e92c1bd750bb0d71a83ca3aea364101dac7))
* Execute: remember the option to kill a process that is already running. ([commit](https://commits.kde.org/kdevelop/9279c8e910371b9527f8ea6fefb27047a6f4c0e0))
* Make compile with MSVC++ 19.24. ([commit](https://commits.kde.org/kdevelop/1952386c834a0aa067be5992f9c48a5c49e0036f))
* Optimize and improve environment variable expansion. ([commit](https://commits.kde.org/kdevelop/9258497a8d09910c853867dd56c90b1df5222c99))
* Support escaping backslash before dollar in environment variables. ([commit](https://commits.kde.org/kdevelop/486b6a39c3cb585c5604707d86af276896b58843))
* Consider catchpoint to be a code breakpoint. ([commit](https://commits.kde.org/kdevelop/b9078cbd4b866be83aafa0bc9695374b4364801b))
* Actually remove temporary item repository directories. ([commit](https://commits.kde.org/kdevelop/73185564fa795c484713f0205380068282ec0f3e))
* Workaround behavior change in QUrl::adjusted(QUrl::NormalizePathSegments). ([commit](https://commits.kde.org/kdevelop/65cdabaf7dc41c7b1455de0cba7d8a971989ed46))
* Prevent recursion in environment variable expansion. ([commit](https://commits.kde.org/kdevelop/6efab5edcdca4700ebc3a600cfef66ba775c26cb))
* Don't crash when there is no documentation for a given URL. ([commit](https://commits.kde.org/kdevelop/4ab4219d5c8302ed97824787e76a50ed39bf6385))
* Skip unexposed DecompositionDecl. ([commit](https://commits.kde.org/kdevelop/28befed97e10f8764b48f33839081914789120d1))
* Allow expansion of custom defined variables inside other variables. ([commit](https://commits.kde.org/kdevelop/1e4fdcf30c73948c0fd8fa759f62fbba021d7fed))
* Allow usePreview=true for user-created source formatter styles. ([commit](https://commits.kde.org/kdevelop/fe268683f2221495750ce015207d66527c213f56))
* Clean up AStyleFormatter declarations. ([commit](https://commits.kde.org/kdevelop/1ec00f2fe3169ac4508f46067d5b5cd99e66a4c3))
* [Documentation] Allow link following between providers. ([commit](https://commits.kde.org/kdevelop/9f1f63fac21c6bbfd2cabceaf3fb7b5965fb6742))
* Only cache PCH and completion results for files opened in editor. ([commit](https://commits.kde.org/kdevelop/cc5d32e3d7b02dc7c8906583f751d3ac7023176a))
* Fix Ctrl+mouse\_scroll zoom of documentation view. ([commit](https://commits.kde.org/kdevelop/728e952a983fb33b974744fb73d1fbf039cbb7ae))
* Use data-error/warning/information icons to mark problems. ([commit](https://commits.kde.org/kdevelop/ea7bed89d9be0435129bc853cade662c633f5d25))
* Fix resetting documentation view's zoom factor via Ctrl+0 shortcut. ([commit](https://commits.kde.org/kdevelop/b278b8f8c9cf3ca47977a0ea3997ce5be4b67d38))
* Allow navigating with forward & back mouse buttons from CMake and ManPage home pages. ([commit](https://commits.kde.org/kdevelop/3bb743764b910df186272471266d1e4ac3d5fb65))
* Fix documentation view navigation via forward & back mouse buttons. ([commit](https://commits.kde.org/kdevelop/325c6e75a0d6ec334d4c351c240a46a69c61f703), [commit](https://commits.kde.org/kdevelop/fbea811e0941e6e6d01e4fd09a07184b34b67e85))
* Do not force update when reloading projects. ([commit](https://commits.kde.org/kdevelop/716ca19830fd55e97ab81cb8df26f97ea9d3e885))
* ProblemNavigationContext: fix pixel-scaled icons in problem tooltip. ([commit](https://commits.kde.org/kdevelop/bda22a5e479a212811ff06c9a178451faaaf7ef8))
* Jump to declaration when definition file doesn't belong to a project. ([commit](https://commits.kde.org/kdevelop/8ebac86b88322c392c933a3a163efa4384e4ab24))
* Do not canonicalize paths to non-existent files. ([commit](https://commits.kde.org/kdevelop/a7dc4800ddef84a5eb94f03a927117f8731ca33a))
* Do not include end-of-line markers in build dir settings. ([commit](https://commits.kde.org/kdevelop/3539e7da6097cea86f1e91fee438cef8bde23660))
* Don't crash when we fail to find a screen for the active tooltip. ([commit](https://commits.kde.org/kdevelop/e37549a4b04cd88174b968b37c7061376e0c6744). See bug [#417151](https://bugs.kde.org/417151))
* Unbreak duchainify. ([commit](https://commits.kde.org/kdevelop/e84eae14148787cc13c397c4f6e888f3f2e62630))
* Don't crash when signatures don't match in AdaptSignatureAssistant. ([commit](https://commits.kde.org/kdevelop/a5cf36fe47abf197d9fdee2a30f5b014d3feead5))


## Get it


Together with the source code, we again provide a pre-built one-file-executable for 64-bit Linux as an AppImage. You can find it on our [download page](https://www.kdevelop.org/download).


The 5.6.0 source code and signatures can be downloaded from [download.kde.org](https://download.kde.org/stable/kdevelop/5.6.0/src/).


Should you have any remarks or in case you find any issues in KDevelop 5.6, please let us [know](https://www.kdevelop.org/support).


