---
title: "KDevelop 5.4.5 released"
date: "2019-12-02 14:56:49+00:00"
lastmod: "2019-12-02 15:13:27+00:00"
author: "kossebau"
aliases:
- /news/kdevelop-545-released
tags:
- release
categories:
- News
---
We today provide a bugfix and localization update release with version 5.4.5. This release introduces no new features and as such is a safe and recommended update for everyone currently using a previous version of KDevelop 5.4.


You can find the updated Linux AppImage as well as the source code archives on our [download](/download) page.


## ChangeLog


### [kdevelop](https://commits.kde.org/kdevelop)


* Add missing forward declaration (qt5.14). ([commit](https://commits.kde.org/kdevelop/1c169b8f11e0f87cbc7e47a7d5fabc94d8207fd7))
* Fix compilation error with gcc 7.4.1, ambiguous overload. ([commit.](https://commits.kde.org/kdevelop/2c3626955cd4651fc8b3c888b59f9db1cecc565d) code review [D25639](https://phabricator.kde.org/D25639))
* Remove bad assert. ([commit](https://commits.kde.org/kdevelop/3d2ff6f3e43eb7f2cdcf2c1ef5fe2eba133c6ba1))
* Remove duplicate fuzzy character "/". ([commit](https://commits.kde.org/kdevelop/904f73c727adac1e3ceaa26c977d1a725557890c))
* Fix reformat for selected code. ([commit](https://commits.kde.org/kdevelop/7e79fd84c9dfff7a54b1d851eed20d8a38147830))
* Qthelp: Unbreak support for zipped/tarred QCH files from KNS. ([commit](https://commits.kde.org/kdevelop/744ea9718492e9c96f0f4883cc396fc51339d858))
* Qthelp: adapt KNewStuff config to move from collect.kde.org to store.kde.org. ([commit.](https://commits.kde.org/kdevelop/44458d9cc234f4b7748fdbbf6178e67be2d343e9) fixes bug [#377183](https://bugs.kde.org/377183))


### [kdev-python](https://commits.kde.org/kdev-python)


No user-relevant changes.


### [kdev-php](https://commits.kde.org/kdev-php)


* Update phpfunctions.php to phpdoc revision 348276. ([commit](https://commits.kde.org/kdev-php/983e9994e5f597e53eeafc37fd4a284f5373e599))


