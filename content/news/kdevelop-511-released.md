---
title: "KDevelop 5.1.1 released"
date: "2017-05-27 08:00:00+00:00"
lastmod: "2017-05-27 13:58:48+00:00"
author: "kfunk"
aliases:
- /news/kdevelop-511-released
tags:
- release
- kf5
- qt5
- 5.1
categories:
- News
---
We are happy to announce the release of KDevelop 5.1.1, the first bugfix and stabilization release for KDevelop 5.1!


## Get it


Together with the source code, we again provide a prebuilt one-file-executable for 64-bit Linux (AppImage), as well as binary installers for 32- and 64-bit Microsoft Windows. You can find them on our [download page](/download).


The 5.1.1 source code and signatures can be downloaded from [here](http://download.kde.org/stable/kdevelop/5.1.1/src/).


Please give this version a try and as always let us know about any issues you find via our [bug tracker](https://bugs.kde.org).


## ChangeLog


### Prebuilt binaries


* Windows installer: Fix missing icons on Windows installers.
* AppImage: Ship Breeze widget style. [T3538](https://phabricator.kde.org/T3538)
* AppImage: Ship Sonnet plugins (based on aspell, hunspell, hspell). [T4100](https://phabricator.kde.org/T4100)
* AppImage: Ship some default colour schemes (to be used with Settings -> Colour Scheme) with AppImage.
* AppImage: Built with KF5SysGuard support: Enables "Attach to process" in the AppImage. [T5878](https://phabricator.kde.org/T5878)


### [kdevplatform](https://commits.kde.org/kdevplatform)


* Do not extract all template preview images, load from archives on demand. [Commit.](https://commits.kde.org/kdevplatform/d7bba41f7eb924ca3775a52892f0c638cfc791f7) Phabricator Code review [D5701](https://phabricator.kde.org/D5701)
* Use https://www.google.com instead of http://www.google.de in google selection external script. [Commit.](https://commits.kde.org/kdevplatform/7d6e9bae579bd71f911301753ccf822f36fecab3) Phabricator Code review [D5719](https://phabricator.kde.org/D5719)
* Use consistent icon names for build stuff, remove left-over legacy icons. [Commit.](https://commits.kde.org/kdevplatform/8a4a7adb1fbe95b7985eecd41e65beb672541066) Phabricator Code review [D5651](https://phabricator.kde.org/D5651)
* Appwizard: fix broken disconnect in ProjectVcsPage. [Commit.](https://commits.kde.org/kdevplatform/8e7f962f7c47f490491d42f2c67ca18b34ebf38e) Phabricator Code review [D5536](https://phabricator.kde.org/D5536)
* Stop unused & broken exposure of Project object on D-Bus. [Commit.](https://commits.kde.org/kdevplatform/2763fe3d3de84e8663fcfb5994fea455b8957cd8) Phabricator Code review [D5607](https://phabricator.kde.org/D5607)
* Appwizard: store chosen vcsPlugin in developer .kdev4 file. [Commit.](https://commits.kde.org/kdevplatform/181341d713bfff9af0d90789596adf963bb25372) Phabricator Code review [D5513](https://phabricator.kde.org/D5513)
* Backgroundparser: Relax assert a bit. [Commit.](https://commits.kde.org/kdevplatform/76765692538d3af40374b0c9ae63eb6eb41c53c0) See bug [#378933](https://bugs.kde.org/378933)
* Work-around issue in Path(QString) ctor. [Commit.](https://commits.kde.org/kdevplatform/080ed286fe100f7c736d5a77c587e0a1b9453882) See bug [#378933](https://bugs.kde.org/378933)
* Fix preview file wrongly added on project generation from app template. [Commit.](https://commits.kde.org/kdevplatform/d1dcb8b94a97ed425051d08c4e9a20028f32a52a) Phabricator Code review [D5314](https://phabricator.kde.org/D5314)
* Fix support for multiple files and relative paths in ShowFilesAfterGeneration. [Commit.](https://commits.kde.org/kdevplatform/52713ba22fbaa3d5b0cad855df722e091e95ad51) Phabricator Code review [D5316](https://phabricator.kde.org/D5316)
* Load Template From File dialogs: fix wrong filter strings usage. [Commit.](https://commits.kde.org/kdevplatform/5c681b15930f4d3f33d1dbb471c02c6160ecf853) Fixes bug [#376040](https://bugs.kde.org/376040). Phabricator Code review [D5155](https://phabricator.kde.org/D5155)
* Find/Replace in files: Do not wrap content of tooltip for an output line. [Commit.](https://commits.kde.org/kdevplatform/97a40f748ff456e89b74cc8aef06e250e17e81ab) Phabricator Code review [D5135](https://phabricator.kde.org/D5135)


### [kdevelop](https://commits.kde.org/kdevelop)


* Install xdg mimetype definition for OpenCL C. [Commit.](https://commits.kde.org/kdevelop/fcd48d9fbc57c17c17a5e93771c90960aaa5f615) Phabricator Code review [D5621](https://phabricator.kde.org/D5621)
* Move print from int to unsigned int. [Commit.](https://commits.kde.org/kdevelop/c1c79f5684f2fbb4333a18b44ed6b6a0f8451701) Phabricator Code review [D5654](https://phabricator.kde.org/D5654)
* Fix build for MinGW. [Commit.](https://commits.kde.org/kdevelop/6f26b8ea98b86819d1d69f7002290285f1d1c853) Fixes bug [#379454](https://bugs.kde.org/379454)
* Look for Cppcheck as RUNTIME dependencies. [Commit.](https://commits.kde.org/kdevelop/dcf0c6e6a4ff27105f883810e654b2c0030b5124) Phabricator Code review [D5632](https://phabricator.kde.org/D5632)
* The OpenCL language is actually called OpenCL C. [Commit.](https://commits.kde.org/kdevelop/d27fd44324e6bc41e02d44c9ab2e2f2ca1655882) Phabricator Code review [D5485](https://phabricator.kde.org/D5485)
* Remove unneeded mimetype for \*.kdevinternal files. [Commit.](https://commits.kde.org/kdevelop/171f6e6b989d3365749fb3a3e734a6d4ddd1d4f4) Phabricator Code review [D5624](https://phabricator.kde.org/D5624)
* Create KAboutData object only after QApp instance, for working translations. [Commit.](https://commits.kde.org/kdevelop/5d068bc7a690e4ea656f3e68424c1b6513b47bca) Phabricator Code review [D5598](https://phabricator.kde.org/D5598)
* CMake - fix bug with dropping changed settings for existing build directory. [Commit.](https://commits.kde.org/kdevelop/15cf6348660c912d97603329dd60866670edea97) Phabricator Code review [D5609](https://phabricator.kde.org/D5609)
* Drop explicit %{PROJECTDIR}/ from templates' ShowFilesAfterGeneration. [Commit.](https://commits.kde.org/kdevelop/70047de580bccade85f15aef3a412870146edfe9) Phabricator Code review [D5531](https://phabricator.kde.org/D5531)
* Remove unused "VersionControl" entries from kdev4 samples/templates. [Commit.](https://commits.kde.org/kdevelop/0f3810caa8d4610b2e01836a4cea3057fece4347) Phabricator Code review [D5512](https://phabricator.kde.org/D5512)
* Fix ShowFilesAfterGeneration to match generated files. [Commit.](https://commits.kde.org/kdevelop/ec0f5f98dbe0b370380bc6fb16b18afed12ea675) Fixes bug [#378499](https://bugs.kde.org/378499)
* Update Qt logo image. [Commit.](https://commits.kde.org/kdevelop/3c80745c143bb37e6e29d2137b5f98985b4954bf) Phabricator Code review [D5278](https://phabricator.kde.org/D5278)


### [kdev-python](https://commits.kde.org/kdev-python)


* Fix crash in syntax fix-up code. [Commit.](https://commits.kde.org/kdev-python/6043407c4ee2e1b3eefac8e1f8c96a651b92a14e) Partially fixes bug [#378827](https://bugs.kde.org/378827).
* Pep8: Make pep8 warnings less annoying. [Commit.](https://commits.kde.org/kdev-python/e4aedbb2223d4d8ff6fc43bf5c49581e285d9015) Phabricator Code review [D5397](https://phabricator.kde.org/D5397)


### [kdev-php](https://commits.kde.org/kdev-php)


* Fix duchain unit tests. [Commit.](https://commits.kde.org/kdev-php/fd433b23319af61b6ef586e9a9a19ec6d8a6801b) Phabricator Code review [D5817](https://phabricator.kde.org/D5817)


