---
title: "kdev-python 1.7.3-py3 released"
date: "2016-01-15 10:22:42+00:00"
lastmod: "2016-08-21 22:08:16+00:00"
author: "sbrauch"
aliases:
- /kdev-python-173-py3-released
tags:
- release
- stable
- kdevelop
- kdev-python
- released
- kde4
categories:
- News
---
Due to a regression in kdev-python 1.7.2-py3 related to module search paths, I have prepared a new release which fixes this issue. All users of the 1.7-py3 series, especially the 1.7.2-py3 version are urged to upgrade as soon as possible. The issue only affects the Python 3 series, for Python 2 kdev-python 1.7.2 is still the recommended and up-to-date release.


### Download


You can download the tarball from the KDE mirrors: <http://download.kde.org/stable/kdevelop/kdev-python/1.7.3/src/kdev-python-v1.7.3-py3.tar.xz.mirrorlist>. 


