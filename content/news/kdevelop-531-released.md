---
title: "KDevelop 5.3.1 released"
date: "2018-12-20 20:45:46+00:00"
lastmod: "2018-12-21 10:38:56+00:00"
author: "kfunk"
aliases:
- /news/kdevelop-531-released
tags:
- release
- 5.3
categories:
- News
---
We today provide a stabilization and bugfix release with version 5.3.1. This is a bugfix-only release, which introduces no new features and as such is a safe and recommended update for everyone currently using KDevelop 5.3.0.


**Important changes to AppImage:**There have been a couple of significant changes to the AppImage, please check whether it still works for you distribution. The most significant changes were: AppImages are now created on a more recent version of CentOS, now 6.10 instead of 6.8 (which is EOL), plus we no longer ship libfontconfig (cf. [commit](https://commits.kde.org/kdevelop/c33cd259ef149e4c7c10c8208199bd9d513ff27e)), libfreetype & libz (cf. [commit](https://commits.kde.org/kdevelop/31e662eceff1385a831f9ccd2ac75db8637953ac)).


You can find the updated Windows 32- and 64 bit installers, the Linux AppImage, as well as the source code archives on our [download](/download) page.


## ChangeLog


### [kdevelop](https://commits.kde.org/kdevelop)


* Remove the CVS plugin. This was unusable and would crash if used in most cases, but accidentally remained in 5.3.0 ([commit.](https://cgit.kde.org/kdevelop.git/commit/?id=0fc8dddfc8f603084af6c723473fedd300ae62ae) code review [D8950](https://phabricator.kde.org/D8950))
* Don't add 'override' specifier for non-modern project settings. ([commit.](https://commits.kde.org/kdevelop/27cca2f34f407d02212b15deb69d4506ce30d8f0) fixes bug [#372280](https://bugs.kde.org/372280). code review [D16773](https://phabricator.kde.org/D16773))
* Use qCWarning in path.cpp. ([commit.](https://commits.kde.org/kdevelop/22c9a57f3b728a6a9fdee716d420ab2f3c9dcf80) code review [D17174](https://phabricator.kde.org/D17174))
* Fix qmakeproject unittest to work on Windows. ([commit.](https://commits.kde.org/kdevelop/e3de595335ed455ed93b472afafa9097bf6920a2) code review [D17158](https://phabricator.kde.org/D17158))
* Fix defines and includes test to run on Windows. ([commit.](https://commits.kde.org/kdevelop/b1ba6403454abdb75f9fef5662505a9fcca4e09e) code review [D17156](https://phabricator.kde.org/D17156))
* Fix projectmodel unittest to work on Windows. ([commit.](https://commits.kde.org/kdevelop/de81ab5c5b484af242d48137e7ef95fd09aa1c60) code review [D17130](https://phabricator.kde.org/D17130))
* Make the filteringstrategy test work on Windows as well. ([commit.](https://commits.kde.org/kdevelop/d46ed7014365ba285a1a7ad3000b87352818c76d) code review [D17098](https://phabricator.kde.org/D17098))
* Fix problem model test on Windows. Make sure that when we create a path the root of that path is created according to what is expected on the current platform. E.g. C: on Windows. ([commit.](https://commits.kde.org/kdevelop/436303a96e42fd964008a3c144dc89e4f98f6e53) code review [D17131](https://phabricator.kde.org/D17131))
* Appimage: use original app desktop file as base for appimage one. ([commit.](https://commits.kde.org/kdevelop/278ec086b0e8eec874b59d8e2b3ad4af7a8b2f72) code review [D17027](https://phabricator.kde.org/D17027))
* Make test outputmodel pass on windows. ([commit.](https://commits.kde.org/kdevelop/3dae49147893cd31c8c5ee37461dffcc027dfffd) code review [D17077](https://phabricator.kde.org/D17077))
* Increase timeout for duchain gcc compatibility test. ([commit.](https://commits.kde.org/kdevelop/6b645175138eca4e785ce625f46b47f33ea7dcb6) code review [D16541](https://phabricator.kde.org/D16541))
* Auto-find Clazy documentation also with clazy >= 1.4. ([commit.](https://commits.kde.org/kdevelop/fee3dd685a6c8d2eaa3ba1fa7e1554d9ce15a4bd) fixes bug [#401075](https://bugs.kde.org/401075). code review [D16910](https://phabricator.kde.org/D16910))
* Disable plugin project config if project without a IBuildSystemManager dep. ([commit.](https://commits.kde.org/kdevelop/a3c782ac37cdcd2dc1ac4c665112aef87de8ab27) fixes bug [#400769](https://bugs.kde.org/400769). code review [D16915](https://phabricator.kde.org/D16915))
* Appimage: Do not bundle libfontconfig. ([commit.](https://commits.kde.org/kdevelop/c33cd259ef149e4c7c10c8208199bd9d513ff27e) code review [D16893](https://phabricator.kde.org/D16893))
* Cmake: fix missing addition of policies to documentation index. ([commit.](https://commits.kde.org/kdevelop/c88472647102433d383c95f330aafaff8d6d9840) code review [D15882](https://phabricator.kde.org/D15882))
* FunctionDefinition: only look for (new/other) function definition if we don't have one. ([commit.](https://commits.kde.org/kdevelop/3c51faa280abf1f76a68e5e79c04f65ca708633b) code review [D16356](https://phabricator.kde.org/D16356))
* Fix memleaks in duchain unittests. ([commit.](https://commits.kde.org/kdevelop/c3c30113bd8ff80cc93ef53164968d7662097fd0) code review [D16458](https://phabricator.kde.org/D16458))


### [kdev-python](https://commits.kde.org/kdev-python)


* Remove two outdated methods from shipped built-in documentation


### [kdev-php](https://commits.kde.org/kdev-php)


* Fix handling of array indexes after ClassNameReferences. ([commit.](https://commits.kde.org/kdev-php/4e07839f5a978193ad10b4e3d7cc6b282c350d3f) fixes bug [#401278](https://bugs.kde.org/401278))
* Fix syntax support for dynamic member access. ([commit.](https://commits.kde.org/kdev-php/2469eba750b2b52107e0c2671c3c11a9d87539f8) fixes bug [#400294](https://bugs.kde.org/400294). code review [D16461](https://phabricator.kde.org/D16461))


