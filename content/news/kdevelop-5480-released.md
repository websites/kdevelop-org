---
title: "KDevelop 5.5 beta 1 released"
date: "2020-01-22 21:51:24+00:00"
lastmod: "2020-01-22 23:14:55+00:00"
author: "kossebau"
aliases:
- /news/kdevelop-5480-released
tags:
- release
categories:
- News
---
We are happy to announce the release of KDevelop 5.5 Beta 1!


5.5 as a new feature version of KDevelop will bring half a year of small improvements to features across the application. Full details will be given in the announcement of the KDevelop 5.5.0 release, which is currently scheduled for in less than 2 weeks.


## Downloads


You can find the Linux AppImage ([learn about AppImage](https://appimage.org/)) here: [KDevelop 5.5 beta 1 AppImage (64-bit)](https://download.kde.org/unstable/kdevelop/5.4.80/bin/linux/KDevelop-5.4.80-x86_64.AppImage) (verify by: [GPG signature](https://download.kde.org/unstable/kdevelop/5.4.80/bin/linux/KDevelop-5.4.80-x86_64.AppImage.sig), keys linked on [Download](/download) page)
Download the file and make it executable (chmod +x KDevelop-5.4.80-x86\_64.AppImage), then run it (./KDevelop-5.4.80-x86\_64.AppImage).


The source code can be found here: [KDevelop 5.5 beta 1 source code](https://download.kde.org/unstable/kdevelop/5.4.80/src/)


Windows installers are currently not offered, we are looking for someone interested to take care of that.


