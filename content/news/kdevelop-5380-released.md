---
title: "KDevelop 5.4 beta 1 released"
date: "2019-07-22 16:30:00+00:00"
lastmod: "2019-07-22 16:53:56+00:00"
author: "kossebau"
aliases:
- /news/kdevelop-5380-released
tags:
- release
categories:
- News
---
We are happy to announce the release of KDevelop 5.4 Beta 1!


5.4 as a new feature version of KDevelop will among other things add some first support for projects using the [Meson build system](https://mesonbuild.com/) and have the [Clang-Tidy](https://clang.llvm.org/extra/clang-tidy/) support plugin merged as part of built-in plugins. It also brings 11 months of small improvements across the application. Full details will be given in the announcement of the KDevelop 5.4.0 release, which is currently scheduled for in 2 weeks.


## Downloads


You can find the Linux AppImage ([learn about AppImage](https://appimage.org/)) here: [KDevelop 5.4 beta 1 AppImage (64-bit)](https://download.kde.org/unstable/kdevelop/5.3.80/bin/linux/KDevelop-5.3.80-x86_64.AppImage)  
Download the file and make it executable (chmod +x KDevelop-5.3.80-x86\_64.AppImage), then run it (./KDevelop-5.3.80-x86\_64.AppImage).


The source code can be found here: [KDevelop 5.4 beta 1 source code](https://download.kde.org/unstable/kdevelop/5.3.80/src/)


Windows installers are currently not offered, we are looking for someone interested to take care of that.


