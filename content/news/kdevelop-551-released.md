---
title: "KDevelop 5.5.1 released"
date: "2020-05-05 18:05:00+00:00"
lastmod: "2020-06-02 13:46:57+00:00"
author: "kossebau"
aliases:
- /news/kdevelop-551-released
tags:
- release
categories:
- News
---
We today provide a bug fix and localization update release with version 5.5.1. This release introduces no new features and as such is a safe and recommended update for everyone currently using a previous version of KDevelop 5.5.


You can find the updated Linux AppImage as well as the source code archives on our [download](/download) page.


Should you have any remarks or in case you find any issues in KDevelop 5.5, please let us [know](https://www.kdevelop.org/support).


## ChangeLog


### [kdevelop](https://commits.kde.org/kdevelop)


* Clang-tidy: fix storing of custom checksets per project. ([commit](https://commits.kde.org/kdevelop/80b362ad702e03fa71771653c97bbd7e4db46578))
* Clazy: fix storing of custom checksets per project. ([commit](https://commits.kde.org/kdevelop/5c829204ea1d7ad97743cb420eed10720e96a2c8))
* Kdev\_format\_source: eliminate a global QString constant. ([commit](https://commits.kde.org/kdevelop/af51b25a624a8105a7d73fbfbea1df35be4d7af3))
* Reduce the scope of local variables in KDevFormatFile::read(). ([commit](https://commits.kde.org/kdevelop/d6a254052de020653069842c5b936aa9f6e029b2))
* Move a format\_sources comment from unsupported position. ([commit](https://commits.kde.org/kdevelop/c3a7739d07400dd7104a32c9c6afea1ac5393d65))
* Kdev\_format\_source: don't ignore the root directory. ([commit](https://commits.kde.org/kdevelop/37a8c9c7394da4ace5451d51745e8294321ff5ad))
* Kdev\_format\_source: fix typos in usage/error messages. ([commit](https://commits.kde.org/kdevelop/09dd4b92ad62f068a774695c31cd2c905b5db7a4))
* Work around QDir::match() behavior change in Qt 5.12.0. ([commit](https://commits.kde.org/kdevelop/f56cdda54b7f88b119f2c7cfb2f534b4fe74478f))
* Add failing kdev\_format\_source and grepview tests. ([commit](https://commits.kde.org/kdevelop/804098422cc6dd72c432ed9544002eba6db02f3a))
* Handle errors better in KDevFormatFile::executeCommand(). ([commit](https://commits.kde.org/kdevelop/607b3fe68590a3d88a50460eb431c75b4e919bf4))
* Grepview: search in hidden files and directories. ([commit](https://commits.kde.org/kdevelop/66e4f3b3847dfab6c4f9c4f9ad2488611384b677))
* FindClang.cmake: Add 10 to the allowed versions. ([commit](https://commits.kde.org/kdevelop/8abc96ce3b27156ca453603ec41ae7e278205f50))
* Disable CSS images workaround for Qt WebEngine build. ([commit](https://commits.kde.org/kdevelop/ccef3c77999fc911a83583900da5bb3cfe7d0c45))
* Disable extra context menu in Documentation tool view. ([commit](https://commits.kde.org/kdevelop/43d73f904f5296bb3ce66788fccfd38e090f1692))
* Fix flickering when loading documentation. ([commit](https://commits.kde.org/kdevelop/9e4e8a213de75a62e507a2255b399dd3c55a5535))
* [Standard Documentation View] Set the correct mime type for job replies. ([commit](https://commits.kde.org/kdevelop/0d9bc18527f326a5956cd62d06644298f9041584))
* Breakpoint icon: add 32 px sized breakpoint icon (also pixel-align SVG). ([commit](https://commits.kde.org/kdevelop/5ca44ca2c5178eb087e4a4022a4919efad62da73))
* IADM: take C++ standard from build system manager into account. ([commit](https://commits.kde.org/kdevelop/a8ceb430be1fd75c172f3a6ec2b4652288bca3f7). fixes bug [#417903](https://bugs.kde.org/417903))
* Clear selection when updating cursor position. ([commit](https://commits.kde.org/kdevelop/f8754f08ac096100b5160940a0003ad4d52c2a57). fixes bug [#415644](https://bugs.kde.org/415644))
* Don't crash when we fail to find a screen for the active tooltip. ([commit](https://commits.kde.org/kdevelop/aa7bc707be21ac268a380a6b4ad972f62b3a72df). See bug [#417151](https://bugs.kde.org/417151))
* Kdev-clang: don't skip unexposed declarations from different files. ([commit](https://commits.kde.org/kdevelop/11cdf99776e4ac039a0351404f76ab697f862ff2). fixes bug [#402066](https://bugs.kde.org/402066))


### [kdev-python](https://commits.kde.org/kdev-python)


No user-relevant changes.


### [kdev-php](https://commits.kde.org/kdev-php)


* Fix a crash when showing the documentation popup. ([commit](https://commits.kde.org/kdev-php/6799a48cbf7c2b5832d45cb947b73378ab69107a))
* Update phpfunctions.php to phpdoc revision 349647. ([commit](https://commits.kde.org/kdev-php/46bca3ec7f6c95348ed73bf90874f500c236ff6b))


