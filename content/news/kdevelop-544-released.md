---
title: "KDevelop 5.4.4 released"
date: "2019-11-04 18:02:49+00:00"
lastmod: "2019-11-04 18:11:32+00:00"
author: "kossebau"
aliases:
- /news/kdevelop-544-released
tags:
- release
categories:
- News
---
We today provide a bugfix and localization update release with version 5.4.4. This release introduces no new features and as such is a safe and recommended update for everyone currently using a previous version of KDevelop 5.4.


You can find the updated Linux AppImage as well as the source code archives on our [download](/download) page.


## ChangeLog


### [kdevelop](https://commits.kde.org/kdevelop)


* Fix copyright date display in About KDevelop/KDevPlatform dialogs. ([commit.](https://commits.kde.org/kdevelop/748ec8549f99893c29169c7603a16990ddc6cfa0) fixes bug [#413390](https://bugs.kde.org/413390))
* FindClang.cmake: also search LLVM version 9. ([commit](https://commits.kde.org/kdevelop/9f82318685ddbd4c4346e00d9e06ff40b4db05cb))
* Clang: Workaround for empty problem ranges at start of document. ([commit](https://commits.kde.org/kdevelop/2384b2baee0e6d49358f8605e61f1db72dc53392))


### [kdev-python](https://commits.kde.org/kdev-python)


No user-relevant changes.


### [kdev-php](https://commits.kde.org/kdev-php)


No user-relevant changes.


