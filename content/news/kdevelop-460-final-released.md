---
title: "KDevelop 4.6.0 Final Released"
date: "2013-12-09 18:27:40+00:00"
lastmod: "2016-02-28 21:10:54+00:00"
author: "milian"
aliases:
- /46/kdevelop-460-final-released
tags:
- release
- stable
- kdevelop
- 4.6
categories:
- News
---
We are happy to announce the immediate availability of KDevelop 4.6.0! It adds more than a thousand commits worth of 
bug fixes, performance improvements and new features.

KDevelop aims to create an IDE which doesn't get into your way and nevertheless provides a powerful
and versatile set of tools for software development. Support for C++ and CMake are KDevelop's most prominent
and widely used features, but the 4.6.0 release -- among other things -- also continues to improve
the language support for PHP and Python.

![](/sites/kdevelop.org/files/kdev-splash.png)


## What's New



Aside from many bug fixes and general improvements such as performance optimizations in various areas, which make KDevelop 4.6.0 faster and less memory-hungry, a few changes are especially noteworthy:

The ReviewBoard plugin now supports updating existing review requests. There is a new plugin which adds support for building projects with ninja instead of make. CMake support was also stabilized and improved, adding support for missing or new CMake features.

### Startup



KDevelop comes with a shiny new animated Splash Screen. Furthermore you can now open files in the currently running session easily from the command line via `kdevelop $file`.

### UI Cleanups



The area tabs ("Code", "Review", "Debug") have been replaced with a unified button, which should make it more obvious how they are supposed to be used. You change the areas by invoking an action such as "Debug Process" and then "Go Back" to coding if you are finished in the "Debug" area. Related to that, the Working Sets are now identified by a fresh, consistent set of icons.

### Project Management


[![](http://milianw.de/sites/default/files/kdev-projectfilter-ctxmenu.png)](http://milianw.de/blog/kdevelop-project-filters)



The new Project Filter plugin provides a poweful tool to define which files you want to be part of your project. You can access it via the context menu in the project tree view, or from within the project settings. The old filter line edit in the project tree view was removed in favor of this more powerful and optimized approach to filtering.

### C++



Support for various C++11 features was added to the C++ language plugin. A shiny new feature is the so called look-ahead completion, which offers potential matches from member variables or functions accessible after dereferencing object or pointer types. Some notable performance improvements were made and in general the code has been cleaned up, making it more robust.

### Python



Python language support has seen various improvements in its static analyzer, as well as in the code completion component. Especially, relative imports are now supported; types can be deduced from isinstance() calls; and there is now a built-in tool to extract information from non-Python libraries to be used by the IDE to provide better support for them.

### PHP



The PHP language plugin received support for various new features introduced by the last PHP releases. As such, the editor now properly understands late static binding from PHP 5.3. From PHP 5.4 comes the support for the new short array syntax, function array dereferencing and trait declarations. Finally, the newstring and array literal dereferencing from PHP 5.5 is also supported.

### Improved GDB Support


[![](/sites/kdevelop.org/files/imagecache/thumb/photos/registers.png)](/screenshots/registers-toolview)



The debug session is now correctly closed when a program exits under specific conditions. Breakpoint modifications such as disabling or enabling them takes effect immediately now, with no need to stop running program manually anymore.
Debugging from external terminals has been improved, and it is now possible to debug from konsole, gnome-terminal, xfce4-terminal, xterm and probably other terminals. The Breakpoints toolview now displays only file names and the full path is accessible through a tooltip, which simplifies the UI and makes it easier to understand which files contain breakpoints.

A new feature is the addition of a CPU registers toolview. It shows, and offers to edit, all user mode registers and general purpose flags for x86/x86\_64 and armV7 architectures. This toolview combined with the Disassemble toolview makes it possible to control and watch a program's behavior on a very low level. Obviously that is not a feature used everyday, but for some specific purposes it can be very useful, e.g. for debugging C/C++ code with assembly inclusions, for testing ring 3 protection algorithms/techniques or for reverse engineering. Also it comes in handy if you debug an application without debug symbols in which case disassemble and registers are the only source of information about what is going on.

## Get it!



Most KDevelop users should get a fresh copy of the new version via their distributions packager. Otherwise you can download the sources from the official KDE mirrors:

[http://download.kde.org/stable/kdevelop/4.6.0/src/](http://download.kde.org/stable/kdevelop/4.6.0/src/ "Download KDevelop 4.6.0 Sources")



To verify the integrity of your download, use the following hash sums for comparison:

### MD5 Hash Sums



```
* 0c1a1e880d27990246e3bdd712db1037  kdevelop-4.6.0.tar.xz
* 83a984a090596dcce0a71ed63aa1e037  kdevelop-php-1.6.0.tar.xz
* 44d324bfad5598d0201e4208e2bf4c8f  kdevelop-php-docs-1.6.0.tar.xz
* 26d78c5374d35ec48d38b780e011883a  kdevplatform-1.6.0.tar.xz
* 146ca82161dada9d32dc31e7e3e1a19d  kdev-python-1.6.0.tar.xz
```

### SHA1 Hash Sums



```
* 2b723c3e16c41dbcd9817336893eff85712cb11d  kdevelop-4.6.0.tar.xz
* 705b1f36a6fbbac207e8ca78619827206b75b48e  kdevelop-php-1.6.0.tar.xz
* 98d7efe6f983c4b439c6cbdc40d427e14954298f  kdevelop-php-docs-1.6.0.tar.xz
* 8ed8ccb63b64a3675716f1ad14b4a73fcbf4133d  kdevplatform-1.6.0.tar.xz
* 88618dbd3a80d5193f49f6c61a6ad02a5e9c0652  kdev-python-1.6.0.tar.xz
```

## Thanks

To wrap up let us all thank the various contributors who made this latest KDevelop release possible! They are, in alphabetic order:

Albert Vaca, Aleix Pol, Alexander Mezin, Alexandre Courbot, Alexey Morozov, Allen Winter, Andras Mantia, Andrea Scarpino, Andreas Pakulat, Aurélien Gâteau, Bartosz Matuszewski, Ben Wagner, Burkhard Lück, Burkhard Lueck, Christophe Giboudeaux, Christoph Feck, David E. Narváez, David Faure, David Nolden, Florian Eßer, Gabo Menna, Ghislain Mary, Hannes Hofer, Heinz Wiesinger, Ivan Shapovalov, Jekyll Wu, Jon Mease, Jørgen Kvalsvik, Juliano F. Ravasi, Kevin Funk, Lambert Clara, Lasse Liehu, Laurent Navet, Luigi Toscano, Maciej Cencora, Marcel Hellwig, Max Schwarz, Meenakshi Khorana, Miha Čančula, Milian Wolff, Miquel Canes Gonzalez, Miquel Sabaté, Morten Danielsen Volden, Nicolai Hähnle, Nicolás Alvarez, Niko Sams, Olivier JG, Pino Toscano, Przemek Czekaj, Sebastian Kügler, Sergey Vidyuk, Steffen Ohrendorf, Sven Brauch, Vlas Puhov, Yuri Chornoivan, Zaar Hai

Thank you all!

