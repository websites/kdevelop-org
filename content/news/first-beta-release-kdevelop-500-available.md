---
title: "First Beta release of KDevelop 5.0.0 available"
date: "2015-10-25 13:50:10+00:00"
lastmod: "2016-02-28 21:10:54+00:00"
author: "milian"
aliases:
- /news/first-beta-release-kdevelop-500-available
tags:
- release
- beta
- unstable
- kf5
- qt5
- clang
- windows
- mac
- 5.0
categories:
- News
---
After more than a year of continuous work by our community, I'm very pleased to finally announce the first beta release of KDevelop 5.0.0. This release is made up of hundreds of commits, and marks a huge step forward:

* We have ported our huge code base to Qt 5 and KDE frameworks 5 (KF5).
* We replaced our legacy C++ parser and semantic analysis plugin with a much more powerful one that is based on Clang from the LLVM project.
* We removed the hand-written CMake interpreter and now leverage meta data provided by upstream CMake itself.
* We finally integrated semantic language support for QML and JavaScript, as well as a project manager for QMake.
* Finally, we cleaned up many areas of our code base and improved the performance of some work flows significantly.



### Qt 5 and KF5



We spent a lot of effort on keeping the porting bugs to a minimum, and our thanks go out to the many testers who have guided us in the process. Porting to KF5 and Qt 5 also cut down our dependencies, bringing us closer to a proper [KDevelop on Windows](/screenshots/kdevelop-5-pre-alpha-windows) and [KDevelop on Mac OS X](/screenshots/kdevelop-5-beta-1-mac-os-x). If you want to see KDevelop 5 become a reality on these platforms, then please [get in touch with us](/contribute-kdevelop) and help us iron out the last issues.

### Clang



KDevelop always prided itself for its state of the art C++ language support. We introduced innovative code browsing functionality, semantic highlighting and advanced code completion, features that our user base has come to rely upon for their daily work. All of this was made possible by a custom C++ parser, and an extremely complex semantic analyzer for the C++ language. Adding support for all the quirky corner cases in C++, as well as maintaining compatibility with the latest C++ language standards such as C++11, drained our energy and stole time needed to improve other areas of our IDE. Furthermore, the code was so brittle, that it was close to impossible to improve its performance or add bigger new features such as proper C language support.

Now, after close to two years of work, we finally have a solution to this dilemma: A Clang based language plugin. Not only does this give us support for the the very latest C++ language standard, it also enables true C and Objective-C language support. Furthermore, you get all of the immensely useful compiler warnings directly inside your editor. Even better, fixing these warnings is now often just a matter of [pressing a single button](http://kfunk.org/2014/04/28/gsoc-2014-improving-the-clang-integration-in-kdevelop/) to apply a Clang provided fix-it!

There are, however, a few caveats that need to be mentioned:

* On older machines the performance may be worse than with our legacy C++ support. But the new Clang based plugin finally scales properly with the number of cores on your CPU, which can lead to significantly improved performance on more modern machines.
* Some features of our legacy C++ support have not yet been ported to the new plugin. This includes special support for Qt code, most notably signal/slot code completion using the old Qt 4 macro syntax. We will be working on improving this situation and welcome feedback from our users on what we should focus on.
* The plugin works fine with Clang 3.6 and above, but some features, such as `auto`-type resolution in the code browser, or argument code completion hints within a function call, require unreleased extensions to Clang. The required changes have been contributed upstream by members of our community and we intend on continuing this effort. For the best C++ experience in KDevelop, we recommend a custom build of a recent Clang. Alternatively, stay tuned for Clang 3.8 early next year.


### CMake



Another area where KDevelop always excelled was its support for CMake based projects. Sadly, similar to the C++ story above, this came at a huge maintenance cost for a custom CMake interpreter. Keeping compatibility with the rapid upstream development was not possible for our small team. As such, we decided on a radical cut for KDevelop 5 and removed our custom CMake interpreter. Instead, we now use meta data provided by CMake itself, giving us a correct listing of targets, as well as all include paths and defines for the source code files in your projects. These are the most important features required of a project manager in KDevelop. But this step also means that we had to remove some of the useful advanced CMake integration features, such as the wizards to add files to a target. We are aware of this situation, and plan to work together with upstream to bring back the removed functionality in the future. Hopefully, you agree that correctness and much improved performance, where opening even large CMake projects is now close to instant, makes up for the loss of functionality.

Another big benefit for us that now both, the new Clang plugin as well as the CMake plugin, are much easier to maintain. This opens up resources for the future to work on other areas of KDevelop, such as its UI, cross-platform support or the debugger integration. Stay tuned!

### QML, JavaScript and QMake



With KDevelop 5, we decided to officially include support for QML and JavaScript code and QMake projects. This functionality has been worked on for years in our playground and now, we finally incorporated these experimental plugins and will start to officially support them. Our thanks go to the Qt Creator community here, as we leverage their QML and JavaScript parser for our language support plugin. The new KDevelop plugin for QMake is simplistic but already very useful for many projects. If you are using more complicated QMake features and want to improve our interpreter, please [get in touch with us!](/contribute-kdevelop)
### Python, PHP, ...



Together with C++, QML and JavaScript, KDevelop 5 will continue to officially support Python 3 and PHP. In our playground we also have support for Ruby, and there are plans to integrate basic Go and Rust support. If you are interested in improving support for any of these languages in KDevelop, please [step up and start contributing!](/contribute-kdevelop)
### Download



You can download KDevelop 5.0.0 Beta 1 from the official KDE mirrors:

* KDevPlatform 4.90.90: <http://download.kde.org/unstable/kdevelop/4.90.90/src/kdevplatform-4.90.90.tar.xz.mirrorlist>
* KDevelop 4.90.90: <http://download.kde.org/unstable/kdevelop/4.90.90/src/kdevelop-4.90.90.tar.xz.mirrorlist>
* KDev-PHP 4.90.90: <http://download.kde.org/unstable/kdevelop/4.90.90/src/kdev-php-4.90.90.tar.xz.mirrorlist>
* KDev-Python 4.90.90: <http://download.kde.org/unstable/kdevelop/4.90.90/src/kdev-python-4.90.90.tar.xz.mirrorlist>


### Wrap Up



To conclude this long announcement, I want to express my sincere gratitude towards the dozens of contributors that have worked on KDevelop 5 over the last months. I'm very excited to see where we have gotten already, and am eagerly awaiting a final KDevelop 5 sometime next year. Up until then, I hope we can iron out the remaining blockers and polish this first release based on Qt 5 and KF5.

Cheers!