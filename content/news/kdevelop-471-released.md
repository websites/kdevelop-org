---
title: "KDevelop 4.7.1 Released"
date: "2015-02-06 00:29:26+00:00"
lastmod: "2016-02-28 21:10:54+00:00"
author: "milian"
aliases:
- /47/kdevelop-471-released
tags:
- release
- stable
- 4.7
- kde4
categories:
- News
---
Hello all!

it's my pleasure to announce the immediate availability of KDevelop 4.7.1. This release contains many improvements and bug fixes - everyone is urged to upgrade. Distributions should already provide updated packages, otherwise you can download via:

* [kdevplatform 1.7.1](http://download.kde.org/stable/kdevelop/4.7.1/src/kdevplatform-1.7.1.tar.xz.mirrorlist)
* [kdevelop 4.7.1](http://download.kde.org/stable/kdevelop/4.7.1/src/kdevelop-4.7.1.tar.xz.mirrorlist)
* [kdev-python 1.7.1-py3](http://download.kde.org/stable/kdevelop/4.7.1/src/kdev-python-1.7.1-py3.tar.xz.mirrorlist)
* [kdev-python 1.7.1-py2](http://download.kde.org/stable/kdevelop/4.7.1/src/kdev-python-1.7.1.tar.xz.mirrorlist)
* [kdev-php 1.7.1](http://download.kde.org/stable/kdevelop/4.7.1/src/kdevelop-php-1.7.1.tar.xz.mirrorlist)
* [kdev-php-docs 1.7.1](http://download.kde.org/stable/kdevelop/4.7.1/src/kdevelop-php-docs-1.7.1.tar.xz.mirrorlist)



Thanks to all contributors, users and bug reporters for making this release possible!

### Changelog


#### kdevelop v4.7.1



```

* Milian Wolff: Prepare release of 4.7.1
* Kevin Funk: Fix CMakePreferences sizing
* Kevin Funk: cpp: Fix conversion warnings
* Kevin Funk: PVS V555 fix
* Kevin Funk: includeswidget: Use KMessageWidget
* Kevin Funk: Lower-case project name
* René J.V. Bertin: Avoid setting CMake variables globally that also exist as target-specific.
* René J.V. Bertin: OS X: * additional protection against App Nap and automaticTermination * really set the BundleDisplayName * generate an application icon resource
* René J.V. Bertin: [OS X] Set the Info.plist bundle name to KDevelop and the Info String to the longer string. Also disable support for automatic termination in the Info.plist.
* Kevin Funk: mac: Use correct bundle version
* HuiJie Zhang: Fix bug on multi-line function-macros expansion,
* Milian Wolff: Delete some dead code
* René J.V. Bertin: fix indentation REVIEW: 121391
* René J.V. Bertin: [OS X] create a dedicated Info.plist for kdevelop REVIEW: 121394
* René J.V. Bertin: Avoid inappropriate warning about faulty ninja.build file from gdb/CMakeLists.txt REVIEW: 121391
* Kevin Funk: Hack for making build with CMake 2.8 and Ninja
* Kevin Funk: Remove .kdev4 file from subdirectory
* Kevin Funk: Revert "MissingDeclarationAssistant: Only show identifier"
* Kevin Funk: Disable missing decl assistant for namespaced ids
* Kevin Funk: Fix test in combination with GCC 4.9.1 and -O2
* Kevin Funk: Don't access dangling pointer
* Nicolas Werner: Fix environment-cd for paths containing spaces.
* Kevin Funk: Make compile for me
* Milian Wolff: Reduce QVariant usage.
* Milian Wolff: Fix regressions introduced by recent refactoring.
* Milian Wolff: Minor code cleanup
* Milian Wolff: Use the add_debuggable_executable macro everywhere its required.
* Milian Wolff: Revert "Use debugfull configuration whenever it is available."
* Milian Wolff: Use debugfull configuration whenever it is available.
* Milian Wolff: Refactor the custom defines and includes plugin.
* Milian Wolff: Fix regression: Don't remove the KDEV_USE_EXTENSION_INTERFACE.
* Milian Wolff: Fix assertion about unknown projects in the IDAIM.
* Aleix Pol: Change precedence of default include paths
* Milian Wolff: Fixup signal connection
* Milian Wolff: Fix compile with clang 3.5
* Pino Toscano: gdb: "#else if" does not exist, "#elif" does
* Pino Toscano: gdb: fix member order
* Pino Toscano: fix ByteArray/QByteArray typo
* René J.V. Bertin: Enables the debugger on OS X. Requires presence of a recent gdb version, e.g. from MacPorts. REVIEW: 120510
* Kevin Funk: Fix uninitialized member 'kind'

```

#### kdevplatform v1.7.1



```

* Milian Wolff: Don't crash when no service is available to open directories.
* Sebastien Speierer: Prevent new project in a non-empty folder.
* Milian Wolff: Prepare release of 1.7.1
* Milian Wolff: Prevent accessing invalid m_view pointer.
* Milian Wolff: Cleanup code to prevent crashes when accessing nullptr.
* Milian Wolff: Fix potential deadlock on shutdown.
* Kevin Funk: Fix access to nullptr
* Kevin Funk: projectfilter: Hide *.orig by default
* Kevin Funk: projectmanagerview: Fix bug on delayed initialization
* René J.V. Bertin: Restore upload functionality to git.reviewboard.kde.org
* Nicolai Hähnle: sublime: prevent accidentally adding nullptrs to ContainerPrivate::viewForWidget
* René J.V. Bertin: replace toAscii() with toLatin1() in KUrl conversions
* Kevin Funk: Fix possible crash in CodeHighlighting
* Kevin Funk: BreakpointWidget: Make less annoying
* Kevin Funk: BreakpointWidget: Fix look of header
* Kevin Funk: Detect more common Qt runtime warnings
* Kevin Funk: ProblemReporter: Improve appearance of the list
* Kevin Funk: Use KDevelop::htmlToPlainText where applicable
* Kevin Funk: Make the assistant popup less obtrusive
* Sven Brauch: Use an estimate of the text width to decide on assistant layout
* Kevin Funk: Introduce KDevelop::htmlToPlainText
* Kevin Funk: Don't use the undocumented qAppName()
* Milian Wolff: Hide inline private data type and use typedef for Defines more.
* Milian Wolff: Improve error reporting.
* Sven Brauch: Move __pycache__ to excluded folders, it's not a file
* Kevin Funk: Make compile on OSX
* Milian Wolff: Use kDebug instead of qDebug
* Kevin Funk: Fix unused variable
* Kevin Funk: Fix unused variable
* Kevin Funk: Revert "Make shell scripts portable and stable."
* Kevin Funk: Revert "Attempt to fix kdevplatform_shell_environment"
* Sergey Kalinichev: Don't start parse project job if there is nothing to parse.
* Kevin Funk: Make F4 work again
* Kevin Funk: externalscript: Fix crash in case of no active document
* Sergey Kalinichev: Use match accuracy when asking for mime type.
* René J.V. Bertin: Workaround a crash on Mac OS X after finishing code review.

```

#### kdev-php v1.7.1



```

* Milian Wolff: Prepare release of 1.7.1
* Milian Wolff: Don't crash on non-existing function body of closure statements.
* Milian Wolff: Do not crash on invalid declaration.

```

#### kdev-python v1.7.1



```

* Milian Wolff: Prepare release of 1.7.1
* Sven Brauch: Do not crash if documentation context is not yet parsed
* Sven Brauch: Fix crash when building completion items for functions with no arguments context

```
