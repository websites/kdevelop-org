---
title: "KDevelop 5.0.4 released"
date: "2017-03-04 00:00:00+00:00"
lastmod: "2017-03-18 01:37:54+00:00"
author: "kfunk"
aliases:
- /news/kdevelop-504-released
tags:
- release
- windows
- linux
- KDevelop 5
categories:
- News
---
Today, we are happy to announce the release of KDevelop 5.0.4, the fourth bug fix and stabilization release for KDevelop 5.0. An upgrade to 5.0.4 is strongly recommended to all users of 5.0.3 and below.

Together with the source code, we again provide a pre-built one-file-executable for 64-bit Linux (AppImage), as well as binary installers for 32- and 64-bit Microsoft Windows. You can find them on our [download page](http://kdevelop.org/download).


List of notable fixes and improvements since version 5.0.3:


## ChangeLog


### [kdevelop](https://cgit.kde.org/kdevelop.git/)


* QObject template: fix typo screwing redefinition of block extra\_definitions. [Commit.](https://cgit.kde.org/kdevelop.git/commit/?id=dc16bb5bf50a5044a60105e2af11ea0fb76d2e17)
* Update version number to 5.0.4. [Commit.](https://cgit.kde.org/kdevelop.git/commit/?id=886c0afc307d522207076e13c38bf7ef689da6bd)
* Clang: Fix missing DUChain lock. [Commit.](https://cgit.kde.org/kdevelop.git/commit/?id=adc39d42eae7a5b609b71fd0f276903c5c414f59)
* Custom-buildsystem: Fix crash while configuring. [Commit.](https://cgit.kde.org/kdevelop.git/commit/?id=5f1e4974d8fd2259425062f02635b1bc27b57495) See bug [#335470](https://bugs.kde.org/335470)
* Try to fix testGccCompatibility test for me. [Commit.](https://cgit.kde.org/kdevelop.git/commit/?id=87ce5ffed2d013ac07dfc53d9c9d75391fea5d7a)
* Clang: Also rename destructors when renaming class. [Commit.](https://cgit.kde.org/kdevelop.git/commit/?id=51658b81204090690ccecc682ef4b064f959d09f) Fixes bug [#373452](https://bugs.kde.org/373452)
* Fix bug with wrong CMake arguments during adding new build directory. [Commit.](https://cgit.kde.org/kdevelop.git/commit/?id=8393f2b438e66b78063b0cc4a72cd29f144b59d7)
* Clang: Don't propose to refactor function defs. [Commit.](https://cgit.kde.org/kdevelop.git/commit/?id=11e20f915543f6f3c292177e0c0eac21e4377a97)
* Fix comments in file template for Python unit test for setUp/tearDown. [Commit.](https://cgit.kde.org/kdevelop.git/commit/?id=d86365bdf1ee6cfa11e12c45ff88b8f05ac74062) Fixes bug [#321005](https://bugs.kde.org/321005)
* Fix TestCustomBuildSystemPlugin by readding .kdev4 folders of test data. [Commit.](https://cgit.kde.org/kdevelop.git/commit/?id=f1f7dce013cef00aee052a1a02f998c39a8999fe)
* Providers: Fix some Qt runtime warnings. [Commit.](https://cgit.kde.org/kdevelop.git/commit/?id=bbfd0c554eafd69b8de7e5cca862f83baa643cda)
* Update TestProblems::testMissingInclude() to current ClangFixitAssistant. [Commit.](https://cgit.kde.org/kdevelop.git/commit/?id=27731561ef254099a9df7b36e36068322b89c135)
* QtHelp CSS images fix. [Commit.](https://cgit.kde.org/kdevelop.git/commit/?id=822fe3e3a273575040eb1cf9e7c3dccec3b77322)
* QtHelp page loading fix. [Commit.](https://cgit.kde.org/kdevelop.git/commit/?id=25af44dabb76e15ec55eb752c36c0769095eeb0c)
* Update INSTALL. [Commit.](https://cgit.kde.org/kdevelop.git/commit/?id=85db611c5f8e55e3cda561506d281da48d5f0f86)
* Streamline README. [Commit.](https://cgit.kde.org/kdevelop.git/commit/?id=78ff3b19fb0b210312ca27a05ae81e2074d86951)
* File\_templates: Remove '#include "foo.moc"' footer. [Commit.](https://cgit.kde.org/kdevelop.git/commit/?id=4b3f07cb02a9237dab0f13d2dcb7c88a58436b3e)
* Prefer qmake over qmake-qt5 over qmake-qt4. [Commit.](https://cgit.kde.org/kdevelop.git/commit/?id=85957712044d7a91d3ee4eec4012c333cd7fe70e)
* Appimage: do not fail on git stash pop. [Commit.](https://cgit.kde.org/kdevelop.git/commit/?id=af964aba19fe59106efc634782f4602e6bb28012)
* Fix default icon for some plugin. [Commit.](https://cgit.kde.org/kdevelop.git/commit/?id=d6d4ad9244b571cdd5e231e141640c7be88a57c0)
* I18n fixes. [Commit.](https://cgit.kde.org/kdevelop.git/commit/?id=a7852ca005f80476ad1f45cd088f45ae8fcdab9d)


### [kdevplatform](https://cgit.kde.org/kdevplatform.git/)


* Don't open nonexistent documents from problems view + assertion fix. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=ab0bbcba75aa14f57d7ecdd25c0c7df1c5b3f612)
* Assertion fix for ProblemModel. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=5ad012aa9e85820b8f6b57a4b9061e66dd8a3573)
* Note more explicit in API dox that nullptr can be returned. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=993b5297824aaf39879d34511b6a702e3a39f2d6)
* File templates dialog: Fix crash in overrides page with unknown base class. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=f01e4cba08e2a2bec6500df9aa24b7aad8b0ca59)
* Silence runtime warnings about missing methods. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=e200edb151f1926aae8c8d82f8338d41b6aa8176)
* Pass KTextEditor::MainWindow to text views created from TextDocuments. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=00de613bcb0fb0cff587e0b8ce090b915bc6f717)
* File templates tool: with no open document, use base dir of project or home. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=faff287f0fbfe6062d6ddeb64422a324b561f04c)
* Update version number to 5.0.4. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=97237d4e6dd22d87a86aa3521a038679aa655300)
* Cleanup: Remove unused member. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=c86a5c3f0b0d859389d14aaa6803fe453af3c2cb)
* Register EnvironmentSelectionWidget properly with KConfigDialogManager. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=6c8a0cd53be32c92528eeb5206da048472550fbb)
* Disable the Revision Graph feature. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=b3896fe5cc7eb66ddc9a987767361001ff8b600b)
* Fix crash on triggering "Reformat Source" without no more file loaded. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=3b476d83ae9a2b0a7e91727d836745d097073d30)
* Restore hiding of help buttons in assistant dialogs. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=f188ca3d58f5bba279b2d9f87192c59480940da2)
* Cleanup: Remove unused method. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=76510169d5d4de7eb4c6e835903a37b5faaefcb3)
* Quickopenfilter: Fix documentation. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=5fd4b46632a7a743a00389d7363b9ae531af6266)
* Fix checkboxes in file template custom options page being misplaced. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=416770b35f65100023bd5a854008f70147e36a63)
* Speed up test\_projectcontroller a bit. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=da8e0a8789715ac664d9a6252bf27f433c28f05a)
* Fix layout accidently being tried to replace OutputPage's main layout. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=61c8e923b38a065bfc8473510cd3e12c1ea757db)
* Fix opening of remote URLs without filename. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=28c38e3b9dd6eda0edb8cfa763db64897314eb4b) Fixes bug [#373213](https://bugs.kde.org/373213)
* TestTemplatesModel, TestGenerationTest: C locale to avoid translated strings. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=c7d2da3ebf4ce03a07a7fddf7e22cc11bc8b0dbf)
* Raise prev/next history context buttons. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=60c4cf0d4315830f4526cddca96acd48ddc3e02c)
* SourceFormatterController: Fix 'Reformat files'. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=b8062f736a2bf2eec098af531a7fda6ebcdc7cde) Fixes bug [#373842](https://bugs.kde.org/373842)
* SourceFormatterController: More debug output. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=a08939425ead7fd980111b030f8ee958ab74b0f5)
* Fix infinite loop on cloning environment group with another clone present. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=7c814217c1953fcc2bacbf6bc469a3643c2742d5)
* Revert "Restore About dialog for plugins in "Loaded Plugins" dialog". [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=dc315cfce108ff58147a08351598a934d82a8a16)
* Fix deprecation warning. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=e10707bbc2116170ea6d07108774afd4efcd654f)
* Add fallback code for KAboutData::fromPluginMetaData() for old kf5 versions. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=e565620aae24ccdb4e2a1641865e1633731c9740)
* Restore About dialog for plugins in "Loaded Plugins" dialog. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=9fe2db57aede3cf79ee521f4f9e167dfed6306bd)
* LoadedPluginsDialog: Fix memleak. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=30259896782bd080351a642be1f69a08183c59ad)
* StandardDocumentationView positioning fixes. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=64f8ca75f0230032964d3abf2e0d015555723002)
* Update INSTALL. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=a372180f280d8e6e47d645dd3d8b9c5efda7d636)
* Add bits about contributing to KDevelop. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=593db5d72e68284a0db49c99a3e31162b9bee1ff)
* Streamline README. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=e57df97b3a4007f5ca8775cbf193588783f503ff)
* Style: C++11ify. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=a1cec207cd6440fe0399bd481b5dc93d8024fad4)
* Projectfilter: Filter out VS artifacts. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=de29b3995a60f5600b012635b1673b08a04822d2)
* ProjectConfigSkeleton: Remove FIXME. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=3fc389e64b2955cd7347d6798202b2bf22ab5643)
* Fix typo in CMake message. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=0271059cb04c44014fdbd6f8c02c719029a3079f)
* Fix extraction of multi-line UI strings from welcome page. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=fd907cfa8659b3dcce1bf1d753959710fe18e389)
* Hotfix for restoring build with newer KConfig. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=e84645d1694bdad7f179cd41babce723fe07aa63)
* Fix default icon for some plugins. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=b3922529f0fde3e4814b21c9cabe237f5c20cabe)
* Hide progress bar after all jobs finished. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=ceae10c925b01536a0848b85f1cfa70b308eb85c)
* DUChainPointer::dynamicCast() - don't segfault if no target. [Commit.](https://cgit.kde.org/kdevplatform.git/commit/?id=357f492e90b1cba8a457bd7cd3b96e43a26fd2e6)


### [kdev-php](https://cgit.kde.org/kdev-php.git/)


* Update version number to 5.0.4. [Commit.](https://cgit.kde.org/kdev-php.git/commit/?id=fcb28ff65e5bcf6a235b9a0f5fdb9e03b126bdf4)


### [kdev-python](https://cgit.kde.org/kdev-python.git/)


* Update version number to 5.0.4. [Commit.](https://cgit.kde.org/kdev-python.git/commit/?id=176da875b28dcf43bf6b7394ff964d80f5cf3459)
* Fix crash when requesting raise items without the docfile being parsed. [Commit.](https://cgit.kde.org/kdev-python.git/commit/?id=1326aa448eab2b8b76e6546c4e3160c98e817502) Fixes bug [#373349](https://bugs.kde.org/373349)


The 5.0.4 source code and signatures can be downloaded from [here](http://download.kde.org/stable/kdevelop/5.0.4/src/).


