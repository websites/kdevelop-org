---
title: "KDevelop 5.0.0 release"
date: "2016-08-23 18:00:00+00:00"
lastmod: "2016-08-25 18:42:08+00:00"
author: "kfunk"
aliases:
- /news/kdevelop-500-released
tags:
- release
- kf5
categories:
- News
---
Almost two years after the release of KDevelop 4.7, we are happy to announce the immediate availability of **KDevelop 5.0**. KDevelop is an integrated development environment focusing on support of the C++, Python, PHP and JavaScript/QML programming languages. Many important changes and refactorings were done for version 5.0, ensuring that KDevelop remains maintainable and easy to extend and improve over the next years. Highlights include much improved **new C/C++ language support**, as well as polishing for **Python, PHP and QML/JS**.


This release announcement is kept short intentionally, to check out what's new in KDevelop 5.0, please read **[this blog post by Kevin](http://kfunk.org/2016/08/23/whats-new-in-kdevelop-5-0/)**.


![KDevelop 5.0 screenshot](/sites/www.kdevelop.org/files/inline-images/kdevelop5-breeze_2_0.png)
 


### C/C++ language supported now backed by Clang


The most prominent change certainly is the move away from our own, custom C++ analysis engine. Instead, C and C++ code analysis is now performed by clang. Aside from being much easier to maintain, this has a number of advantages:


* Even the most complex C++ code constructs are now parsed and highlighted correctly and reliably. In the end there's a compiler in the background -- KDevelop will complain exactly if it wouldn't compile.
* Diagnostics are a lot more accurate and reliable.  For example, KDevelop can now detect whether or not there is an overload of a function available with the parameters you are passing in.
* For many problems (e.g. misspelled variable names, missing parentheses, missing semicolon, ...), we get suggestions on how to correct the problem from clang, and offer the user a shortcut key (Alt+1) to apply the fix automatically.
* There is now a C parsing mode, which enables the analysis engine to correctly parse C code.


Work on getting all our old utilities for C++ to work nicely with the new infrastructure is still ongoing in some areas, but most of the important things are already in place. In contrast to the C++ support, the Python support has not undergone any significant refactoring, but has instead seen further stabilization and polishing. The same is true for the PHP and QML/JS language support components.


### Qt 5, KDE Frameworks 5, and other platforms


Apart from those changes, KDevelop 5 has of course been ported to KDE Frameworks 5 and Qt 5. This will for the first time enable us to offer an experimental version of KDevelop for Microsoft Windows in the near future, in addition to support for Linux.  Additionally, we offer experimental stand-alone Linux binaries, which make it much easier for you to try KDevelop 5 before upgrading your system-wide installation.


### Download


You can download the source code [**from here**](http://download.kde.org/stable/kdevelop/5.0.0/src/). The archives are signed with the following key ID: AC44AC6DB29779E6.


Along with KDevelop 5.0, we also release version 2.0 of the kdevelop-pg-qt parser generator utility; download it [from here](http://download.kde.org/stable/kdevelop-pg-qt/2.0.0/).


We also provide an experimental pre-built binary package which should run on any moderately recent linux distribution: [Download AppImage binary for Linux (any distribution)](http://download.kde.org/stable/kdevelop/5.0.0/bin/linux/). After downloading the file, just make it executable and run it.


*Update: We updated the AppImage (the new version is 5.0.0-1) and fixed a few issues with the packaging, esp. file and project templates not working. It also comes with kdev-php and the console toolview now.*


Thanks to everyone involved in preparing the release!


