---
title: "KDevelop 5.5.2 released"
date: "2020-06-02 13:28:58+00:00"
lastmod: "2020-06-02 13:46:50+00:00"
author: "kossebau"
aliases:
- /news/kdevelop-552-released
tags:
- release
categories:
- News
---
We today provide a bug fix and localization update release with version 5.5.2. This release introduces no new features and as such is a safe and recommended update for everyone currently using a previous version of KDevelop 5.5.


You can find the updated Linux AppImage as well as the source code archives on our [download](/download) page.


Should you have any remarks or in case you find any issues in KDevelop 5.5, please let us [know](https://www.kdevelop.org/support).


## ChangeLog


### [kdevelop](https://commits.kde.org/kdevelop)


* Remove plugin "kde repo provider" due to defunct service. ([commit](https://commits.kde.org/kdevelop/a36a50baf050850fa0e513ceae2e99be7749c27c))
* Fix extra margins around config pages. ([commit](https://commits.kde.org/kdevelop/97befad9e55cdf1337ed7c1afeae4cfee73b02f6))


### [kdev-python](https://commits.kde.org/kdev-python)


No user-relevant changes.


### [kdev-php](https://commits.kde.org/kdev-php)


* Check type before accessing it. ([commit](https://commits.kde.org/kdev-php/5753d80c89217bda3d4865a8d4ca6fa29a7b6ee3). fixes bug [#421509](https://bugs.kde.org/421509))


