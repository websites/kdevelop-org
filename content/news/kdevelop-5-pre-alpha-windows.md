---
title: "KDevelop 5 Pre-Alpha on Windows"
date: "2014-09-13 17:54:50+00:00"
lastmod: "2016-11-17 13:16:39+00:00"
author: "milian"
aliases:
- /screenshots/kdevelop-5-pre-alpha-windows
tags:
- KDevelop 5
---
A preliminary screenshot of a KDevelop 5 session based on Qt 5 and KDE Frameworks 5 running on a Windows host.

Download here: https://www.kdevelop.org/download