---
title: "KDevelop 4.7.2 Released"
date: "2015-10-12 09:33:04+00:00"
lastmod: "2016-02-28 21:10:54+00:00"
author: "milian"
aliases:
- /kde4/kdevelop-472-released
tags:
- release
- stable
- kdevelop
- kde4
categories:
- News
---
Hey all,

I'm happy to announce the availability of KDevelop 4.7.2. This is a bug fix 
release increasing the stability of our KDE 4 based branch. Please update to 
this version if you are currently using 4.7.1 or older.

Download from:
http://download.kde.org/stable/kdevelop/4.7.2/src/

### SHA sums:



SHA256Sum: 5801a38a9abfebead18b74c0a0f5a6d68118b297c96ef1e2f75c8bfae3463b19 
kdevelop-4.7.2.tar.xz
SHA256Sum: 2dff2f54f631296c87007be84a0dc439d328d473717577c0d1450a9fc7e6e828 
kdevelop-php-docs-1.7.2.tar.xz
SHA256Sum: 75de9a5afe32ecaa35eb14e1ae04dd9c6d3e9abf87274ca4537fbdc3b296a369 
kdevelop-php-1.7.2.tar.xz
SHA256Sum: 0afcde7a746adb83b00f2bd6b02efb4022adbd61b6ba4325fb63b71902f1f4e4 
kdevplatform-1.7.2.tar.xz
SHA256Sum: 58dfaaae929da77b0c9656fdfad1f8a509059feffdb71c943a721f40034dd9bf 
kdev-python-1.7.2-py3.tar.xz
SHA256Sum: 949254984bd489cd5597d4c4986057c43b60febde329f6f9d1b42228aef9444d 
kdev-python-1.7.2.tar.xz

### Changes in this release:


#### ChangeLog for kdevplatform v1.7.2



```

* Felix Mauch: Added  and  as placeholders in license templates.
\* Zhang HuiJie: ensure topData->m\_importers/topData->m\_importedContexts is not 
nullptr, this fix crash while finding usage of var/function/class.
\* Alexander Potashev: filetemplates: Automatically create directory for 
license templates
\* Kevin Funk: Make compile (cf. BOOST\_JOIN)
\* Kevin Funk: patchreview: Fix invalid read
\* Alex Merry: Make subversion plugin compile with subversion 1.9.
\* Andreas Pakulat: Do not report subversion as being not found when it 
actually was
\* Aleix Pol: Manners++

```

#### ChangeLog for kdevelop v4.7.2



```

* Zhang HuiJie: oldcpp::CppDUContext: use max instantiate depth to avoid 
circular loop.
* Jingqiang Zhang: Fix one more use of function return refer to temp variable.
* Zhang HuiJie: oldcpp: check if the macro is defined in the current file 
after the current line.
* Zhang HuiJie: oldcpp: CodeCompletionContext::addOverridableItems(): limite 
max search depth to 5000
* Milian Wolff: Unbreak build - sorry for that.
* Fernando Rodriguez: Fix interrupt of debuggee in remote debugging sessions.
* Zhang HuiJie: Set maximum number of recursive calls for getMemberFunctions 
to 50.
* Fernando Rodriguez: Set the replyReceived flag only if the current command 
is exec-run.
* Sébastien Lambert: Do not use function returning reference to temporary.
* Zhang HuiJie: rpp: test for mispaired brackets
* Zhang HuiJie: Handle defined in preprocessor.
* Zhang HuiJie: Handle macro aliases of function like macros in navigation 
popups.
* Zhang HuiJie: Add __restrict__ to be more compatible with g++

```

#### ChangeLog for kdev-python v1.7.2-py3



```

* Sven Brauch: remove raw linker flag
* Sven Brauch: change version back to 1.7.1 temporarily to test CI
* Sven Brauch: debugger: do not crash when launching current document and 
there is none
* Sven Brauch: debugger: correctly launch program with "launch current file" 
option
* Sven Brauch: backport: fix crash: use a locker on the project paths
* Sven Brauch: add missing quotes in cmake
* Sven Brauch: require correct version of python
* Sven Brauch: properly handle multi-line attributes
* Sven Brauch: New method to find the correct attribute ranges
* Sven Brauch: somewhat working version, but still failing tests
* Sven Brauch: fix a few more corner cases of what was broken by python 3.4.3
* Sven Brauch: Port to Python 3.4.3. Work in progress, not everything is ok 
yet.
* René J.V. Bertin: Improved Python macros for CMake
* René J.V. Bertin: use kDebug() instead of qDebug()
* Sven Brauch: hard requirement on python 3.4.3 for this version
* Sven Brauch: Work around new behaviour in Python 3.4.3.
* Sven Brauch: find project search paths in parsejob, not in a background 
thread
* Vladimir Berezenko: Fix for introspection script to enable possibility of 
introspecting in-project packages

```

#### ChangeLog for kdev-python v1.7.2



```

* Vladimir Berezenko: Fix for introspection script to enable possibility of 
introspecting in-project packages

```


Many thanks to everyone involved! Stay tuned for the upcoming first beta 
release of KDevelop 5.