---
title: "KDevelop 5.0.1 released"
date: "2016-09-19 15:30:00+00:00"
lastmod: "2016-10-21 09:31:09+00:00"
author: "sbrauch"
aliases:
- /news/kdevelop-501-released
tags:
- kdevelop
- KDevelop 5
- windows
- release
categories:
- News
---
One month after the release of KDevelop 5.0.0, we are happy to release KDevelop 5.0.1 today, fixing a list of issues discovered with 5.0.0. The list of changes below is not exhaustive, but just mentions the most important improvements; for a detailed list, please see our git history.


An update to version 5.0.1 is highly recommended for everyone using 5.0.0.


### Issues fixed in 5.0.1


* Fix a deadlock in the background parser, which especially occured on projects containing both C++ and Python/JS/QML code and caused either parsing or the whole application to freeze randomly. [BR: 355100]
* Do not display the "project is already open in a different session" dialog on starting up a session under some circumstances.
* Fix a crash which sometimes happened when switching git branches on command line.
* Fix a crash when starting debugger from command-line. [BR: 367837]
* Mouseover highlight now uses the "Search highlight" color from the configuration dialog, instead of a hard-coded bright yellow. [BR: 368458]
* Fix a crash in the PHP plugin when editing text in the line after a "TODO". [BR: 368257]
* Fix working directory of Custom Makefile plugin [BR: 239004]
* Fix a possible crash on triggering an assistant popup action [BR: 368270]
* Fix a freeze under some circumstances when the welcome page is displayed. [BR: 368138]
* Fix some translation issues.
* Fix imports sometimes not being found in kdev-python without pressing F5 by hand [BR: 368556]


### Issues fixed in the Linux AppImage


* Ship the subversion plugin.
* Fix QtHelp not working.
* Ship various X11 libraries, which reportedly makes the binary run on relatively old systems now (SLES 11 and similar)
* Disable the welcome page for now.


### Download


The source code for 5.0.1 is available here: <http://download.kde.org/stable/kdevelop/5.0.1/src/>  

*Source archives are signed with the GPG key of Sven Brauch, key fingerprint 4A62 9799 32BB BCE5 E395 6ACF 68CA 8E38 C4BB 3F4B.*


The AppImage pre-built binaries for Linux can be downloaded from here: <http://download.kde.org/stable/kdevelop/5.0.1/bin/linux/>


