---
title: "KDevelop 5 Pre-Alpha with Breeze Theme"
date: "2014-09-13 17:50:15+00:00"
lastmod: "2016-02-28 21:10:54+00:00"
author: "milian"
aliases:
- /screenshots/kdevelop-5-pre-alpha-breeze-theme
tags:
- KDevelop 5
---
A first teaser of KDevelop 5 based on Qt 5 and KDE Frameworks 5 running on Linux with the new KDE Breeze theme.