---
title: "KDevelop 5.0 Beta 2 Release"
date: "2016-01-31 21:30:08+00:00"
lastmod: "2016-02-28 21:10:54+00:00"
author: "milian"
aliases:
- /news/kdevelop-50-beta2-release
tags:
- release
- beta
- unstable
- KDevelop 5
- kf5
- qt5
- clang
- gci
categories:
- News
---
Three months after the [first KDevelop 5.0 Beta release](/news/first-beta-release-kdevelop-500-available), I have the pleasure to announce our second beta release! We have worked hard on improving the stability and performance of our new KDevelop 5.0 based on Qt 5 and KDE Frameworks 5. We also continued to port many features from our old C++ language support to the new Clang-based C/C++ plugin, which is still an ongoing effort.

Thanks to Google Code-In, we managed to fix a lot of small issues throughout our code base. Google Code-In was a huge success for us and we were surprised by the number of students we have had working on KDevelop in that short period of time. In total we had around 30 tasks, almost all of them have been completed successfully. Lots of thanks go to Artur Puzio, Imran Tatriev, Sergey Popov, Mikhail Ivchenko and Andrey Cygankov (I hope I didn't forget anyone), who all did a tremendous job during their GCI. Thanks guys!

The Windows and OS X version of KDevelop is shaping up as well. Various issues for cross-platform support got fixed, such as wrong or missing icons, support for different line endings in various plugins and views, or removing DBUS as a hard dependency.

The following is an incomplete list of notably new features in this beta:

* The Python support for scripts using numpy arrays in the debugger has been enhanced.
* Quite a few left-over porting bugs were fixed throughout the codebase.
* A KDE4 config migration has been added.
* The frame stack view now highlights the crashed thread.
* Our Includes & Defines and Compilers widgets got a UI cleanup.
* The job to build with Make is now reporting its progress in the status bar, just like the Ninja job.
* The Okteta plugin got revived.
* The Grep View and Uses Widget now save vertical space.
* On Windows, there is now better support for the various make flavours (nmake, mingw32-make, ...).
* Also on Windows, we now parse MSVC warnings / errors from compiler output and highlight them properly.
* On OS X, we enabled Hi DPI support.
* Also on OS X, we don't start KIO slaves via dbus anymore, but just fork them off instead.



You can download the sources for KDevelop 5.0 Beta 2 from the KDE mirrors: <http://download.kde.org/unstable/kdevelop/4.90.91/src/>

Alternatively, you will be able to test this new release by using experimental packages provided by your distribution.

Note that KDevelop 5.0 Beta 2 is still an experimental release. We welcome more testers and early feedback, and many of us developers use KDevelop 5 already in production for their day jobs. That said, we are also aware of quite a few [blocking issues](https://bugs.kde.org/buglist.cgi?keywords=release_blocker&keywords_type=allwords&product=kdev-python&product=kdevelop&product=kdevplatform&query_format=advanced&target_milestone=5.0.0). These will be fixed in the next weeks, at which point we can hopefully create our first release candidate for KDevelop 5.0!

Below is the full list of the many commits that make up this new release. A huge "Thank you!" goes out to everyone who contributed to this release, both in form of patches as well as giving feedback, e.g. in the form of bug reports on <http://bugs.kde.org>.


### ChangeLog for kdevplatform v4.90.91


* Milian Wolff: Set version to 4.90.91 for 5.0.0 Beta 2
* Milian Wolff: Set version to 1.7.3
* Sergey Popov: Fix issue with low-res image in Quick Open menu
* Kevin Funk: Silence some Clang warnings
* Kevin Funk: TopContextDynamicData: Fix bug in loadPartialData
* Kevin Funk: TopContextDynamicData: Fix bug in loadPartialData
* Artur Puzio: MainWindow: Add indicator for read-only files
* Kevin Funk: Prefer static version of QFileInfo::exists
* Milian Wolff: Ensure the languages of possible resolutions match.
* Pino Toscano: i18n: typo fixes
* Milian Wolff: Always force recursive list jobs.
* Kevin Funk: OutputExecuteJob: Handle progress properly
* Kevin Funk: Make compile with Qt 5.4
* Sergey Popov: Fix almost all the remaining Clazy warnings I fixed almost all the remaining warnings in KDevPlatform but there are some warnings, which are still not fixed. "Missing Q\_OBJECT macro" in some files, generated while building, for example. REVIEW:126782
* Pino Toscano: cmake: fix macro\_log\_feature for subversion
* Sven Brauch: correctly load completion list icons from our own data directory
* Kevin Funk: OutputJob::setModel: Fix memleak
* Kevin Funk: ParseJob: Remove unused member
* Kevin Funk: Fix memleaks, detected by ASAN
* René J.V. Bertin: preserve existing windowIcons via QIcon::fromTheme's fallback
* Kevin Funk: Fix memleak, reported by ASAN
* Kevin Funk: Fix crash in TemplatePreviewToolView
* Kevin Funk: OutlineModel: Potential fix for crashes-on-exit
* Gleb Popov: Fix kdevfiletemplates previews on Windows
* Imran Tatriev: Remember Filesystem ToolView's location in each session
* Kevin Funk: Use ascending order by default in variables list
* Kevin Funk: FrameStackModel: Color non-existing files in gray
* Gleb Popov: Fix template addition dialog from kdevfiletemplate plugin.
* Kevin Funk: Minor: Remove excessive debug output
* Kevin Funk: Variables view: Improve coloring
* Kevin Funk: FrameStackModel: Add "crashed" to crashed thread
* Aleix Pol: Fix some Clazy warnings in KDevPlatform
* Aleix Pol: Simplify url lookup in the ProjectChangesModel
* Aleix Pol: Prevent reloading the project changes twice
* Kevin Funk: Fix i18n call
* Kevin Funk: Minor: Code reformatting
* Kevin Funk: Pimpl IDebugSession
* Kevin Funk: Pimpl IFrameStackModel
* Kevin Funk: Pimpl FrameStackModel
* Sergey Popov: Improve the problems tool view CCBUG: 339839
* Imran Tatriev: Highlight crashed threads
* Milian Wolff: Use non-recursive mutex to speed up the indexed string operations.
* Milian Wolff: Cleanup IndexedString implementation and remove double locking.
* Milian Wolff: Disable copying of RepositoryManager.
* Milian Wolff: Share refcount ref/deref code in IndexedString.
* Milian Wolff: Use uint as short form for unsigned int, share code for char <-> index.
* Milian Wolff: Further cleanup the code and reuse c\_strFromItem code.
* Milian Wolff: Fix IndexedString::c\_str() for single-char items.
* Milian Wolff: Reformat sources and assimilate license header.
* Kevin Funk: Cleanup header/library dependencies slightly
* Aleix Pol: Prevent crash if ::effectiveCommandLine is empty
* Kevin Funk: Fix potential crash in OutputExecuteJob::start
* Gleb Popov: Partially revert 2dff0f24356aa615c1f3b725ce183fabaf916e6d
* Mikhail Ivchenko: Setup QCollator into ctor instead of lessThan. Also, remove unneeded includes.
* Kevin Funk: Support of $VARIABLE in environment settings
* Kevin Funk: NativeAppJob: Base on OutputExecuteJob
* Mikhail Ivchenko: [GCI] Use natural sorting in variable list
* Kevin Funk: Minor: Q\_NULLPTR -> nullptr
* Kevin Funk: Fix bug wrt QMessageBox::StandardButton
* Kevin Funk: OutputExecuteJob: Use "Finished" again
* Kevin Funk: OutputExecuteJob: Improve strings
* Mikhail Ivchenko: [GCI] Show warning in launch configuration if no execute plugins were enabled.
* Mikhail Ivchenko: Remove related actions while removing LaunchConfigurationType.
* Kevin Funk: Give the Batch Edit dlg a reasonable size
* Kevin Funk: Abort in case no plugins were found
* Kevin Funk: UsesWidget: Save vertical space
* Kevin Funk: Warn about missing/bogus QT\_PLUGIN\_PATH
* Kevin Funk: GrepView: Save some vertical space
* Milian Wolff: Remove unused file.
* Milian Wolff: Get rid of lots of temporary allocations in KDevelop::formatComment.
* Kevin Funk: OutputModel: Allow to set IFilterStrategy pointer
* Kevin Funk: Introduce IFilterStrategy::progressInLine
* Kevin Funk: OutputFilteringStrategies: Pimpl (again)
* Milian Wolff: Leverage QScopedValueRollback for PushValue.
* Milian Wolff: Revert "PushValue: Base on QScopedValueRollback"
* Kevin Funk: OutputView: Remove output rewriting feature
* Kevin Funk: Extent project interfaces, cleanup
* Mikhail Ivchenko: [GCI] Add "copy filename" action to context menu in tabs.
* Mikhail Ivchenko: EnvironmentWidget: Add support for cloning envs
* Sergey Popov: Add feature to edit existing environment group
* Imran Tatriev: Parse MSVC compile errors Fixes T602
* Andreas Cord-Landwehr: Fix default fallback for Sublime::View.
* Kevin Funk: PushValue: Base on QScopedValueRollback
* Sergey Popov: Fix most Clazy warnings in shell/ and sublime
* Kevin Funk: OutputView: Remove now redundant filter expr
* Kevin Funk: OutputView: Cleanup native app filters
* Kevin Funk: OutputView: Parse QML import runtime errors
* Kevin Funk: Respect KDE HIG
* Kevin Funk: [GCI] Parse runtime output of assert() message, make it clickable
* Kevin Funk: WelcomePage: Register QAction properly
* Kevin Funk: KDevVarLengthArray: Deduplicate implementation
* René J.V. Bertin: Create m\_finishReview as a KAction instead of as a QAction, in order to prevent error messages.
* Kevin Funk: Perf: Fix other minor issues
* Kevin Funk: Perf: Fix accidental detaches in ItemRepository
* Kevin Funk: Revert "IndexedString::toUrl: Fix too-strict assert"
* Kevin Funk: IndexedString::toUrl: Fix too-strict assert
* Kevin Funk: DUChain: Remove duplicates inside getInheriters
* Kevin Funk: Minor: Strip KDevelop prefix
* Kevin Funk: Navigation: Handle anonymous decls in popup
* Kevin Funk: CMake: Remove text from else/endif
* Kevin Funk: OutputModel: Reduce complexity re. pre-filtering
* Kevin Funk: OutputView: Filter ANSI sequences
* Kevin Funk: Fix MSVC warnings
* Kevin Funk: clang-tidy: modernize-use-override
* Kevin Funk: Modernize CMake, auto-generate config file
* Andrew McCann: Make kdevduchain use QStandardPaths for Cache location
* Kevin Funk: DBus: Fix SessionController publishing
* Kevin Funk: Expose RunController on DBus
* Kevin Funk: Cleanup unused CMake code
* Andrew McCann: Make status 'popup' button more appopriate size on OSX
* Andrew McCann: OSX: Make StatusBar transparent to clear up visual oddity
* Milian Wolff: Workaround crash when expanding project folder.
* Andrew McCann: Be explicit about special menu item roles, required to prevent menu items from missing on OSX
* Milian Wolff: Guard against reentrancy issues that may trigger a crash.
* Milian Wolff: Cleanup: trim trailing spaces.
* Kevin Funk: Open File: Use KEncodingFileDialog only under KDE
* Milian Wolff: Add new DUContext::resortLocalDeclarations and resortChildContexts API
* Milian Wolff: Add hover animation for session names on welcome page.
* Milian Wolff: Make function a slot such that its invokation can be delayed.
* Milian Wolff: Cleanup code.
* Kevin Funk: documentswitcher: Fix document paths
* Kevin Funk: Fix behavior of main window caption
* Kevin Funk: Dont hide/restore docks on double-click with RMB
* Kevin Funk: Dont hide/restore docks on double-click with RMB
* Kevin Funk: ProjectSourcePage: Use KMessageWidget
* Kevin Funk: ProjectSelectionPage: Use KMessageWidget
* Milian Wolff: Allow to open files/folders with unassociated applications.
* Kevin Funk: Fix CMake compilation output parsing
* Kevin Funk: Enable 'Apply' button when needed
* René J.V. Bertin: more sensible workaround for code review crash on OS X
* Alex Richardson: Fix build without Grantlee
* Kevin Funk: Make compile
* Andrew McCann: fix crash on navigation crash, when clicking on '\_\_1' container of clang's libc++ std::\_\_1::string
* Aleix Pol: Restore the dashboard as we used to have in kdevelop 4
* Milian Wolff: Cleanup code.
* Milian Wolff: Use QLatin1String in comparison to avoid temporary allocations.
* Milian Wolff: Minor code cleanup.
* Milian Wolff: Implement filesForPath for TestProject.
* Milian Wolff: Fix compiler warning about struct/class mismatch.
* Milian Wolff: Sort problems by severity in the problem tool view.
* Milian Wolff: Cleanup code.
* Milian Wolff: Use resizeColumnToContents code instead of reinventing the wheel.
* Milian Wolff: Skip void return types in "document function" helper.
* Milian Wolff: Fix compile warning
* Milian Wolff: Add signals for when a code completion model (un)registered itself.
* Milian Wolff: Remove dead code.
* Milian Wolff: Remove dead code.
* Aleix Pol: Remove recovery
* Andrew McCann: Don't segfault when dbus is not running
* Maciej Cencora: Fix detailed progress dialog visual glitches
* Milian Wolff: Cleanup code.
* Milian Wolff: Clear problems when document gets reloaded.
* Milian Wolff: Minor code cleanup.
* Milian Wolff: Properly update the assistant popup contents when the actions changed.
* Milian Wolff: Minor code cleanup, and remove obsolete code.
* rishabh gupta: Sort variables alphabetically in debugger variables view.
* Maciej Cencora: Fix Problem tooltip flickering
* Andrew McCann: boost include dirs
* Sven Brauch: debugger: produce a new tooltip if the expression changes
* Milian Wolff: Update locations of currently opened files when renaming a folder.
* Milian Wolff: Revert "Add assistants to the "argument hints" code completion"
* Milian Wolff: Revert "Remove assistant popup"
* Milian Wolff: Revert "Move assistant completion to its own KTextEditor::CompletionModel"
* Milian Wolff: Use the ServiceType to detect whether a KTextEditor plugin is KDevelop compatible.
* Milian Wolff: Fix compile with older KTextEditor
* Andrew McCann: Address style issues, Comment fix better
* Andrew McCann: Fix HiDPI issue with project tree view
* Kevin Funk: Update copyright year
* Kevin Funk: JSON tests: Expose 'isMutable'
* Tobias Berner: Fix build on FreeBSD: std::abs needs cstdlib
* Christoph Feck: Remove QtQuick1 dependency
* Aleix Pol: No need to close documents before changing the area
* Aleix Pol: Don't close documents when saving


### ChangeLog for kdevelop v4.90.91


* Milian Wolff: Set version to 4.90.91 for 5.0.0 Beta 2
* Milian Wolff: Make compile, sorry...!
* Milian Wolff: Don't access declaration without holding the DUChain lock.
* Kevin Funk: Silence some Clang warnings
* Kevin Funk: Prefer static version of QFileInfo::exists
* Kevin Funk: Fix MSVC build
* Milian Wolff: Hide declarations from other languages in UnknownDeclarationProblem.
* Kevin Funk: Make compile
* René J.V. Bertin: preserve existing windowIcons via QIcon::fromTheme's fallback
* Kevin Funk: CMake: Fix memleak, detected by ASAN
* Kevin Funk: Minor: Prefer QString::toHtmlEscaped()
* Kevin Funk: Minor: Fix accidental detach
* Kevin Funk: GDB: Merge console stream into Debug View
* Kevin Funk: Minor: Constify parameters
* Kevin Funk: Minor: Fix variable naming
* Kevin Funk: DebugJob: Use native app error filter
* Imran Tatriev: Provide support for highlighting crashed threads
* Kevin Funk: Bump required Okteta version to 0.3.1
* Artur Puzio: Brought Okteta plugin back to life
* Milian Wolff: Filter out declarations that don't belong to the clang plugin.
* Kevin Funk: Clang: Fix crash in findDeclaration
* Imran Tatriev: Provide support for highlighting crashed threads
* Kevin Funk: Cleanup header/library dependencies slightly
* Zhang HuiJie: Make compile.
* Kevin Funk: Raise required KF5 version slightly
* Andrew McCann: Make kdevelop-app and kdev-plugins use Qt5 resources for splash, rc, knsrc
* Kevin Funk: Add hint for KDEV\_CLANG\_DISPLAY\_DIAGS
* Kevin Funk: test\_gdb: Don't crash on CI
* Gleb Popov: Add KDevelop icon for Windows OS and attach it to build.
* Kevin Funk: CMake: Clang: Tell the user to get 'libclang'
* Kevin Funk: CMake: Clang: Be more strict wrt libclang lookup
* Milian Wolff: Add basic benchmark for clang-based code completion.
* Kevin Funk: Improve look of ParserWidget
* Kevin Funk: Improve look of CompilersWidget
* Kevin Funk: Make compile
* Sergey Kalinichev: Also change label when switching between tabs
* Sergey Kalinichev: Make sure that (project root) is the first path
* Sergey Kalinichev: A couple of fixes to the language profile selector
* Sergey Kalinichev: Move compilers tab to global settings.
* Kevin Funk: Clang: Check for errors in test files
* Kevin Funk: MakeJob: Take advantage of custom filter strategy
* Kevin Funk: NinjaJob: Take advantage of custom filter strategy
* Kevin Funk: Adapt to kdevplatform changes
* Kevin Funk: Fix off-by-one in ClangUtils::getRawContents
* Gleb Popov: Fix running test on Windows. KDevelop::Path seems to expect an absolute path.
* Sergey Kalinichev: Remove obsolete code
* Sergey Kalinichev: Make TestDUChain::testGccCompatibility pass for me
* Kevin Funk: Clang: Print full argv for debugging purposes
* Kevin Funk: Clang: Add test case for pure C file
* Kevin Funk: Clang: test\_files: Also parse C files
* Kevin Funk: Clang: test\_files: Print out problems
* Kevin Funk: Set Clang\_FOUND when just libclang found
* Gleb Popov: Use QLatin1String() on string literals.
* Gleb Popov: CMake: Fix target enumerating on Windows by using QTextStream for reading TargetDirectories.txt. Move code into cmakeutils.cpp. Add a testcase.
* Gleb Popov: Filter out internal CMake targets that appear on Windows.
* Kevin Funk: Fix another c&p mistake
* Kevin Funk: Perf: Allow to inline ClangUtil::isFileEqual
* Kevin Funk: Introduce KDEV\_CLANG\_EXTRA\_ARGUMENTS
* Kevin Funk: CMake: Modernize Clang/LLVM find scripts
* Gleb Popov: Fix condition.
* Gleb Popov: Use QString::compare() with Qt::CaseInsensitive instead of toLower() and ==.
* Gleb Popov: Fix default CMake generator setting.
* Gleb Popov: Don't append -k and -j flags to command line when we are using nmake. Select default make tool based on which platform we are, not the compiler we using. On windows first try "make" (extremely rare), then "mingw32-make" and then "nmake".
* Gleb Popov: Fix typo in method name.
* Kevin Funk: CMake: Remove text from else/endif
* Kevin Funk: clang-tidy: modernize-use-override
* Kevin Funk: CMake: Export KDev::DefinesAndIncludesManager
* Kevin Funk: Modernize CMake code
* Kevin Funk: Make compile
* Kevin Funk: Make: Saner job number defaults
* Kevin Funk: Minor: Fix MSVC warnings
* Kevin Funk: QMake: Generalize inc path handling slightly
* Kevin Funk: QMake: Find include for 'core\_private' module
* Hannah von Reth: Add Clang lib clangRewriteFrontend to FindClang.cmake Add LLVM\_LIBS which represents all LLVM libs to FindLLVM.cmake
* Gleb Popov: Fix rare cases when rangeForIncludeSpec() wasn't working correctly. Move it to ClangUtils:: namespace. Add a testcase. Based on
* Milian Wolff: Minor code cleanup: Replace class with namespace.
* Milian Wolff: Make Clang's JSON tests run properly on Windows.
* Milian Wolff: Remove dead code.
* Andrew McCann: OSX: Kdelibs4ConfigMigrator needs to happen after App object is created.
* Milian Wolff: Use raw string for nicer formatting.
* Milian Wolff: Keep default arguments when adapting signature from definition side.
* Kevin Funk: QMakeManager: Streamline for use on Windows
* Kevin Funk: Minor: Sync .ui file/class name with C++ class
* Kevin Funk: Make sure CMake settings are persisted
* Kevin Funk: QMake: Fix configure, also configure when needed
* Milian Wolff: Keep the order of local declarations and child contexts on update.
* Milian Wolff: Remove dead code.
* Milian Wolff: Keep static modifier of class functions if we adapt the signature.
* Pär-Ola Nilsson: Fix size of splash screen on high-dpi configurations.
* Kevin Funk: clang-minimal-visitor: Fix Windows build
* Kevin Funk: FindClang.cmake: Introduce CLANG\_LIBCLANG\_LIB
* Kevin Funk: MSVC compile fix
* Kevin Funk: Revert "Enable legacy cpp support for Windows"
* Kevin Funk: Minor: QL1S -> QStringLiteral
* Kevin Funk: Implement KDE4 config migration
* Kevin Funk: Add missing QMake/Qt4 app template
* Kevin Funk: Fix potential SEGV (as seen on CI)
* Milian Wolff: Fixup scope generation for nested classes in implementation helper.
* Milian Wolff: Abort on failure, instead of crashing.
* Milian Wolff: Except failure for now.
* Milian Wolff: Share code instead of duplicating it.
* Kevin Funk: QMakeJob: Instrument OutputExecuteJob properly
* Kevin Funk: test\_duchain: Fix crash on the CI
* Kevin Funk: Revert "Port to KDevelop::Stack"
* Kevin Funk: QmlJS: Fix crash when parsing QRC import
* Kevin Funk: Port to KDevelop::Stack
* Gustaw Smolarczyk: Add unit test for DUChainUtils::getInheriters.
* Sergey Kalinichev: Move some tests to test\_duchain
* Sergey Kalinichev: Adjust to Clang regarding TypeAliasTemplate visitation
* Sergey Kalinichev: Use newly added AutoType
* Sergey Kalinichev: Make sure parserArguments is never empty
* Milian Wolff: Make it possible to save the custom environment for make/ninja.
* Milian Wolff: Cleanup code
* Iain Nicol: Port "Attach to process" to KF5
* Milian Wolff: Improve GCC compatibility by adding stubs for some builtins.
* Milian Wolff: Disable Kate's built-in keyword completion for Clang-supported files.
* Milian Wolff: Fix crash when invoking code completion inside complex template class.
* Milian Wolff: fix indent
* Milian Wolff: Don't offer to implement functions related to Qt MOC.
* Milian Wolff: Associate a declaration with ImplementsItem.
* Milian Wolff: Fallback to non-canonicalized cursor for declaration lookup in use builder.
* Milian Wolff: Use CXCursor directly as QHash key.
* Milian Wolff: Demote problems about unused code to hints.
* Milian Wolff: Cleanup code.
* Milian Wolff: Fix compile
* Milian Wolff: Don't add () when completing a function that is followed by a ( already.
* Milian Wolff: Extend test to ensure adding multiple chars up front works as expected.
* Sven Brauch: splash: set correct window flag; fix outdated HTML formatting version string
* Sergey Kalinichev: Make kdevelop session applet work
* Olivier JG: Only add the Incomplete flag for Unknown TU environments
* Kevin Funk: Minor: Fix deprecation warning
* Milian Wolff: Register KTextEditor::Range for compatibility with older KTExtEditor.
* Milian Wolff: Mark function as override, to fix clang warning.
* Milian Wolff: Fix compile with older KTextEditor.
* Milian Wolff: Don't crash later when we try to parse a non-existing file.
* Milian Wolff: Don't report type mismatch warning if any type is mixed.
* Milian Wolff: make string data static, don't use QList.
* Milian Wolff: Only expect failure for clang 3.7, seems to work fine in 3.6 on the CI
* Milian Wolff: Cleanup code
* Milian Wolff: Require KDevelop-PG-Qt 1.90.90 or higher
* Andrew McCann: make qmljs kdevelop plugin compile on OSX
* Andrew McCann: Make OSX work like Windows with respect to KIO slave launching
* Andrew McCann: Do not add empty paths to include arguments for clang parser
* Andrew McCann: Enable HiDPI on OSX for KDevelop
* Kevin Funk: Update copyright year
* Milian Wolff: Restructure about menu, list active members first, add Kevin and Olivier.
* Kevin Funk: Clang: Cleanup CMake code a bit
* Kevin Funk: Clang: Also detect executables like llvm-config35
* Kevin Funk: Respect new clang\_CXXField\_isMutable
* Kevin Funk: Speed up test\_files test
* Kevin Funk: Make sure templates.cpp is tested again
* Alexandre Courbot: Language Support: add gnu99 and gnu11 standards options


### ChangeLog for kdev-php v4.90.91


* Milian Wolff: Set version to 4.90.91 for 5.0.0 Beta 2
* Milian Wolff: Fix some serious issues with the todo extractor.
* Milian Wolff: Make compile: adapt to KDevplatform API changes.
* Kevin Funk: Get rid off kdelibs4support
* Milian Wolff: Make sure we don't access a nullptr.
* Milian Wolff: Report problems for comments containing a TODO marker keyword.


### ChangeLog for kdev-python v4.90.91


* Milian Wolff: Set version to 4.90.91 for 5.0.0 Beta 2
* Pino Toscano: cmake: use CMAKE\_LIBRARY\_ARCHITECTURE
* Alex Richardson: Fix build on openSUSE
* Sven Brauch: Fix path in docfile KCM
* Sven Brauch: fix cmake macro name of python executable
* René J.V. Bertin: Improved Python macros for CMake
* Sven Brauch: fix build error with gcc 5.2 (?)
* Kevin Funk: Minor: CMake cleanup
* Kevin Funk: CMake: Also check KDevelop version
* Kevin Funk: Adapt to KDevplatform/KDevelop changes
* Kevin Funk: Include qstack.h where used
* Sven Brauch: print numpy arrays as "np.array, shape=..." in debugger instead of using str()
* Sven Brauch: debugger: better support for numpy arrays
* Sven Brauch: fix another reference-to-temporary issue found by asan
* Sven Brauch: fix use-after-free caused by accessing temporary
* Sven Brauch: remove KLocale #include in two files
* Sven Brauch: visit function return annotation as well
* Sven Brauch: remove #include requiring an (unused) KDE4 header
* Sven Brauch: matplotlib: correctly document return value of plt.subplots()
* Sven Brauch: use generate\_export\_header for debugger plugin as well
