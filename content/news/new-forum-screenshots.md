---
title: "New Forum & Screenshots!"
date: "2012-02-27 15:46:27+00:00"
lastmod: "2016-02-28 21:10:54+00:00"
author: "milian"
aliases:
- /community/new-forum-screenshots
tags:
- screenshots
- forum
- community
categories:
- News
---
Hello everyone!

We finally have a new [KDevelop forum](http://forum.kde.org/viewforum.php?f=218)! Thanks to the kind people from [forum.kde.org](http://forum.kde.org) for setting it up for us.

I thought it would be a good idea to get the forum started with a [call for screenshots](http://forum.kde.org/viewtopic.php?f=218&t=99510), which will then be shown [here on the website](/screenshots). Either post the screenshot in the forum or post a link in the comments here. Please use a creative-commons license and write a short note on your screenshot!

See the forum thread for more details: http://forum.kde.org/viewtopic.php?f=218&t=99510

