---
title: "KDevelop 5.1 Beta 1 released"
date: "2017-01-03 10:00:00+00:00"
lastmod: "2017-03-18 01:45:02+00:00"
author: "kfunk"
aliases:
- /news/kdevelop-51-beta1-released
tags:
- release
- beta
- unstable
- kf5
- qt5
- clang
- windows
- mac
- 5.1
categories:
- News
---
We are happy to announce the release of KDevelop 5.1 Beta! Tons of new stuff entered KDevelop 5.1, a bigger blog post show-casing all the features in 5.1 will follow when we release the final version. Here's a brief summary of what's new in this version:


## LLDB support


We had a great student for GSoC 2016 implementing LLDB support in KDevelop. The end result is that we now have a debugger framework which can be used both for the GDB & LLDB MI communcation. The LLDB plugin teaches KDevelop to talk to the standalone LLDB MI Driver (lldb-mi); so now it's possible to use LLDB as an alternative debugger backend for KDevelop. One interesting thing with LLDB that it's also potentially useful on OS X & Windows for us, especially when the Windows port of LLDB is getting more and more stable.


## Analyzer run mode


With 5.1, KDevelop got a new menu entry *Analyzer* which features a set of actions to work with analyzer-like plugins. During the last months, we merged analyzer plugins into kdevelop.git which are now shipped to you out of the box:


### Cppcheck


[Cppcheck](http://cppcheck.sourceforge.net/) is a well-known static analysis tool for C/C++ code. Cppcheck is useful for taking a closer look at your source code checking for common programming faults such as out of bounds accesses, memory leaks, null pointer dereferences, uninitialized variables, etc. pp. With the Cppcheck integration in KDevelop running the *cppcheck* executable is just one click away. KDevelop will pass the correct parameters to *cppcheck* including potential include paths and other options.


[![KDevelop with Cppcheck integration](/sites/www.kdevelop.org/files/inline-images/kdevelop-cppcheck.png)](/sites/www.kdevelop.org/files/inline-images/kdevelop-cppcheck.png)
### Other analyzers in the pipeline: Valgrind, clang-tidy, krazy2


While the Cppcheck plugin is shipped out of the box, other analyzers are not considered 100% stable yet and still reside in their own repositories. The clang-tidy plugin looks super promising (another static analysis & refactoring tool for C/C++) as it really easy to use from the command-line and thus easy to integrate into our IDE. We plan to import more of those analyzers into kdevelop.git so they'll be part of the kdevelop tarball and are thus available to you without having to install yet another package.


## Improved Python language support


Python language support now supports Python 3.6 syntax and semantics. In addition, thanks to the work of Francis Herne, various long-standing issues in the semantic analysis engine have been fixed:  most notably: loops and comprehensions infer types correctly in many more cases; type guessing works for arguments named by keywords (not only \*\*kwargs), and works better for class/staticmethods; types are inferred correctly from PEP-448 syntax in container literals; unsure types are handled in subscripts and tuple unpacking, and uses are found for \_\_call\_\_() and \_\_get/setitem\_\_(). All these improvements were accompanied by cleaning up dusty code, making future changes simpler as well. Furthermore our style checker integration has been rewritten, making it much faster and easier to configure.


These changes also mean that this beta is especially important for Python support, because we want to catch any potential regressions before the final 5.1 release. Please let us know about everything you notice!


## Perforce integration


Thanks to Morten Danielsen Volden we now have Perforce integration in kdevplatform.git, which can be used freely starting with KDevelop 5.1. Perforce is a commercial, proprietary revision control system. The Perforce integration in KDevelop simply works by running a local version of the *p4* executable (needs to be installed independently of KDevelop) with appropriate parameters. This is similar to how KDevelop integrates with other VCS, such as Git & Bazaar.


## Ongoing support for other platforms


We're continuously improving the Windows version of KDevelop and we're planning to release a first KDevelop version for OS X soon. For the Windows version, we upgraded the KF5 version to 5.29 and the LLVM/Clang version to 3.9.1.


## Get it


Together with the source code, we again provide a prebuilt one-file-executable for 64-bit Linux, as well as binary installers for 32- and 64-bit Microsoft Windows. You can find them on our [download page](/download#experimental_releases) in the *Experimental Releases* section.


The 5.0.80 source code and signatures can be downloaded from [here](http://download.kde.org/unstable/kdevelop/5.0.80/src/).


Please give this version a try and as always let us know about any issues you find via our [bug tracker](https://bugs.kde.org).


