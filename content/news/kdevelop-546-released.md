---
title: "KDevelop 5.4.6 released"
date: "2020-01-06 21:22:23+00:00"
lastmod: "2020-01-06 21:49:10+00:00"
author: "kossebau"
aliases:
- /news/kdevelop-546-released
tags:
- release
categories:
- News
---
We today provide a bugfix and localization update release with version 5.4.6. This release introduces no new features and as such is a safe and recommended update for everyone currently using a previous version of KDevelop 5.4.


You can find the updated Linux AppImage as well as the source code archives on our [download](/download) page.


## ChangeLog


### [kdevelop](https://commits.kde.org/kdevelop)


* Filetemplates: update addresses to web ones for GPLv2 & LGPLv2. ([commit](https://commits.kde.org/kdevelop/4f3f10452e98c7ce5433c0772a38c33cb005d00d))
* Filetemplates: fix wrong text of LGPLv2+ (KDE) (was GPLv2+ (KDE) one). ([commit.](https://commits.kde.org/kdevelop/58713d247795b03cc1e4d0e2e9406bfce370bd89) fixes bug [#414978](https://bugs.kde.org/414978))
* Shell: default to kdevelop icon for plugins in LoadedPluginsDialog list. ([commit](https://commits.kde.org/kdevelop/680153df1cc7efd0928ffa740364f31abfe5aa4e))
* Shell: use KAboutPluginDialog in LoadedPluginsDialog. ([commit](https://commits.kde.org/kdevelop/12016eae42b0483ef44dca61ec53b26f7824687f))
* Make ForegroundLock::isLockedForThread() also consider the current thread being the main (Qt) thread, and thus add this check in more places. ([commit](https://commits.kde.org/kdevelop/54597a6a389f278705f5c84262f4ecf0b90ef023))
* Clang: Fix a case of concurrent access to KTextEditor. ([commit](https://commits.kde.org/kdevelop/241d71a2ed0b526a905f285dd524dbfa516718c5))
* Fix wrongly internally duplicated KHelpMenu instance. ([commit](https://commits.kde.org/kdevelop/9c4b8e752937dd4523b04b57f237ac20c3bc3e45))
* Only unload plugins after the DUChain shutdown to prevent infinite loop due to non-loaded factories for types. ([commit](https://commits.kde.org/kdevelop/c3c9ea8a3cbf9afecd3fcdd9d6da96d92ecf7d9c))


### [kdev-python](https://commits.kde.org/kdev-python)


No user-relevant changes.


### [kdev-php](https://commits.kde.org/kdev-php)


* Update phpfunctions.php to phpdoc revision 348802. ([commit](https://commits.kde.org/kdev-php/5fb10062726449fcaaf8d5f8a1e29761957bf8c3))


