---
title: "KDevelop 4.7.0 Released"
date: "2014-09-13 18:11:23+00:00"
lastmod: "2016-02-28 21:10:54+00:00"
author: "milian"
aliases:
- /news/kdevelop-470-released
tags:
- release
- stable
- php
- python
- kde
- 4.7
categories:
- News
---
Today, the KDevelop team is proud to announce the final release of KDevelop 4.7.0. It is, again, a huge step forwards compared to the last release in terms of stability, performance and polishedness. This release is special, as it marks the end of the KDE4 era for us. As such, KDevelop 4.7.0 comes with a long-term stability guarantee. We will continue to improve it over the coming years, but will refrain from adding new features. For that, we have the upcoming KDevelop 5, based on KDE frameworks 5 and Qt 5, which our team is currently busy working on. See below for more on that topic.

## Important KDevelop 4.7.0 Changes



This new release of KDevelop comes with many important changes that should ensure it will stay usable for the foreseeable future. The CMake support was improved and extended to ensure that all idioms needed for KF5 development are available. The unit test support UI was polished and several bugs fixed. In the same direction, some noteworthy issues with the QtHelp integration were addressed. KDevelop's PHP language support now handles namespaces better and can understand traits aliases. Furthermore, some first fruits of the Google summer of code projects are included in this release. These changes pave the path toward better support for cross compile toolchains. Feature-wise, KDevelop now officially supports the Bazaar (bzr) version control system. On the performance front, it was possible to greatly reduce the memory footprint when loading large projects with several thousand files in KDevelop. Additionally, the startup should now be much faster.

As always, many thanks to the various contributors that made this release possible!

[![](/sites/kdevelop.org/files/kdev-47.png)  
*KDevelop 4.7.0*](/sites/kdevelop.org/files/kdev-47.png)


## Download KDevelop 4.7.0



The packagers of various distributions have already updated their version of KDevelop to the latest and greatest. If you want to look at the sources or compile it yourself, you can find the release tarballs on the KDE mirrors:

* [kdevplatform-1.7.0.tar.xz](http://download.kde.org/stable/kdevelop/4.7.0/src/kdevplatform-1.7.0.tar.xz.mirrorlist)
* [kdevelop-4.7.0.tar.xz](http://download.kde.org/stable/kdevelop/4.7.0/src/kdevelop-4.7.0.tar.xz.mirrorlist)
* [kdevelop-php-1.7.0.tar.xz](http://download.kde.org/stable/kdevelop/4.7.0/src/kdevelop-php-1.7.0.tar.xz.mirrorlist)
* [kdevelop-php-docs-1.7.0.tar.xz](http://download.kde.org/stable/kdevelop/4.7.0/src/kdevelop-php-docs-1.7.0.tar.xz.mirrorlist)
* [kdev-python-1.7.0.tar.xz](http://download.kde.org/stable/kdevelop/4.7.0/src/kdev-python-1.7.0.tar.xz.mirrorlist)
* [kdev-python-1.7.0-py3.tar.xz](http://download.kde.org/stable/kdevelop/4.7.0/src/kdev-python-1.7.0-py3.tar.xz.mirrorlist)


### md5 checksums



```
0912f881cb1219450aeb155494846bbd kdevelop-4.7.0.tar.xz
d1e87ff5d7417ead1143471957eeeae2 kdevelop-php-1.7.0.tar.xz
1f6f391d6afe1e708d491eb267de199d kdevelop-php-docs-1.7.0.tar.xz
72375e077f97b44056c76c7f85ce49ad kdevplatform-1.7.0.tar.xz
4a28c46ec22cc59722f3fee5d5106e3a kdev-python-1.7.0.tar.xz
708e43202056cc241209109b8bbd5bd0 kdev-python-1.7.0-py3.tar.xz
```

### sha1 checksum



```
6c4becf482956334975886963ba0acf158f2aa15 kdevelop-4.7.0.tar.xz
e3a41f7700af56840fd60219e083b761180b8b8c kdevelop-php-1.7.0.tar.xz
487b60f591929e7a8292ef8f1c555a0243f1176a kdevelop-php-docs-1.7.0.tar.xz
9fc196e7cb09ab33fd5cfbf5af19aa7c513efdc9 kdevplatform-1.7.0.tar.xz
559dc7f8c3a69844af92eb581b7205346b2fcd10 kdev-python-1.7.0.tar.xz
69ec55b920cfa1559561d367d435c15d63e920ac kdev-python-1.7.0-py3.tar.xz
```

### sha256 checksums



```
68de8f412e8ab6107766f00623e54c458d02825e3a70f5ea9969688f8c77c120 kdevelop-4.7.0.tar.xz
89afe5dcfd902025bba4b1da170fac494f3cb2ac39921bc342bdb8c576e08458 kdevelop-php-1.7.0.tar.xz
bdaa0798d3f5d587814af19dc253a359af2a443e33ce0075b357bd575c0af26e kdevelop-php-docs-1.7.0.tar.xz
bfd765019511c5c9abc19bc412c75d7abd468f1a077ce4bc471cd6704b9f53f7 kdevplatform-1.7.0.tar.xz
f4d025b45ea31fba8333c2112e2f521a75d1868a88b1a5ddae9a4b0119a0f36f kdev-python-1.7.0.tar.xz
b9bacacdd97246d0e0a7d36874ac8e59ecf1959f17d672316a65bbb16a082f48 kdev-python-1.7.0-py3.tar.xz
```

## The Future of KDevelop 5



The development branches of the various KDevelop related projects are currently being ported to KDE frameworks 5 and Qt 5. Already, this reached a very usable state. Some plugins are still missing, but the team is actively working on filling the gaps. It is good to see how quick this transition is taking place, and we are confident to release a first KDevelop 5 version next year.

[![](/sites/kdevelop.org/files/imagecache/reg/photos/kdev5-breeze.png)  

*A screenshot of the current KDevelop 5 development version running on Plasma 5 with the new Breeze theme.*](/screenshots/kdevelop-5-pre-alpha-breeze-theme)



We are *not* planning to rewrite our core architecture, considering how well it works for us in KDevelop 4. There are, however, other big changes planned. First and foremost we are in the process of writing a [language plugin based on LLVM/Clang](http://kfunk.org/2014/04/28/gsoc-2014-improving-the-clang-integration-in-kdevelop/). Not only does this give us an excellent C++ language support, it also comes with support for C and Objective-C languages for free. Even better, it allows us to throw away thousands of lines of code of our current C++ language plugin, which is a maintenance burden and hard to extend. The KDevelop team is very excited to see this happening and eagerly awaiting the day where the Clang language plugin is ready for prime-time.

[![](/sites/kdevelop.org/files/kdev-clang-codecompletion.png)  

*C++ language support based on Clang providing code completion on the usage of `std::vector`.*](/sites/kdevelop.org/files/kdev-clang-codecompletion.png)



Similarly, we are in the process of extending CMake upstream to get more information out of it directly. This enables us to replace our custom implementation of the CMake language in KDevelop with the real deal, which promises perfect support for all CMake features and greatly decreases the maintenance burden on our side.

Combined, we hope this brings us to a state, where the KDevelop team can work on improving the IDE and start innovating more rather than playing catch-up with the rapid progress in both the C++ language as well as CMake. The time gained will be put into finalizing the multitude of proof-of-concept plugins that so far never saw a release. Noteworthy in this regard are plugins that add support for the QML, JavaScript, CSS and Go languages. Additionally a tighter integration with QMake, Valgrind and other tools can be added.

Furthermore we are happy to see the improvements of Qt 5 in KDevelop, which lead, among others, to better performance in many places. And, combined with KDE frameworks 5, it finally becomes much simpler to get KDevelop up and running on Windows and Mac OS X. It is great to see contributions there and hope that more people start to use it. KDevelop 5 promises to become the first true cross-platform release of our IDE.

[![](/sites/kdevelop.org/files/imagecache/reg/photos/kdev-win.png)  

*A preview of KDevelop 5 running on Windows*](/screenshots/kdevelop-5-pre-alpha-windows)


## Help and Contribute



If you are interested in shaping the future of KDevelop, please [get in touch with us and start contributing](/contribute-kdevelop). We, for one, welcome our new frameworks overlords!