---
title: "KDevelop 4.5.1 Released"
date: "2013-05-30 10:51:44+00:00"
lastmod: "2016-02-28 21:10:54+00:00"
author: "apol"
aliases:
- /45/kdevelop-451-released
tags:
- stable
- kdevelop
- php
- 4.5
- released
- kdevplatform
categories:
- News
---
Good news everyone!

KDevelop 4.5.1 has been released, it's still fresh for all of you to try. For those who don't know, KDevelop is an IDE for all those developers who want to integrate the tools they need for developing, in a comfortable and simple solution. We have a focus on C++ and CMake, but you can find it useful when using other languages such as PHP and Python.

## What to expect


See [KDevelop 4.5.0](http://kdevelop.org/45/kdevelop-450-released), only more stable. Over 20 issues have been solved with this new release making it more reliable to your everyday use. Update very recommended.

## Get KDevelop now!


If you want to try it, you can either compile it from our tarballs or use your distribution's packages. You can find more information about how to install it in your distro on this wiki page: http://userbase.kde.org/KDevelop/Install4.5
Download the source tarballs from the KDE mirrors: <http://download.kde.org/stable/kdevelop/4.5.1/src/>

Happy coding!
The KDevelop Team