---
title: "KDevelop 5.3.3 released"
date: "2019-07-17 21:07:35+00:00"
lastmod: "2019-07-22 09:33:41+00:00"
author: "kossebau"
aliases:
- /news/kdevelop-533-released
tags:
- release
- 5.3
categories:
- News
---
We today provide a stabilization and bugfix release with version 5.3.3. This is a bugfix-only release, which introduces no new features and as such is a safe and recommended update for everyone currently using a previous version of KDevelop 5.3.


You can find a Linux AppImage as well as the source code archives on our [Download](/download) page. Windows installers are currently not offered, we are looking for someone interested to take care of that.


### [kdevelop](https://commits.kde.org/kdevelop)


* Use KDE\_INSTALL\_LOGGINGCATEGORIESDIR for kdebugsettings .categories files. ([commit.](https://commits.kde.org/kdevelop/f2ca627031f8976cf06641366592d3601c386795) )
* TextDocument: remove actions from contextmenu on hide already. ([commit.](https://commits.kde.org/kdevelop/d940a88e8bd92031cd7ffce2450cc24f266e6acc) code review [D22424](https://phabricator.kde.org/D22424))
* Sublime: fix crash on undocking toolviews with Qt 5.13. ([commit.](https://commits.kde.org/kdevelop/7f636275a2e41d5137ebc24b23cec2412f561404) fixes bug [#409790](https://bugs.kde.org/409790))
* Kdevplatform/interfaces: fix missing explicit QVector include. ([commit.](https://commits.kde.org/kdevelop/e2bea1a148bdb882115d29877c1cd458833015b4) )
* Fix kdevelopui.rc: bump version as required by string context changes. ([commit.](https://commits.kde.org/kdevelop/fcd66afaf8a4a71f2007365beb3ef3f981310ff1) )
* Shell: overwrite katectagsplugin to be disabled by default. ([commit.](https://commits.kde.org/kdevelop/efb70121bf0e0d0b20a63a9e8cac139a312e749a) )
* Translate relative paths of input files to absolute ones. ([commit.](https://commits.kde.org/kdevelop/bee988a1b680d8eba7efc600d9761247f90e5ee4) )
* Welcome page: do not add currently unused qml pages to qrc data. ([commit.](https://commits.kde.org/kdevelop/2fa7d3e962031d23f6952594fee5f9b619356f0e) )
* Fix browse mode not disabled after Ctrl is released. ([commit.](https://commits.kde.org/kdevelop/5c5495929fcfa0dd9d75ae76bbbf190cdac3e428) )
* Attempt to fix a crash on shutdown. ([commit.](https://commits.kde.org/kdevelop/58adfaab7af9fe0e70337544b9abd6bef49fe881) )
* ProblemHighlighter: Fix mark type handling. ([commit.](https://commits.kde.org/kdevelop/0c6bf764b6344397f46464b68ecbea3ea630706a) )
* Cmakebuilddirchooser: Set a minimum size. ([commit.](https://commits.kde.org/kdevelop/3903e400f0f8dde511f7043ae455c49e3913ff3b) )
* Fix memory leaks reported by ASAN. ([commit.](https://commits.kde.org/kdevelop/56c41656029d52b8a97026077cde7dccc4ec5368) )
* Qmake: Move builder plugin to correct category. ([commit.](https://commits.kde.org/kdevelop/f543df17fe440efe3cd6531a7c1d2452bc9bae7d) fixes bug [#407396](https://bugs.kde.org/407396))
* Add DesktopEntry to notifyrc. ([commit.](https://commits.kde.org/kdevelop/e9c489eba1743159b2852793980f487e75e890bd) code review [D20920](https://phabricator.kde.org/D20920))
* Output config subpages alphabetically, instead of order in which corresponding plugins were loaded. ([commit.](https://commits.kde.org/kdevelop/e8b3ee4a2541ba3d59545107896cc3830e030e7b) code review [D14391](https://phabricator.kde.org/D14391))
* Flatpak plugin - fix typo ("flies" -> "files"). ([commit.](https://commits.kde.org/kdevelop/44fcd808e6bf87d5882b756d1118f960147084ae) )


### [kdev-python](https://commits.kde.org/kdev-python)


* Use KDE\_INSTALL\_LOGGINGCATEGORIESDIR for kdebugsettings .categories file. ([commit.](https://commits.kde.org/kdev-python/2cda96b01178da046e9a8f91a37e5b067fe97ceb) )


### [kdev-php](https://commits.kde.org/kdev-php)


* Use KDE\_INSTALL\_LOGGINGCATEGORIESDIR for kdebugsettings .categories file. ([commit.](https://commits.kde.org/kdev-php/d86136c8030112ccf8958ff4bc4755b069fbcf66) )
* Update phpfunctions.php to phpdoc revision 347011. ([commit.](https://commits.kde.org/kdev-php/2b4a2abcc2330829552602020788be4865809bd3) )
* Parse more function aliases. ([commit.](https://commits.kde.org/kdev-php/fb4484a9b18c1743c445d648658b1169ab07d5ee) )
* More special handling for mcrypt constants. ([commit.](https://commits.kde.org/kdev-php/1004709844d0ca670429f6b55c6dca2770d92329) )
* Parse more class constants. ([commit.](https://commits.kde.org/kdev-php/fed9a170e9b9bc5f489ff5fbd3954a59b0bb0d81) )
* Parse more constants. ([commit.](https://commits.kde.org/kdev-php/bbdaceac32878f8f72af376429ea2d3e8484d672) )
* Parse constants under para. ([commit.](https://commits.kde.org/kdev-php/c95326706c2630b4b54937f16e770383257c9453) )
* Parse more constants within tables. ([commit.](https://commits.kde.org/kdev-php/a6b8d3a1e664bd65f2575a774fbd7ef00b25f582) )
* Parse url stat constants. ([commit.](https://commits.kde.org/kdev-php/cd5cc1375da3c8692adf237f12366de7d73d3170) )
* Parse php token constants. ([commit.](https://commits.kde.org/kdev-php/34ede9006d6ef0890f5d5f3b799f9bc1eb320203) )
* Parse file upload constants. ([commit.](https://commits.kde.org/kdev-php/9a4fb516de9ca27f9233c55dd05965e607eb86aa) )
* Parse constants under tables. ([commit.](https://commits.kde.org/kdev-php/1055217b856ce6a4cce553ed706170adbc19b97a) )
* Parse constants under section and within tables. ([commit.](https://commits.kde.org/kdev-php/709b95116e0d3bea6616aa52b8f38f4ffa537ac3) )
* Parse nl-langinfo constants. ([commit.](https://commits.kde.org/kdev-php/35b50146169e7326e31afd511ffde2ad24abb73c) )
* Parse constants split over multiple lists within one file. ([commit.](https://commits.kde.org/kdev-php/c031d1c43754009b01d39b1efa433eb51bc6da8f) )
* Fix function declarations with varargs. ([commit.](https://commits.kde.org/kdev-php/77fb3cad39deaf4aaf0a5112b8b90cd75053bde8) )


