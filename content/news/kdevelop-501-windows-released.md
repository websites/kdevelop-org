---
title: "KDevelop for Windows: Official 5.0.1 beta installer available now"
date: "2016-10-09 23:10:24+00:00"
lastmod: "2016-10-10 18:41:12+00:00"
author: "sbrauch"
aliases:
- /news/kdevelop-501-windows-released
tags:
- KDevelop 5
- windows
categories:
- News
---
Today, we are happy to announce the availability of the first official KDevelop installer for Microsoft Windows. The installer is for the stable KDevelop 5.0.1 release, but we still release it as a *beta*, since there might be Windows-specific issues.


![Sceenshot of KDevelop on Windows 10](/sites/www.kdevelop.org/files/inline-images/kdevelop-win10-mainwindow.png)
 


The installer contains the standard **C++ and QML/JS language plugins**, as well as the **Python** and **PHP plugins**, such that you can use KDevelop to write code in those languages on Windows as well.


Instructions on [how to set up a compiler and build system for C++ development can be found here](https://userbase.kde.org/KDevelop4/Manual/WindowsSetup).


Download [KDevelop for Windows](/download) now!


Your feedback on your experiences with KDevelop under Windows are very much appreciated. Please send us bug reports and/or comment below!


