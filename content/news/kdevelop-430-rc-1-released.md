---
title: "KDevelop 4.3.0 RC 1 Released"
date: "2012-02-25 16:50:58+00:00"
lastmod: "2016-02-28 21:10:54+00:00"
author: "milian"
aliases:
- /43/kdevelop-430-rc-1-released
tags:
- release
- kdevelop
- 4.3
- unstable
- release-candidate
categories:
- News
---
The KDevelop team is proud to announce the immediate availability of KDevelop 4.3.0 RC 1. This is probably the last testing version before the release of 4.3 final. This release candidate includes additional bug fixes and further improves the C++11 language support. Please try it out and give us feedback to ensure a smooth 4.3 release.

## Download


You can find the source code available to download on the KDE mirrors: [download KDevelop 4.3 RC 1](http://download.kde.org/download.php?url=unstable/kdevelop/4.2.90/src/). There, you can also find the changelogs in case you wonder what exactly has happened since KDevelop 4.3 Beta 2.

## Hashes



To verify the correctness of your downloaded packages, you can use the following SHA1 and MD5 hashes:

### SHA1Sum



```

459dbbc51f157b9a9322bf429bec84a881473f32  kdevelop-4.2.90.tar.bz2
51511e78067a98a69003ba8bc36cf677dba9c69a  kdevelop-php-1.2.90.tar.bz2
03747b9abcc6a7144a5e6ec914e513fc258c29b8  kdevelop-php-docs-1.2.90.tar.bz2
1049eb9c723225c24a38cf2e34381feb370013fc  kdevplatform-1.2.90.tar.bz2

```

### MD5Sum



```

6666e85fdc41447b951fb53e05f67d36  kdevelop-4.2.90.tar.bz2
694206cbcb7f9ceabac5c1ce44c3e8da  kdevelop-php-1.2.90.tar.bz2
d7241539140c60f4878f9a1986f8f938  kdevelop-php-docs-1.2.90.tar.bz2
469f75c7afc62b6ce8289c790b096612  kdevplatform-1.2.90.tar.bz2

```
