---
title: "kdev-python 1.5 and 1.4.2 released"
date: "2013-05-03 11:44:31+00:00"
lastmod: "2016-02-28 21:10:54+00:00"
author: "sbrauch"
aliases:
- /45/kdev-python-15-and-142-released
tags:
- release
- kdevelop
- python
- kdev-python
- 4.5
- tests
categories:
- News
---
Half a year after the first stable release, I'm happy to announce the release of kdev-python 1.5 today, which brings Python support to KDevelop 1.5. Together with 1.5, I release the second (and last) update for the 1.4 series which contains two backported crash fixes. Please refer to [announcement on my blog for more information](http://scummos.blogspot.de/2013/05/kdev-python-15-released.html). Download links are in the full view of this article.

  
**kdev-python 1.5** tarballs are here:
http://download.kde.org/stable/kdevelop/kdev-python/1.5.0/src/kdev-python-v1.5.0.tar.bz2.mirrorlist
SHA 256 Checksum: 8ff2b8aeb4b3b00e67e8c09a1a57ec442c5a0f2b68a741c7322da5caeffb6cda

  
**kdev-python 1.4.2** is here:
http://download.kde.org/stable/kdevelop/kdev-python/1.4.2/src/kdev-python-v1.4.2.tar.bz2.mirrorlist
SHA 256 Checksum: 8cb88ec0686c0c9503177896eb125ac6e2f0066e96d0b9492174324ed6ae3700