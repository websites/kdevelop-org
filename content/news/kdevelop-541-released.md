---
title: "KDevelop 5.4.1 released"
date: "2019-08-12 20:00:00+00:00"
lastmod: "2019-08-12 22:44:34+00:00"
author: "kossebau"
aliases:
- /news/kdevelop-541-released
tags:
- release
categories:
- News
---
We today provide a stabilization and bugfix release with version 5.4.1. This is a bugfix-only release, which introduces no new features and as such is a safe and recommended update for everyone currently using KDevelop 5.4.0.


You can find the updated Linux AppImage as well as the source code archives on our [download](/download) page.


## ChangeLog


### [kdevelop](https://commits.kde.org/kdevelop)


* Fix crash: add missing Q\_INTERFACES to OktetaDocument for IDocument. ([commit.](https://commits.kde.org/kdevelop/4e0b20cd8b36814527331a3e62513377304dd05e) fixes bug [#410820](https://bugs.kde.org/410820))
* Shell: do not show bogus error about repo urls on DnD of normal files. ([commit](https://commits.kde.org/kdevelop/7cd3dd22ace2de7a59d8ce6d9d4562930868cee1))
* [Grepview] Use the correct icons. ([commit](https://commits.kde.org/kdevelop/8c8c7b4120bfc83e8ad8cfee447262438e2ede32))
* Fix calculation of commit age in annotation side bar for < 1 year. ([commit](https://commits.kde.org/kdevelop/082844c1fc110c460b00fa0766035d7cf7272cc8))
* Appdata: add <launchable/> entry. ([commit](https://commits.kde.org/kdevelop/ab22ca659cf337fc1de01d69b0ced949156f01e5))
* Fix registry path inside kdevelop-msvc.bat. ([commit](https://commits.kde.org/kdevelop/c702a84702b3800ed92d80dfd7a0c2ddfd6318f7))


### [kdev-python](https://commits.kde.org/kdev-python)


No user-relevant changes.


### [kdev-php](https://commits.kde.org/kdev-php)


* Update phpfunctions.php to phpdoc revision 347831. ([commit](https://commits.kde.org/kdev-php/7dbc9c1e30c5a8c4563d7e1091d75343a14e2d25))
* Switch few http URLs to https. ([commit](https://commits.kde.org/kdev-php/aec473c02c8d58aa43ad4e7c8d0ac286f0546cce))


