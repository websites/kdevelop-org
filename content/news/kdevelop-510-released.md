---
title: "KDevelop 5.1.0 released"
date: "2017-03-21 10:00:00+00:00"
lastmod: "2017-03-21 16:16:26+00:00"
author: "kfunk"
aliases:
- /news/kdevelop-510-released
tags:
- release
- kf5
- qt5
- 5.1
categories:
- News
---
We are happy to announce the release of KDevelop 5.1! Tons of new stuff entered KDevelop 5.1. Here's a summary of what's new in this version:


## LLDB support


We had a great student for GSoC 2016 [implementing LLDB support in KDevelop](https://unlimitedcodeworks.xyz/blog/2016/04/23/gsoc-kdevelop-lldb-support/). The end result is that we now have a debugger framework which can be used both for the GDB & LLDB MI communcation. The LLDB plugin teaches KDevelop to talk to the standalone LLDB MI Driver (lldb-mi); so now it's possible to use LLDB as an alternative debugger backend for KDevelop. One interesting thing with LLDB that it's also potentially useful on OS X & Windows for us, especially when the Windows port of LLDB is getting more and more stable.


## Analyzer run mode


With 5.1, KDevelop got a new menu entry *Analyzer* which features a set of actions to work with analyzer-like plugins. During the last months, we merged analyzer plugins into kdevelop.git which are now shipped to you out of the box:


### Cppcheck


[Cppcheck](http://cppcheck.sourceforge.net/) is a well-known static analysis tool for C/C++ code. Cppcheck is useful for taking a closer look at your source code checking for common programming faults such as out of bounds accesses, memory leaks, null pointer dereferences, uninitialized variables, etc. pp. With the Cppcheck integration in KDevelop running the *cppcheck* executable is just one click away. KDevelop will pass the correct parameters to *cppcheck* including potential include paths and other options.


[![KDevelop with Cppcheck integration](/sites/www.kdevelop.org/files/inline-images/kdevelop-cppcheck.png)](/sites/www.kdevelop.org/files/inline-images/kdevelop-cppcheck.png)
### Other analyzers in the pipeline: Valgrind, clang-tidy, krazy2


While the Cppcheck plugin is shipped out of the box, other analyzers are not considered 100% stable yet and still reside in their own repositories. The clang-tidy plugin looks super promising (another static analysis & refactoring tool for C/C++) as it really easy to use from the command-line and thus easy to integrate into our IDE. We plan to import more of those analyzers into kdevelop.git so they'll be part of the kdevelop tarball and are thus available to you without having to install yet another package.


## Initial OpenCL language support, CUDA support upcoming


Since 5.1 KDevelop is able to parse code written in the [Open Computing Language (OpenCL)](https://www.khronos.org/opencl/). The OpenCL language support inside KDevelop is backed by our Clang-based language support backend and thus just required minimal changes in KDevelop to start supporting it. Support for handling NVidia's [CUDA](http://www.nvidia.com/object/cuda_home_new.html) files will be part of 5.2 instead. Stay tuned.


[![KDevelop with OpenCL language support](/sites/www.kdevelop.org/files/inline-images/kdevelop-opencl-new.png)](/sites/www.kdevelop.org/files/inline-images/kdevelop-opencl-new.png)
 


Note that for KDevelop to detect *.cl* files as OpenCL files, an up-to-date shared-mime-info package which contains [this](https://bugs.freedesktop.org/show_bug.cgi?id=26913) patch is required. Alternatively, you can add the mime type yourself by creating the file */usr/share/mime/text/x-opencl-src.xml* with appropriate contents and re-running *update-mime-database* yourself.


## Improved Python language support


Python language support now supports Python 3.6 syntax and semantics. In addition, thanks to the work of Francis Herne, various long-standing issues in the semantic analysis engine have been fixed:


* Loops and comprehensions infer types correctly in many more cases, including on user-defined types with \_\_iter\_\_ and \_\_next\_\_ methods.
* Type guessing works for arguments named by keywords (not only \*\*kwargs), and works better for class/staticmethods.
* Property accesses get the return type of the decorated method.
* Types are inferred correctly from PEP-448 syntax in container literals.
* Unsure types are handled in subscripts and tuple unpacking.
* Uses are found for \_\_call\_\_() and \_\_get/setitem\_\_().


These improvements were accompanied by cleaning up dusty code, making future changes simpler as well.Furthermore, our style checker integration has been rewritten, making it much faster and easier to configure.


![Screenshot of advanced features under kdev-python](/sites/www.kdevelop.org/files/inline-images/kdev-python-newfeatures_0.png)
## Perforce integration


Thanks to Morten Danielsen Volden we now have Perforce integration in kdevplatform.git, which can be used freely starting with KDevelop 5.1. Perforce is a commercial, proprietary revision control system. The Perforce integration in KDevelop simply works by running a local version of the *p4* executable (needs to be installed independently of KDevelop) with appropriate parameters. This is similar to how KDevelop integrates with other VCS, such as Git & Bazaar.


## Color scheme selection inside KDevelop


It is now possible to select the current color scheme from within KDevelop, a feature which has been requested several times in the past. This is especially useful for when KDevelop is run under a different desktop environment than KDE Plasma, where the color scheme settings may not be easily accessible.


![Screenshot of KDevelop's color scheme selection menu](/sites/www.kdevelop.org/files/inline-images/kdevelop-color-scheme-selection.png)
## Ongoing support for other platforms


We're continuously improving the Windows version of KDevelop and we're planning to release a first KDevelop version for OS X soon (yep, we're repeating us here, please stay tuned!). For the Windows version, we upgraded the KF5 version to 5.32 and the LLVM/Clang version to 3.9.1.


## Get it


Together with the source code, we again provide a prebuilt one-file-executable for 64-bit Linux, as well as binary installers for 32- and 64-bit Microsoft Windows. You can find them on our [download page](/download).


The 5.1.0 source code and signatures can be downloaded from [here](http://download.kde.org/stable/kdevelop/5.1.0/src/).


Please give this version a try and as always let us know about any issues you find via our [bug tracker](https://bugs.kde.org).


## ChangeLog


### [kdevplatform](https://commits.kde.org/kdevplatform)


* Codegen: Fix crash when documenting template func. [Commit.](https://commits.kde.org/kdevplatform/63ba13534516a3552b2096c901a66a2952d71da1) Fixes bug [#377036](https://bugs.kde.org/377036)
* Grepview: Fix search in project root with trailing slash. [Commit.](https://commits.kde.org/kdevplatform/0f2ed78aff29582d993f23175b9640a8c2b064b2) Phabricator Code review [D4774](https://phabricator.kde.org/D4774)
* Fix project tree state not being saved. [Commit.](https://commits.kde.org/kdevplatform/c8180003ba1face3ab5e26c4b1b307c1f12e65c4) Phabricator Code review [D4665](https://phabricator.kde.org/D4665)
* Trim identifiers before triggering rename action. [Commit.](https://commits.kde.org/kdevplatform/9e440fc3f5acd36846ce01abbd65a577aad31e91) Fixes bug [#376521](https://bugs.kde.org/376521)
* Update tab colors when the palette changes. [Commit.](https://commits.kde.org/kdevplatform/42503e8640ae449021782326d883db6aec325f2a) Fixes bug [#358776](https://bugs.kde.org/358776)
* Grepview: Fix crash on exit with grep dialog open. [Commit.](https://commits.kde.org/kdevplatform/b7c15acecda39681f1c9ae5c7da01989e9471d74) Fixes bug [#348784](https://bugs.kde.org/348784)
* Template file dialog: kill old widgets on re-entering TemplateOptionsPage. [Commit.](https://commits.kde.org/kdevplatform/8ec0ea56560c801790966cde9957c67f4462ae31) Phabricator Code review [D4413](https://phabricator.kde.org/D4413)
* GrepOutputView: show older search results fully expanded initially. [Commit.](https://commits.kde.org/kdevplatform/7f3cc8ef0b6efc08d48902a3a6109817fd04fb3b) Phabricator Code review [D4605](https://phabricator.kde.org/D4605)
* Toolbar state was not being saved / restored. [Commit.](https://commits.kde.org/kdevplatform/6532c83a8b86a1ab1eb9e815dc0a254762881066) Phabricator Code review [D4122](https://phabricator.kde.org/D4122)
* Fix Bug 374458 by properly blocking signals on debugger combo box. [Commit.](https://commits.kde.org/kdevplatform/7aa07c93de95cc46afd0cb02b191910010ad231f) Phabricator Code review [D4555](https://phabricator.kde.org/D4555)
* Prevent progress notifications on copying and writing back project files. [Commit.](https://commits.kde.org/kdevplatform/f09049e58970544039038410be0808c5365e3eeb) Phabricator Code review [D4409](https://phabricator.kde.org/D4409)
* Fix preview for file templates with options: add vars with default values. [Commit.](https://commits.kde.org/kdevplatform/1ee5c9bcb2098fe4bef8303502482c8490437752) Phabricator Code review [D4384](https://phabricator.kde.org/D4384)
* TemplatePreviewRenderer: inject also some dummy test data. [Commit.](https://commits.kde.org/kdevplatform/f687b9cbe4f70a31d55107a83a0ae8bdcd69ff5e) Phabricator Code review [D4369](https://phabricator.kde.org/D4369)
* Fix bug 374921: Tool view grouping no longer works in 5.1 branch. [Commit.](https://commits.kde.org/kdevplatform/38fa47e9860da2c3c3deb9f4312fa5a4bd343e56) Fixes bug [#373213](https://bugs.kde.org/373213). Phabricator Code review [D4219](https://phabricator.kde.org/D4219)
* Enable "Open Config"/"Close Project" always if just one project is loaded. [Commit.](https://commits.kde.org/kdevplatform/f52c7379858dc868d073a4608e74b27dc641ce36) Fixes bug [#375234](https://bugs.kde.org/375234). Phabricator Code review [D4216](https://phabricator.kde.org/D4216)
* Fix bug 375007 - Asserts when pressing "Show Imports" button in problems tool view. [Commit.](https://commits.kde.org/kdevplatform/e9e7ed91682d36da4dd27acce23748a37cfc459b) Phabricator Code review [D4152](https://phabricator.kde.org/D4152)
* Container types: do not write lock when calling addContentType(). [Commit.](https://commits.kde.org/kdevplatform/4cf23720b123dee3b4ad85957ed165d23e37e1e7) See bug [#374097](https://bugs.kde.org/374097)
* Fix progress widget flags. [Commit.](https://commits.kde.org/kdevplatform/115cacb46905e35f81c495894ee85272a593aed2) Fixes bug [#374357](https://bugs.kde.org/374357)
* Don't hit assertion with "View Diff". [Commit.](https://commits.kde.org/kdevplatform/58ef793180cd1cc4b071072fdf2d85e4fb9ea52a) Fixes bug [#373763](https://bugs.kde.org/373763)
* Remove KDEV\_USE\_EXTENSION\_INTERFACE. [Commit.](https://commits.kde.org/kdevplatform/71b57dcade5d8677495775b43299c2d4093c5719) Phabricator Code review [D3774](https://phabricator.kde.org/D3774)
* Add button to re-run search in files. [Commit.](https://commits.kde.org/kdevplatform/eb500a3e7c1c274f0cc3ec33790fe178c7ae2840) Phabricator Code review [D3550](https://phabricator.kde.org/D3550)
* Add "Documentation" settings category. [Commit.](https://commits.kde.org/kdevplatform/7fef687ed4ac026e57bed228f6513362af1c1f37) Phabricator Code review [D3700](https://phabricator.kde.org/D3700)
* Shell env script: Read user bashrc again. [Commit.](https://commits.kde.org/kdevplatform/b28cd75ff962333834046e3fe54fb286a6da74c3) Fixes bug [#373419](https://bugs.kde.org/373419)
* Add zsh support for kdevplatform konsole integration. [Commit.](https://commits.kde.org/kdevplatform/e48231347a8d3b9cc7a8d281d51c8e4a802c73b2) Phabricator Code review [D3171](https://phabricator.kde.org/D3171)
* Save/Restore Tool Views order. [Commit.](https://commits.kde.org/kdevplatform/af272d38ed05d4c99fcc94354cb2f4588452188b) Phabricator Code review [D3002](https://phabricator.kde.org/D3002)
* Fix context help for template data types. [Commit.](https://commits.kde.org/kdevplatform/70492f16ef2d2ef2c68493d91218d360ba434390) Phabricator Code review [D3205](https://phabricator.kde.org/D3205)
* Port kdev\_format\_source script to C++/Qt. [Commit.](https://commits.kde.org/kdevplatform/9a9bbf752999b902ae079307f5b83dd1d76b8610) Phabricator Code review [D2925](https://phabricator.kde.org/D2925)
* Documentation: small UI fix. [Commit.](https://commits.kde.org/kdevplatform/d88ad853f1559134e75c80e9f9ccc81d0c80a118) Phabricator Code review [D3255](https://phabricator.kde.org/D3255)
* Feature "Diff From Branch" on BranchManager. [Commit.](https://commits.kde.org/kdevplatform/0c9ff4de47481606e7f31cb83a4537831e52ae03) Phabricator Code review [D3073](https://phabricator.kde.org/D3073)
* Add filtering for ProblemsView. [Commit.](https://commits.kde.org/kdevplatform/766eaefcb21e110e1d76297c90725b1801bd8326) Fixes bug [#339839](https://bugs.kde.org/339839). Phabricator Code review [D3108](https://phabricator.kde.org/D3108)
* More helpful error message when failing to load project plugin. [Commit.](https://commits.kde.org/kdevplatform/fb0230e3b6f08303ae92b1912268733d73348061) See bug [#369326](https://bugs.kde.org/369326)
* Add "Analyze" group to ContextMenuExtension. [Commit.](https://commits.kde.org/kdevplatform/d5055a3bf4bbec17e36128df49381faafb4bd05d) Phabricator Code review [D3013](https://phabricator.kde.org/D3013)
* Add keyboard shortcut for "Switch Header/Source". [Commit.](https://commits.kde.org/kdevplatform/c9e008f0af694815a9b08e69bb47e844b8414fd6) Fixes bug [#359136](https://bugs.kde.org/359136). Phabricator Code review [D2757](https://phabricator.kde.org/D2757)
* Process QML runtime errors. [Commit.](https://commits.kde.org/kdevplatform/de37265e0536999fee5dc18ec4d07d2d7d9bd867) Phabricator Code review [D2735](https://phabricator.kde.org/D2735)
* Use folder prefix is filename is not unique. [Commit.](https://commits.kde.org/kdevplatform/99db1a6956bba70f0561392e0a4b835b47912e14) Phabricator Code review [D1947](https://phabricator.kde.org/D1947)
* Implement color scheme selection from these installed in the system using KColorSchemeManager. [Commit.](https://commits.kde.org/kdevplatform/938f1f7f14443180e91a55b0180e42589c3a3af5) Code review [#127979](https://git.reviewboard.kde.org/r/127979). Fixes bug [#279592](https://bugs.kde.org/279592)
* KDevPlatform changes for supporting the -iframework and -F header search path. [Commit.](https://commits.kde.org/kdevplatform/fe976f84222f6e33e22068c144479da7c7bdcbc3) Code review [#128284](https://git.reviewboard.kde.org/r/128284)
* [OS X] Show job progress in the Dock tile. [Commit.](https://commits.kde.org/kdevplatform/c0626300e1c18448dd653a3484fd1ca6298e2d8c) Code review [#128188](https://git.reviewboard.kde.org/r/128188)
* Project controller: make displaying project config dialog async. [Commit.](https://commits.kde.org/kdevplatform/7cac941ea94fff5fffda448030f5f9ef105fc3e2) Fixes bug [#364433](https://bugs.kde.org/364433)
* Integrate kdev-perforce into kdevelop. [Commit.](https://commits.kde.org/kdevplatform/a0842d40f09e938c2b2f1400203b52404f7dc62c) Phabricator Code review [D2001](https://phabricator.kde.org/D2001)
* Fix variable toolview not sync with framestack view. [Commit.](https://commits.kde.org/kdevplatform/e58084ee59c7f30b8794bd0d9b858080dd160cac) Phabricator Code review [D1351](https://phabricator.kde.org/D1351)
* Respect KDE HIG. [Commit.](https://commits.kde.org/kdevplatform/fae696a1756d741be832828ef3ff87a4643b6c5e) Fixes bug [#360503](https://bugs.kde.org/360503)
* [GCI] Add buttons "jump to first (last) item" to standardoutputview/outputwidget. [Commit.](https://commits.kde.org/kdevplatform/f3bbf469f42cff6ccff07054036b3a88d1b9c26a) Fixes bug [#338153](https://bugs.kde.org/338153). Phabricator Code review [D750](https://phabricator.kde.org/D750)
* Remove QtQuick1 dependency. [Commit.](https://commits.kde.org/kdevplatform/74c3a618db399aa5c758ced9d66f447d661c8b43) Code review [#125782](https://git.reviewboard.kde.org/r/125782)
* Don't close documents when saving. [Commit.](https://commits.kde.org/kdevplatform/22ade7a08a11be4ecd9b5d2b0f9b5553493ffb3a) Fixes bug [#351895](https://bugs.kde.org/351895)


### [kdevelop](https://commits.kde.org/kdevelop)


* Qmljs: Fix crash access internal function context. [Commit.](https://commits.kde.org/kdevelop/8f62583027a7d0ce89d0993bcb205fdda7317be6) Fixes bug [#376586](https://bugs.kde.org/376586)
* CustomBuildSystemConfigWidget: clear configs before reloading. [Commit.](https://commits.kde.org/kdevelop/e88622395a875b305796d573b44edc8d62f7f79a) Fixes bug [#376523](https://bugs.kde.org/376523)
* Add basic support for parsing OpenCL. [Commit.](https://commits.kde.org/kdevelop/da4e1a0f23a061feb88dc49bc085aad713551377) Phabricator Code review [D4930](https://phabricator.kde.org/D4930)
* Add path escaping to cmake completion. [Commit.](https://commits.kde.org/kdevelop/5276b1b4ea39dd62103d2bb890d9d05a7d45c3f6) Fixes bug [#338196](https://bugs.kde.org/338196). Phabricator Code review [D4760](https://phabricator.kde.org/D4760)
* Return immediately with unsupported custom-build-job arguments. [Commit.](https://commits.kde.org/kdevelop/7c03e9445a50a5730b220a1156ff17418660c995) Fixes bug [#376833](https://bugs.kde.org/376833)
* Improve the Qt5 QtQuick template and make it available in CMake as well. [Commit.](https://commits.kde.org/kdevelop/c0e586514af4a2de8b349cda79aa9ec1afc3ef62) Phabricator Code review [D4721](https://phabricator.kde.org/D4721)
* Fix a crash when gdb produces malformed output. [Commit.](https://commits.kde.org/kdevelop/a29c54f0cc1bafce18d80d66f7a6df3be538ad3e) Phabricator Code review [D4714](https://phabricator.kde.org/D4714)
* Clang: Don't insert new include beyond moc include. [Commit.](https://commits.kde.org/kdevelop/b7c4a7479e000751b0d067b359f3c1ad5803c77e) Fixes bug [#375274](https://bugs.kde.org/375274)
* Avoid utimes() in custommake projects, remove unsermake support. [Commit.](https://commits.kde.org/kdevelop/4f3813672089258a2044cbeb901c9432dc8d9a03) Phabricator Code review [D4612](https://phabricator.kde.org/D4612). Fixes bug [#376432](https://bugs.kde.org/376432)
* Ninjabuilder: Don't crash on `sudo ninja install`. [Commit.](https://commits.kde.org/kdevelop/e464a4a9790a659e9d0274516af295a11c965872) Fixes bug [#372888](https://bugs.kde.org/372888)
* Specify generator whenever there's no CMakeCache.txt. [Commit.](https://commits.kde.org/kdevelop/fdd6636711a9e5d58a7c18b2f520ee8ef7ec0285) Fixes bug [#349877](https://bugs.kde.org/349877)
* C/C++ Compilers config page: use KUrlRequester, focus edit field on Add. [Commit.](https://commits.kde.org/kdevelop/b2a27a23b8aa58f23c594d4368730f9164786b30) Phabricator Code review [D4582](https://phabricator.kde.org/D4582)
* Switch to new X-KDevelop-Languages key. [Commit.](https://commits.kde.org/kdevelop/e6873ddc2cdb492cd647db4c3ec22630033925df) Phabricator Code review [D4424](https://phabricator.kde.org/D4424)
* Add ClangClassHelper, to restore features of CppClassHelper. [Commit.](https://commits.kde.org/kdevelop/73d252a3e692413e55b9e504b26cf442310e1cb8) Phabricator Code review [D4359](https://phabricator.kde.org/D4359)
* More unit tests and fixes for lldb formatters. [Commit.](https://commits.kde.org/kdevelop/cdf2ec8b7ce7b1dd7cc4fe4fcbc467c472d1f952) Phabricator Code review [D3942](https://phabricator.kde.org/D3942)
* Fix segfault when analyzing source code issue. [Commit.](https://commits.kde.org/kdevelop/fe1a940bf1278eee8a8b3248222195f2cb5a8bde) Code review [#129767](https://git.reviewboard.kde.org/r/129767). Fixes bug [#374525](https://bugs.kde.org/374525)
* Add an app\_template for a simple KDevelop plugin. [Commit.](https://commits.kde.org/kdevelop/6df9566854e5d5ee4f5bddd19998c86a099ff5f7) Phabricator Code review [D3785](https://phabricator.kde.org/D3785)
* Advanced configuration settings for the CMake ProjectManager. [Commit.](https://commits.kde.org/kdevelop/d56117d68f29fff86103758078d6cc0f7bae6dab) Code review [#129416](https://git.reviewboard.kde.org/r/129416)
* QtHelp page loading fix. [Commit.](https://commits.kde.org/kdevelop/2c04b1a85078fb0aa369f1e3e6113787cbe80268) Phabricator Code review [D3671](https://phabricator.kde.org/D3671)
* Add zsh support for kdevelop konsole integration. [Commit.](https://commits.kde.org/kdevelop/98fac3c84d2c46c378b82c14da2ca22a2e5df0fc) Phabricator Code review [D3172](https://phabricator.kde.org/D3172)
* Assert if the gdb plugin was unloaded. [Commit.](https://commits.kde.org/kdevelop/0aaa24ecea04c26f1c9321860567d115b9b8b210) See bug [#370314](https://bugs.kde.org/370314). Code review [#129131](https://git.reviewboard.kde.org/r/129131)
* Cmake manager: fix incorrect settings import from existing build directory. [Commit.](https://commits.kde.org/kdevelop/81f8d15884578c7ba0cb2dbcb330b828b75dcaa1) Phabricator Code review [D2964](https://phabricator.kde.org/D2964)
* Remove non-existing include dir. [Commit.](https://commits.kde.org/kdevelop/675d9f0b3944002e76d7f473d4e9beb9e2160414) Phabricator Code review [D2898](https://phabricator.kde.org/D2898)
* Consider showing CppCheckPreferences page under "Analyzers" category in session global config menu. [Commit.](https://commits.kde.org/kdevelop/b0f80f3ddee70a17a0e79f5cefc2dd447f6bcd40) Phabricator Code review [D2821](https://phabricator.kde.org/D2821)
* Report man pages loading error. [Commit.](https://commits.kde.org/kdevelop/99d2e78582744646e4e9cb25739a1d440471119c) Code review [#128943](https://git.reviewboard.kde.org/r/128943)
* Fix KDevelop crashes when trying to debug from command-line. [Commit.](https://commits.kde.org/kdevelop/b386fb1cf30d5d0682b1fd2eebe2f458ebd75e86) Fixes bug [#367837](https://bugs.kde.org/367837)
* LLDB/GDB plugin: add support for coredumpctl in unittests, and fix LldbTest::testCoreFile. [Commit.](https://commits.kde.org/kdevelop/42fa01d4023d49f40d54dd39cdcd874d5af39936) Phabricator Code review [D2604](https://phabricator.kde.org/D2604)
* GDB/LLDB plugin: correctly handle string escaping and unescaping. [Commit.](https://commits.kde.org/kdevelop/6448a19c34e83344ca29e1209cfcc1c7c250575f) Phabricator Code review [D2375](https://phabricator.kde.org/D2375)
* Merge LLDB plugin into master. [Commit.](https://commits.kde.org/kdevelop/63e47f1d09dfa793f34bca256d1e479d849fbd11) Phabricator Code review [D2293](https://phabricator.kde.org/D2293)
* Reapply some patches to 5.0 that are lost when merging into master. [Commit.](https://commits.kde.org/kdevelop/0f623d54c4db06ee91fa020d549d65a23039fde3) Phabricator Code review [D2292](https://phabricator.kde.org/D2292)
* Implement color scheme selection from these installed in the system using KColorSchemeManager. [Commit.](https://commits.kde.org/kdevelop/f6965687228383239cdf5b1e97c012e82fd5d119) Code review [#127980](https://git.reviewboard.kde.org/r/127980). Fixes bug [#279592](https://bugs.kde.org/279592)
* Support -iframework and -F header search path options. [Commit.](https://commits.kde.org/kdevelop/46c0ea43dd3f51cb45e000b528f36bbc83802fac) Code review [#128272](https://git.reviewboard.kde.org/r/128272)
* Recognise #import directive in the context browser. [Commit.](https://commits.kde.org/kdevelop/ef2355e6fcd0429d18422140ece1381fbbf7947c) Code review [#128276](https://git.reviewboard.kde.org/r/128276)
* Qmljs: Don't hold locks while performing filesystem accesses or calling qmlplugindump. [Commit.](https://commits.kde.org/kdevelop/8389fb2330207b2fa04627d47eb754342f1adc85) Fixes bug [#363509](https://bugs.kde.org/363509). Code review [#128227](https://git.reviewboard.kde.org/r/128227)
* Fix issue with kdev-cppcheck selecting wrong project. [Commit.](https://commits.kde.org/kdevelop/8c81637dbc4a56d1a887473dff36c2671910fd92) Phabricator Code review [D1116](https://phabricator.kde.org/D1116)
* Always show recursive "missing include" errors in problem view. [Commit.](https://commits.kde.org/kdevelop/a1d6a13183c01b9ea2c45e470e81f9ec53df7ada) See bug [#358853](https://bugs.kde.org/358853)
* Add a testcase for BUG: 358853. [Commit.](https://commits.kde.org/kdevelop/7bdc9e8d9539c6e904edbd021d0cc359cdc8e843) See bug [#358853](https://bugs.kde.org/358853)
* Preserve existing windowIcons via QIcon::fromTheme's fallback. [Commit.](https://commits.kde.org/kdevelop/ba2b190354c89230fce6e0e30caf1041f1be1d6f) Code review [#126759](https://git.reviewboard.kde.org/r/126759)
* Make kdevelop-app and kdev-plugins use Qt5 resources for splash, rc, knsrc. [Commit.](https://commits.kde.org/kdevelop/3c3d01fe78a43ddb8f788f19ec1d841e3a713eaf) Phabricator Code review [D529](https://phabricator.kde.org/D529)
* Make qmljs kdevelop plugin compile on OSX. [Commit.](https://commits.kde.org/kdevelop/584240b54d50eeff55e05233e52d9b6d43fc049d) Phabricator Code review [D456](https://phabricator.kde.org/D456)
* Use IProject->path() instead of folder() which is deprecated and will be removed. [Commit.](https://commits.kde.org/kdevelop/762f6bfa06f6266875045f1788a450c0015b5429) Code review [#123117](https://git.reviewboard.kde.org/r/123117)


### [kdev-python](https://commits.kde.org/kdev-python)


* Restore compile-time support for Python 3.4.3+. [Commit.](https://commits.kde.org/kdev-python/f75c3b7433492c0f8b61612be3d0713a502500ee) Phabricator Code review [D4936](https://phabricator.kde.org/D4936)
* Very basic property getter support. [Commit.](https://commits.kde.org/kdev-python/eec18d928db9bc1c698569a528819c0b23392dc1) See bug [#372273](https://bugs.kde.org/372273). Phabricator Code review [D4207](https://phabricator.kde.org/D4207)
* Fix OOM condition in style checking: move buffer processing out of read loop. [Commit.](https://commits.kde.org/kdev-python/1e483026bc536321dd255ef8dc3324bb1ce39a07) Fixes bug [#375037](https://bugs.kde.org/375037)
* Style checking: read data from stdin as bytes, not in some encoding. [Commit.](https://commits.kde.org/kdev-python/07a4f7a2df00d4d891e6683e2dbbe416f993df51) See bug [#375037](https://bugs.kde.org/375037)
* Completion: wrap expression visitor into a read lock. [Commit.](https://commits.kde.org/kdev-python/600f6b34d95e506fdf941c5bfadc79aeaf7de617) Fixes bug [#374097](https://bugs.kde.org/374097)
* Yet more range fixes. [Commit.](https://commits.kde.org/kdev-python/458f64330f89187d8efd558da550974c2777904c) See bug [#373850](https://bugs.kde.org/373850)
* Completion hint: Correct default-argument index for non-static methods. [Commit.](https://commits.kde.org/kdev-python/cbcde7f0136bde15858931774fb9e44a8e217a7e) Fixes bug [#369369](https://bugs.kde.org/369369)
* PEP-448 for list and set literals. [Commit.](https://commits.kde.org/kdev-python/c870ddd5ace48fc17423a91659f0bdde48c979d4) Phabricator Code review [D3868](https://phabricator.kde.org/D3868)
* PEP-448 unpacking in dict literals. [Commit.](https://commits.kde.org/kdev-python/fde31d655526d861306e68810b84e01993754272) Phabricator Code review [D3867](https://phabricator.kde.org/D3867)
* Add types from unpacked dict argument to \*\*kwargs parameter. [Commit.](https://commits.kde.org/kdev-python/c17d665c3998591fec69bd33e3e2814f5fecb80e) Phabricator Code review [D3861](https://phabricator.kde.org/D3861)
* Lambda expression improvements. [Commit.](https://commits.kde.org/kdev-python/a16e2de3f089fa5f13046fad4ea934d33e9bd128) Fixes bug [#306212](https://bugs.kde.org/306212). Phabricator Code review [D3555](https://phabricator.kde.org/D3555)
* Set mostly-correct endCol on numbers, single-quoted strings and subscripts. [Commit.](https://commits.kde.org/kdev-python/2049938304a0a8b65240d551117c8c169ee60ece) See bug [#373850](https://bugs.kde.org/373850)
* Skip explicit `self` argument when calling via class. [Commit.](https://commits.kde.org/kdev-python/b36edbb6c54a462cc6d72c344c0f760d5a7c2248) Fixes bug [#369364](https://bugs.kde.org/369364)
* Make ExpressionVisitor::visitCall() clearer. [Commit.](https://commits.kde.org/kdev-python/b56caec83b465d30c0e25ce8dc7e1dc61fdb790e) Phabricator Code review [D3524](https://phabricator.kde.org/D3524)
* Shorten class context range by one line. [Commit.](https://commits.kde.org/kdev-python/d324e78c60cda606c661f01b4b49defd93829c00) Fixes bug [#309817](https://bugs.kde.org/309817)
* Pass docstring into `docstringContainsHint()` rather than declaration. [Commit.](https://commits.kde.org/kdev-python/9e9bcdc233f10430cbc97284e4174ccb923536c7) Phabricator Code review [D3535](https://phabricator.kde.org/D3535)
* Show uses for \_\_call\_\_() and \_\_{get,set}item\_\_(). [Commit.](https://commits.kde.org/kdev-python/94ab1eeedba313c85b966b447223a05af3091133) Phabricator Code review [D3512](https://phabricator.kde.org/D3512)
* Rename functionDeclarationForCalledDeclaration -> functionForCalled and tweak types. [Commit.](https://commits.kde.org/kdev-python/d757d25e9638c406f42329ae58fee50976725a79) Phabricator Code review [D3534](https://phabricator.kde.org/D3534)
* Get iterable content using \_\_iter\_\_() and \_\_next\_\_(). [Commit.](https://commits.kde.org/kdev-python/e0a92aac7d716bf706205a913d92246716e8727e) Fixes bug [#369363](https://bugs.kde.org/369363). Phabricator Code review [D3540](https://phabricator.kde.org/D3540)
* Make the hack for finding attribute ranges more robust. [Commit.](https://commits.kde.org/kdev-python/4814255d24fb61cc46446362ddfd10d5a4605c04) Phabricator Code review [D3437](https://phabricator.kde.org/D3437)
* Get more possible types when subscripting unsure-types. [Commit.](https://commits.kde.org/kdev-python/257246616406f136edba73644b33c49bbd5c54fa) Phabricator Code review [D3427](https://phabricator.kde.org/D3427)
* Pass AbstractType::Ptr, not Declaration\*, to accessAttribute(). [Commit.](https://commits.kde.org/kdev-python/3a534dbb961fa3757e7066583b5a24c16525ea5f) Fixes bug [#359912](https://bugs.kde.org/359912). Phabricator Code review [D3422](https://phabricator.kde.org/D3422)
* Improved unpacking and iteration. [Commit.](https://commits.kde.org/kdev-python/69177a77cbd17af0c19205ce25943cc8f693d80d) Fixes bug [#359915](https://bugs.kde.org/359915). Phabricator Code review [D3352](https://phabricator.kde.org/D3352)
* Allow configuring a per-project Python interpreter for search paths. [Commit.](https://commits.kde.org/kdev-python/82729e275d176a81101da98f9af4fd8a465f73c2) Fixes bug [#368970](https://bugs.kde.org/368970)
* Support expanding widgets in the completion list. [Commit.](https://commits.kde.org/kdev-python/cdce7813857d729755c02eaa0e9b2985f052dd7c) Fixes bug [#368263](https://bugs.kde.org/368263)
* Fix porting bug: path for docfile generator files. [Commit.](https://commits.kde.org/kdev-python/6023a85b30ad419cf37559cbaa47a82ef8ef6e66) Fixes bug [#359905](https://bugs.kde.org/359905)
* Lock parse lock for a short moment when destroying language support. [Commit.](https://commits.kde.org/kdev-python/4b4857bdffb2b5667c2e82f137cb7a974a6e3179) Fixes bug [#359263](https://bugs.kde.org/359263)
* Fix path in docfile KCM. [Commit.](https://commits.kde.org/kdev-python/9b575f8a5dcb261983eddbe369500b806bf9e655) Fixes bug [#358036](https://bugs.kde.org/358036)


### [kdev-php](https://commits.kde.org/kdev-php)


* Support of Class::{expr}() syntax from Php 5.4. [Commit.](https://commits.kde.org/kdev-php/609c18e3fcc014d8bec64ee40e080211dd27451b) Phabricator Code review [D4902](https://phabricator.kde.org/D4902)
* Support for $this as an array when implementing ArrayAccess. [Commit.](https://commits.kde.org/kdev-php/dd90e850afdc541b6edbd16eec74eaad8a4b49ae) Phabricator Code review [D4776](https://phabricator.kde.org/D4776)
* Php7 IIFE syntax parsing. [Commit.](https://commits.kde.org/kdev-php/deeba5b7af324456278cd78f469c4091abe8bda2) Fixes bug [#370515](https://bugs.kde.org/370515). Phabricator Code review [D4391](https://phabricator.kde.org/D4391)
* Fix compile issue due to recent changes in kdevplatform. [Commit.](https://commits.kde.org/kdev-php/0a5c869ba5aee49c47d50ec154b3601d58a9d554) Phabricator Code review [D3411](https://phabricator.kde.org/D3411)
* Fix parsing of the ::class constant. [Commit.](https://commits.kde.org/kdev-php/917be0efc3e14a496779af633b73d2ceb97ad71c) Phabricator Code review [D988](https://phabricator.kde.org/D988)
* Fix parsing of the ::class constant. [Commit.](https://commits.kde.org/kdev-php/ff5a7d86a3f13634d000bf9efde3d1d4ef8e9326) Phabricator Code review [D988](https://phabricator.kde.org/D988)


