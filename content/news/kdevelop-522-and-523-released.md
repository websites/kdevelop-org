---
title: "KDevelop 5.2.2 and 5.2.3 released"
date: "2018-05-19 22:00:00+00:00"
lastmod: "2018-05-20 09:27:46+00:00"
author: "kfunk"
aliases:
- /news/kdevelop-522-and-523-released
tags:
- kdevelop
- release
- 5.2
- kdevelop
- release
- 5.2
- kdevelop
- release
- 5.2
categories:
- News
---
We today provide a stabilization and bugfix release with version 5.2.2 and 5.2.3. 5.2.2 was tagged 6 weeks ago, but we never managed to release it because we did not have the patience to fix the Windows installers in time due to a broken CI. Windows installers are provided for 5.2.3 again. We'll only provide source tarballs for 5.2.2 and we encourage everyone to just skip this release and use 5.2.3 which contains a few more bug fixes.


This is a bugfix-only release, which introduces no new features and as such is a safe and recommended update for everyone currently using KDevelop 5.2.1.


You can find the updated Windows 32- and 64 bit installers, the Linux AppImage, as well as the source code archives on our [download](/download) page.


Please let us know of any issues you encounter when using KDevelop on the [bug tracker](http://bugs.kde.org), in the mailing lists for development or users (kdevelop@kde.org, kdevelop-devel@kde.org), or in the comment section of this release announcement.


## Change log



### [kdevelop](https://commits.kde.org/kdevelop)


* KDevelop : support whitespace between the '#' and 'include' (or 'import'). ([commit.](https://commits.kde.org/kdevelop/06cbebf36cfa39388430d7043197189b2e54a5d2) fixes bug [#394200](https://bugs.kde.org/394200). code review [D12903](https://phabricator.kde.org/D12903))
* Shell: Display generic project managers again. ([commit.](https://commits.kde.org/kdevelop/ab2fcd3b27890dfdc0b33f9eb31163e62bdd34e9) code review [D12279](https://phabricator.kde.org/D12279))
* Github: Fix Github repositories not fetched. ([commit.](https://commits.kde.org/kdevelop/0de6a6a006a9f4ae981b26e2849f1ee8d1859558) fixes bug [#392553](https://bugs.kde.org/392553). code review [D11980](https://phabricator.kde.org/D11980))
* Avoid emptry entries in project files filter list. ([commit.](https://commits.kde.org/kdevelop/3a74e103e03afea5946e96d12540ce32cb2f67bc) code review [D11912](https://phabricator.kde.org/D11912))
* Note org.kdevelop.IBasicVersionControl@kdevgit as dep for kdevghprovider. ([commit.](https://commits.kde.org/kdevelop/55754c49ae4d4d5e73f10b91c36fb262bc3e5831) code review [D11823](https://phabricator.kde.org/D11823))
* Never assert when the assert can fire sporadically. ([commit.](https://commits.kde.org/kdevelop/2dbd9c0451f0380195b072ae5e530c70f211d2d7) See bug [#357585](https://bugs.kde.org/357585))
* Sublime: Release space in tab bar when no status. ([commit.](https://commits.kde.org/kdevelop/1ec19dcbdf19fdc20a27841e3965b149ee43d0aa) See bug [#314167](https://bugs.kde.org/314167))
* Shell: Save entries of recent projects action. ([commit.](https://commits.kde.org/kdevelop/9e2f3f3b11810c1a4903db232d4b822589adbedc) fixes bug [#385915](https://bugs.kde.org/385915))
* Lldb: don't issue command when there's no env variables to set, fix Bug 391897. ([commit.](https://commits.kde.org/kdevelop/63819c741f7263d754f17d8dfa60caa198e751a7) code review [D11524](https://phabricator.kde.org/D11524))
* Fix crash when activating code completion item. ([commit.](https://commits.kde.org/kdevelop/8e3304857aad1f7e84c61fa9ec1877ea26b9dc5c) fixes bug [#391742](https://bugs.kde.org/391742))
* Do not add return type to constructors declaration when editing definition in signature assistant. ([commit.](https://commits.kde.org/kdevelop/3f99e3deefbe3b77ba023bf09b69d8da3ca815fc) fixes bug [#365420](https://bugs.kde.org/365420). code review [D11291](https://phabricator.kde.org/D11291))
* Make lambda introduce a context in DU chain. ([commit.](https://commits.kde.org/kdevelop/dd038eb1a8fb4d663fe03eb6bd6ebf16d8ace3d0) fixes bug [#387994](https://bugs.kde.org/387994). code review [D11303](https://phabricator.kde.org/D11303))
* Fix bug 384082 - cppcheck is checking CMake generated files. ([commit.](https://commits.kde.org/kdevelop/db0d8027749ba8c94702981ccb3062fa6c6006eb) fixes bug [#384082](https://bugs.kde.org/384082). code review [D11041](https://phabricator.kde.org/D11041))
* Never run qmlplugindump on plugins that already offer plugins.qmltypes. ([commit.](https://commits.kde.org/kdevelop/6c8c3263d7b56216ce5cf38d74ba8e910db6422f) code review [D10872](https://phabricator.kde.org/D10872))
* Fix CodeCompletion of Strongly Typed Enum. ([commit.](https://commits.kde.org/kdevelop/76d7b807825ddc1b30c4111bb27754f84a63c0a1) code review [D10738](https://phabricator.kde.org/D10738))
* Make sure qmlplugindump works on my system. ([commit.](https://commits.kde.org/kdevelop/7331eb40cb0b1c6d663ba91a1f99b57ab848cb1e) code review [D10782](https://phabricator.kde.org/D10782))
* Make sure we don't crash when stopping all jobs. ([commit.](https://commits.kde.org/kdevelop/8430d3058f92725b4e6b7d85d8d68550ce12ee79) code review [D10874](https://phabricator.kde.org/D10874))
* Help automoc to find metadata JSON files referenced in the code. ([commit.](https://commits.kde.org/kdevelop/d4378dc31b95bbc138c9837b4571203768c290d5) code review [D10693](https://phabricator.kde.org/D10693))
* Link against KF5::Purpose if it's available. ([commit.](https://commits.kde.org/kdevelop/39c110e3fdb08230eb2cb58831ebb8ad92f44296) code review [D9921](https://phabricator.kde.org/D9921))
* Properly quote expected string value in lldb formatter unittests. ([commit.](https://commits.kde.org/kdevelop/ad234814927813d5cf39cda527877b381ab92892) code review [D9929](https://phabricator.kde.org/D9929))
* Unbreak the GDB QUrl pretty printer test. ([commit.](https://commits.kde.org/kdevelop/a2a4632e9401bc32c7345fc2c17a33fbc22c86a3) code review [D9922](https://phabricator.kde.org/D9922))
* Unbreak QtPrintersTest::testQString. ([commit.](https://commits.kde.org/kdevelop/7a47aa5cc869a216fd9239602a28598d4c11a630) code review [D9923](https://phabricator.kde.org/D9923))
* Also unbreak QtPrintersTest::testQByteArray. ([commit.](https://commits.kde.org/kdevelop/7add59d4e142a42d395a6cf07a406870f68d590e) code review [D9924](https://phabricator.kde.org/D9924))
* Work around bug in kLineEdit. ([commit.](https://commits.kde.org/kdevelop/e6ee67c23ae333d2ea4129bf507c5a389684c960) code review [D9809](https://phabricator.kde.org/D9809). fixes bug [#373004](https://bugs.kde.org/373004))
* Fix crash when stopping process. ([commit.](https://commits.kde.org/kdevelop/f0fbcb7e6f52037c42d3155c404166077ccc3e75) code review [D9858](https://phabricator.kde.org/D9858))
* Performance: Reuse the global icon loader. ([commit.](https://commits.kde.org/kdevelop/94ed49aceac454f9b1fba8b65ba63e5c7562f8c6) code review [D9783](https://phabricator.kde.org/D9783))
* Cache ProblemPointers per translation unit. ([commit.](https://commits.kde.org/kdevelop/f2a6941e086cdf506c8fb1798c52982bff43792d) fixes bug [#386720](https://bugs.kde.org/386720). code review [D9772](https://phabricator.kde.org/D9772))
* Only set CMAKE\_AUTOMOC\_MACRO\_NAMES with KF5 < 5.42. ([commit.](https://commits.kde.org/kdevelop/4a9455fdbbad8c3497a0675144215a7465ad144c) code review [D9778](https://phabricator.kde.org/D9778))
* Format comments before setting them on the DUChain. ([commit.](https://commits.kde.org/kdevelop/9761e029d65cefa1a4c074101061b3e4e5b78e77) code review [D9472](https://phabricator.kde.org/D9472))
* Set toolbar/toolbutton font on quickopen line edit. ([commit.](https://commits.kde.org/kdevelop/56416bdde6325e0b4435cb348943d14c695a5b6e) code review [D9481](https://phabricator.kde.org/D9481))


### [kdev-python](https://commits.kde.org/kdev-python)


* Ensure that codestyle.py always returns something on stdout to unlock m\_mutex. ([commit.](https://commits.kde.org/kdev-python/864d2d70965e86b5b6b73ab726b88fa07d064121) fixes bug [#392031](https://bugs.kde.org/392031). code review [D11474](https://phabricator.kde.org/D11474))
* Fix crash with contexts opened in the baseclass list of a class definition. ([commit.](https://commits.kde.org/kdev-python/ec8c1c43e3803a39c1d1f72045ad800b50d3e98c) fixes bug [#389326](https://bugs.kde.org/389326))
* Fix appstream metadata filename and some content, and install it. ([commit.](https://commits.kde.org/kdev-python/c11088f118ca6748cb83fefcd61880bb650a77ea) code review [D9488](https://phabricator.kde.org/D9488))


### [kdev-php](https://commits.kde.org/kdev-php)


*No changes*


