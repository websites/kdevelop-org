---
title: "KDevelop 5.0.3 released"
date: "2016-12-01 21:00:00+00:00"
lastmod: "2016-12-06 20:16:19+00:00"
author: "sbrauch"
aliases:
- /news/kdevelop-503-released
tags:
- release
- windows
- linux
- KDevelop 5
categories:
- News
---
Today, we are happy to announce the release of KDevelop 5.0.3, the third bugfix and stabilization release for KDevelop 5.0. An upgrade to 5.0.3 is strongly recommended to all users of 5.0.0, 5.0.1 or 5.0.2.


Together with the source code, we again provide a prebuilt one-file-executable for 64-bit Linux, as well as binary installers for 32- and 64-bit Microsoft Windows. You can find them on our [download page](http://kdevelop.org/download).


List of notable fixes and improvements since version 5.0.2:


* Fix a performance issue which would lead to the UI becoming unresponsive when lots of parse jobs were created (BUG: 369374)
* Fix some behaviour quirks in the documentation view
* Fix a possible crash on exit (BUG: 369374)
* Fix tab order in problems view
* Make the "Forward declare" problem solution assistant only pop up when it makes sense
* Fix GitHub handling authentication (BUG: 372144)
* Fix Qt help jumping to the wrong function sometimes
* Windows: Fix MSVC startup script from not working in some environments
* kdev-python: fix some small issues in the standard library info


The 5.0.3 source code and signatures can be downloaded from [here](http://download.kde.org/stable/kdevelop/5.0.3/src/).


