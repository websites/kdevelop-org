---
title: "KDevelop 4.5.2 available"
date: "2013-10-31 12:21:19+00:00"
lastmod: "2016-02-28 21:10:54+00:00"
author: "milian"
aliases:
- /45/kdevelop-452-available
tags:
- release
- stable
- kdevelop
- kde
- 4.5
- kdevplatform
categories:
- News
---
Hello all!

After five months of work the KDevelop team is pleased to announce KDevelop 4.5.2. It comes packed with more than twenty bug fixes, among which are stability improvements that should benefit many users. As such, it is highly suggested to update to the new version as soon as possible.

KDevelop 4.5.2 is going to be the last patch release in the 4.5 time frame. We are already busily hacking away at KDevelop 4.6, which will come with even more bug fixes, higher stability, improved performance and better visuals and workflows. Stay tuned for the release at the end of the year!

## Download 4.5.2



While most people should just wait to get updated packages from their distributions, you can also download the sources from the usual location: http://download.kde.org/stable/kdevelop/4.5.2/src/

To verify that the downloaded tarballs are correct, use the following hash sums:

### MD5:



```

73cb25cf55740666a4ade996f0dd214b kdevelop-4.5.2.tar.xz
3c4ec01fb13e88195cfbd9c34a320377 kdevelop-php-1.5.2.tar.xz
3d11e1cd95cff5a822b5580538f3e96e kdevelop-php-docs-1.5.2.tar.xz
f0d1d9bd73f051593cd016f2a24be0de kdevplatform-1.5.2.tar.xz

```

### SHA1:



```

3bd354ca9ff8562c8099a41836ccd72456561409 kdevelop-4.5.2.tar.xz
784f76dd12ee0ab443fa0f9d05d0e3062aafdb38 kdevelop-php-1.5.2.tar.xz
44e737743d0c9663912480508adac0ccbb826a56 kdevelop-php-docs-1.5.2.tar.xz
a2fa1f765e992739546fa04d0aaffc4f2200c520 kdevplatform-1.5.2.tar.xz

```

## Changelog


### KDevplatform 1.5.2



```

* Kevin Funk: Update .gitignore, add kdiff3 files
* Kevin Funk: Disable color output for git-pull call
* Aleix Pol: Fix crash
* Aleix Pol: Make sure delayedModificationWarningOn() is always called as a slot
* Milian Wolff: Use relativePath instead of relativeUrl to properly display spaces.
* Milian Wolff: Cleanup and fixup ProjectUtils to not leak and not trigger crashes.
* Kevin Funk: Add "--force" to git-rm call
* Raphael Kubo da Costa: Install the Grantlee filter into the right location.
* Kevin Funk: Fix crash / odd behavior in project tree view
* Kevin Funk: Minor: Code cleanup
* Kevin Funk: Fix uninitialized value found by valgrind
* Aleix Pol: Fix crash when re-loading the file manager plugin
* Aleix Pol: Fix licensing mistake in output view
* Kevin Funk: Revert "ProjectBuildsetWidget: Add shortcut for delete"

```

### KDevelop 4.5.2



```

* Kevin Funk: Make qt4.py compatible with Python 3.x
* Kevin Funk: Update .gitignore, add kdiff3 files
* Milian Wolff: Compile fix, this should not have been part of the last commit...
* Milian Wolff: Backport crash fixes for crash in Cpp TypeBuilder.
* Rolf Eike Beer: when calling a constructor don't offer deleted ones for completion
* Rolf Eike Beer: do not offer explicitely deleted methods for code completion
* Rolf Eike Beer: do not insert spaces in function definition after void or type with no argument name
* Aleix Pol: Fix missing break in the c++ parser
* Andreas Cord-Landwehr: Fix build on big endian plattforms.
* Andreas Pakulat: Fix endless recursion in setSpecializedFrom
* Milian Wolff: Fix usage of PushValue for recursion counters in TemplateDeclaration.
* Aleix Pol: Gracefully react to a lack of a working directory
* Kevin Funk: cpp_header.h: Conditionally add license header
* Aleix Pol: Don't run an execute process command if the working dir is unknown
* Aleix Pol: Don't use the items after calling ::removeUrl
* Ben Wagner: Fix overloaded-virtual of computeCompletions.
* Aleix Pol: RETURN() also works with find_package() and include()
* Aleix Pol: Revert "Update global vars when visiting set(..CACHE..FORCE) AST node"
* Sergey Vidyuk: Support return statement
* Sergey Vidyuk: Update global vars when visiting set(..CACHE..FORCE) AST node
* Milian Wolff: Verify validity of type after deserialization from the storage.
* Aleix Pol: take() variables when they go out of scope
* Manuel Riecke: Correct Basic C++ Template
* Andreas Pakulat: Fix builds for okteta in a separate prefix
* Sven Brauch: add missing braces
* Aleix Pol: Prevent crash

```


Many thanks to all contributors who made this release possible!