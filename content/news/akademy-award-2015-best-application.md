---
title: "Akademy Award 2015 for Best Application"
date: "2015-07-30 16:17:39+00:00"
lastmod: "2016-02-28 21:10:54+00:00"
author: "milian"
aliases:
- /screenshots/akademy-award-2015-best-application
tags:
- Awards
---
At Akademy 2015, we got the Akademy Application award! I, Milian, received it in 
the name of the whole KDevelop team for our work on this important tool in the 
KDE ecosystem.

I want to explicitly state my sincere thanks to everyone ever involved in 
KDevelop - be it in the past or present. Without the tons of high quality 
contributions, we would never be where we are today. I also said that during 
my (short, made-up-on-the-spot) acceptance speech, that we nowadays stand on 
the shoulders of giants. Thousands of lines of code bringing ever amazing and 
innovative functionality even today have been created by contributors that are 
(sadly!) not active anymore Thank you all! And maybe it's a cool time to start 
getting back again? :).

It was quite a surprise for me to be the one to receive the award, and I have 
to admit that it was quite touching :) Personally, I have learned nearly 
everything I know about programming during the last 7 years I've been 
contributing to KDevelop, and this really keeps me motivated. I've had the 
best teachers imaginable - thank you, again!

I also want to express my deep gratitude to the currently active developers. I 
look forward to working together with you all. Let's continue to evolve the 
KDevelop project - we are all in this together!

Cheers, keep rocking!
